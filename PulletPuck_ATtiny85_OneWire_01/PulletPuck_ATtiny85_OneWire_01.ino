#include <OneWire.h>

const int redPin = 0;   // analog out, to PB0 for red LED, pin 5 on ATtiny85
const int bluePin = 1;  // analog out, to PB1 for blue LED, pin 6 on ATtiny85
const int greenPin = 4; // analog out, to PB4 for green LED, pin 3 on ATtiny85
const int onewirePin = 5; // digital in, to PB3 for one wire sensor, pin 2 on ATtiny85
OneWire  ds(5);  // on digital pin 3
const int LL = 63; // Lower Limit of acceptable temperature range; below this the light is blue
const int UL = 73; // Upper Limit of acceptable temperature range; above this the light is red


void setup()
{
  pinMode(bluePin, OUTPUT);
  pinMode(redPin, OUTPUT);
  pinMode(greenPin, OUTPUT);
  pinMode(onewirePin, INPUT);

}

void loop()
{
  byte i;
  byte present = 0;
  byte data[12];
  byte addr[8];
  int Temp;
  
  // GET THE TEMPERATURE FROM THE DALLAS ONE-WIRE TEMP SENSOR
  if ( !ds.search(addr)) {  // if address search fails, 
      ds.reset_search();
      return;
  }
  ds.reset();
  ds.select(addr);
  ds.write(0x44,1); // start conversion, with parasite power on at the end
  
  // This delay is used with the one-wire sensor for parasitic power.
  delay(750);
  
  present = ds.reset();
  ds.select(addr);    
  ds.write(0xBE);         // Read Scratchpad
  for ( i = 0; i < 9; i++) // we need 9 bytes
  {           
    data[i] = ds.read();
  }
  
  //take the two bytes from the response relating to temperature
  Temp=(data[1]<<8)+data[0];
  Temp=Temp>>4; //divide by 16 to get pure Celsius readout
  Temp=Temp*1.8+32; // comment this line out to get Celsius
  //digitalWrite(oneWirePower, LOW);  // cease sending power to the temp sensor
  
  
// NOW LIGHT THE LED BASED ON THE TEMPERATURE READ
  if (Temp < LL) // then light the blue LED - it's cold!
  {
    analogWrite(greenPin, 0);
    analogWrite(redPin, 0);
    GlowLightOnce(bluePin);
  }
  else if (Temp >= LL && Temp <= UL) // then light the green LED
  {
    analogWrite(bluePin, 0);
    analogWrite(redPin, 0);
    GlowLightOnce(greenPin);
  }
  else // light the red LED
  {
    analogWrite(bluePin, 0);
    analogWrite(greenPin, 0);
    GlowLightOnce(redPin);
  }
  
  //GlowLightOnce(bluePin);
  //GlowLightOnce(greenPin);
  //GlowLightOnce(redPin);
}

void GlowLightOnce(int bulb)  // subroutine applies one fade in - fade out blink of an LED
{
  int brightness = 0;
  int fadeAmount = 5;
  int totalcount = 0;

  do
  {
    analogWrite(bulb, brightness);
    brightness = brightness + fadeAmount;
    totalcount++;
    if (brightness == 255)
    {
      fadeAmount = -fadeAmount;
    }
    delay(35);
  } while (totalcount < 103);
  delay(1000);
}
