#define redLED 7
#define grnLED 8
#define bluLED 12
#define timelag 1000

void setup() {
  // put your setup code here, to run once:
  pinMode(redLED,OUTPUT);
  pinMode(grnLED,OUTPUT);
  pinMode(bluLED,OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(redLED,LOW); // LOW turns the light on as it's a common anode
  delay(timelag);
  digitalWrite(redLED,HIGH); // LOW turns the light on as it's a common anode
  digitalWrite(bluLED,LOW); // LOW turns the light on as it's a common anode
  delay(timelag);
  digitalWrite(bluLED,HIGH); // LOW turns the light on as it's a common anode
  digitalWrite(grnLED,LOW); // LOW turns the light on as it's a common anode
  delay(timelag);
  digitalWrite(grnLED,HIGH); // LOW turns the light on as it's a common anode
}
