#include <Wire.h>

#include <ESP8266WiFi.h>
#include "NTPClient.h" // to get internet date and time
#include"WiFiUdp.h"    // to get internet date and time

const char* ssid     = "Warp2";
const char* password = "z2xwSs6M";

// SI7021 I2C address is 0x40(64)
#define si7021Addr 0x40
 
void setup()
{
  Wire.begin();
  Serial.begin(9600);
  Wire.beginTransmission(si7021Addr);
  Wire.endTransmission();
  delay(300);

  pinMode(LED_BUILTIN, OUTPUT); // Initialize the LED_BUILTIN pin
  digitalWrite(LED_BUILTIN, LOW); // Start with it on (low = lit)
  
  Serial.print("Connecting to ");
  Serial.println(ssid);
  
  WiFi.begin(ssid, password);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  // Once you connect...
  digitalWrite(LED_BUILTIN, HIGH); // High = off
  Serial.println("");
  Serial.println("WiFi connected");  
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  //Serial.print("Netmask: ");
  //Serial.println(WiFi.subnetMask());
  //Serial.print("Gateway: ");
  //Serial.println(WiFi.gatewayIP());

}
 
void loop()
{
  unsigned int data[2];
 
  Wire.beginTransmission(si7021Addr);
  //Send humidity measurement command
  Wire.write(0xF5);
  Wire.endTransmission();
  delay(500);
 
  // Request 2 bytes of data
  Wire.requestFrom(si7021Addr, 2);
  // Read 2 bytes of data to get humidity
  if(Wire.available() == 2)
  {
    data[0] = Wire.read();
    data[1] = Wire.read();
  }
 
  // Convert the data
  float humidity  = ((data[0] * 256.0) + data[1]);
  humidity = ((125 * humidity) / 65536.0) - 6;
 
  Wire.beginTransmission(si7021Addr);
  // Send temperature measurement command
  Wire.write(0xF3);
  Wire.endTransmission();
  delay(500);
 
  // Request 2 bytes of data
  Wire.requestFrom(si7021Addr, 2);
 
  // Read 2 bytes of data for temperature
  if(Wire.available() == 2)
  {
    data[0] = Wire.read();
    data[1] = Wire.read();
  }
 
  // Convert the data
  float temp  = ((data[0] * 256.0) + data[1]);
  float celsTemp = ((175.72 * temp) / 65536.0) - 46.85;
  float fahrTemp = celsTemp * 1.8 + 32;
 
  // Output data to serial monitor
  Serial.print("Temperature = "); Serial.print(fahrTemp); Serial.println(" F");
  Serial.print("Humidity : "); Serial.print(humidity); Serial.println(" % RH");
  //Serial.print("Celsius : ");
  //Serial.print(celsTemp);
  //Serial.println(" C");
  //Serial.print("Fahrenheit : ");
  //Serial.print(fahrTemp);
  //Serial.println(" F");
  delay(5000);
}
