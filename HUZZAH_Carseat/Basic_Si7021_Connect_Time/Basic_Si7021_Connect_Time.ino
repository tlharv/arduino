#include <Wire.h>

#include <ESP8266WiFi.h>
#include "NTPClient.h" // to get internet date and time
#include"WiFiUdp.h"    // to get internet date and time

const char* ssid     = "Warp2";
const char* password = "z2xwSs6M";

#define BlueLED 2 // blue LED on Huzzah

// Determine offset from UTC, or Coordinated Universal time
// ref https://time.is/UTC
// Seattle is UTC-7 hrs, so offset = -9*60*60 = -32400
// Honolulu is UTC-10 hrs so offset = -10*60*60 = -36000
const long utcOffsetInSeconds = -25200;

// Define NTP Client to get time
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP,"pool.ntp.org",utcOffsetInSeconds);

// SI7021 I2C address is 0x40(64)
#define si7021Addr 0x40
 
void setup()
{
  Wire.begin();
  Serial.begin(9600);
  Wire.beginTransmission(si7021Addr);
  Wire.endTransmission();
  delay(300);

  pinMode(LED_BUILTIN, OUTPUT); // Initialize the LED_BUILTIN pin
  pinMode(BlueLED, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW); // Start with it on (low = lit)
  digitalWrite(BlueLED, HIGH);    // Start with blue LED off
  
  Serial.print("Connecting to ");
  Serial.println(ssid);
  
  WiFi.begin(ssid, password);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  timeClient.begin();

  // Once you connect...
  digitalWrite(LED_BUILTIN, HIGH); // High = off
  Serial.println("");
  Serial.println("WiFi connected");  
  Serial.print("IP address: "); Serial.println(WiFi.localIP());
  Serial.print("Netmask: "); Serial.println(WiFi.subnetMask());
  Serial.print("Gateway: "); Serial.println(WiFi.gatewayIP());
}
 
void loop()
{
  unsigned int data[2];

  // Flash the blue LED each loop
  digitalWrite(BlueLED,LOW); // turn on blue LED
  delay(100);
  digitalWrite(BlueLED,HIGH); // turn off blue LED
  timeClient.update();
  Serial.println();
  Serial.print("Current time in Seattle is ");
  Serial.print(timeClient.getHours());
  Serial.print(":");
  Serial.print(timeClient.getMinutes());
  Serial.print(":");
  Serial.println(timeClient.getSeconds());
 
  Wire.beginTransmission(si7021Addr);
  //Send humidity measurement command
  Wire.write(0xF5);
  Wire.endTransmission();
  delay(500);
 
  // Request 2 bytes of data
  Wire.requestFrom(si7021Addr, 2);
  // Read 2 bytes of data to get humidity
  if(Wire.available() == 2)
  {
    data[0] = Wire.read();
    data[1] = Wire.read();
  }
 
  // Convert the data
  float humidity  = ((data[0] * 256.0) + data[1]);
  humidity = ((125 * humidity) / 65536.0) - 6;
 
  Wire.beginTransmission(si7021Addr);
  // Send temperature measurement command
  Wire.write(0xF3);
  Wire.endTransmission();
  delay(500);
 
  // Request 2 bytes of data
  Wire.requestFrom(si7021Addr, 2);
 
  // Read 2 bytes of data for temperature
  if(Wire.available() == 2)
  {
    data[0] = Wire.read();
    data[1] = Wire.read();
  }
 
  // Convert the data
  float temp  = ((data[0] * 256.0) + data[1]);
  float celsTemp = ((175.72 * temp) / 65536.0) - 46.85;
  float fahrTemp = celsTemp * 1.8 + 32;
 
  // Output data to serial monitor
  Serial.print("Temperature = "); Serial.print(fahrTemp); Serial.println(" F");
  Serial.print("Humidity : "); Serial.print(humidity); Serial.println(" % RH");
  //Serial.print("Celsius : ");
  //Serial.print(celsTemp);
  //Serial.println(" C");
  //Serial.print("Fahrenheit : ");
  //Serial.print(fahrTemp);
  //Serial.println(" F");
  delay(59000);
}
