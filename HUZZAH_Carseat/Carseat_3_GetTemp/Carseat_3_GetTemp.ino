/*
 * This version of the sketch uses a TMP36 analog temp sensor.
 * It uses 3.3V input and GND from the Huzzah, and its signal
 * goes to the analog pin on the Huzzah.
*/

#include <ESP8266WiFi.h>
#include "NTPClient.h" // to get internet date and time
#include"WiFiUdp.h"    // to get internet date and time

int sensorPin = 0; // Analog pin of Huzzah

const char* ssid     = "Warp2";
const char* password = "z2xwSs6M";

#define BlueLED 2

// Determine offset from UTC, or Coordinated Universal time
// ref https://time.is/UTC
// Seattle is UTC-7 hrs, so offset = -7*60*60 = -25200
// Honolulu is UTC-10 hrs so offset = -10*60*60 = -36000

const long utcOffsetInSeconds = -25200;

// Define NTP Client to get time
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP,"pool.ntp.org",utcOffsetInSeconds);

void setup() {
  Serial.begin(115200);
  delay(100);

  pinMode(LED_BUILTIN, OUTPUT); // Initialize the LED_BUILTIN pin
  pinMode(BlueLED, OUTPUT);
  pinMode(sensorPin, INPUT); // temp sensor analog pin
  
  digitalWrite(LED_BUILTIN, LOW); // Start with it on (low = lit)
  digitalWrite(BlueLED, HIGH);
    
  Serial.print("Connecting to ");
  Serial.println(ssid);
  
  WiFi.begin(ssid, password);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  timeClient.begin();

  // Once you connect...
  digitalWrite(LED_BUILTIN, HIGH); // High = off
  //digitalWrite(BlueLED, LOW);
  Serial.println("");
  Serial.println("WiFi connected");  
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  Serial.print("Netmask: ");
  Serial.println(WiFi.subnetMask());
  Serial.print("Gateway: ");
  Serial.println(WiFi.gatewayIP());

}

void loop() {
  // Flash the blue LED each loop
  digitalWrite(BlueLED,LOW); // turn on blue LED
  delay(100);
  digitalWrite(BlueLED,HIGH); // turn off blue LED

  // Do what you want to do each loop
  timeClient.update();
  int reading = analogRead(sensorPin);
  float voltage = reading * 3.3;
  voltage /= 1024.0;
  
  // Output results to serial monitor
  Serial.print("Current time in Seattle is ");
  Serial.print(timeClient.getHours());
  Serial.print(":");
  Serial.print(timeClient.getMinutes());
  Serial.print(":");
  Serial.println(timeClient.getSeconds());

  Serial.print("Analog pin reading = "); Serial.println(reading);
  Serial.print(voltage); Serial.println(" volts");
  float tempC = (voltage - 0.5) * 100; // converting from 10mV per degree with 500mV offset
  // to degrees ((voltage - 500mV) times 100)
  Serial.print(tempC); Serial.println(" degrees C");
  // now convert to Fahrenheit
  float tempF = (tempC * 9.0 / 5.0) + 32.0;
  Serial.print(tempF); Serial.println(" deg F");
  Serial.println();
  
  delay(5000); // delay between loops  
}
