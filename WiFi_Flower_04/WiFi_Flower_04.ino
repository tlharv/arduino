/* 
  This sketch uses an Adafruit Huzzah, three Adafruit.IO feeds and multiple Neopixels to paint
  different aspects of a plexiglass flower that you can color using the color dashboard tool
  on Adafruit.IO.  Each feed will set the color of one or more Neopixels that will illuminate 
  the flower center, petals, and leaves.

  The sketch used as a starting point the Smart Toilet Light with ESP8255 sketch by Toni DiCola.
  License: MIT (https://opensource.org/licenses/MIT)

  Where the Smart Toilet Light sketch uses one feed to set one Neopixel color, this sketch assumes
  a series of eight Neopixels and multiple feeds to target key Neopixels with certain colors.
  
*/

#include <ESP8266WiFi.h>
#include "Adafruit_MQTT.h"
#include "Adafruit_MQTT_Client.h"
#include "Adafruit_NeoPixel.h"


// Configuration that you _must_ fill in:
//#define WLAN_SSID       "Warp2"    // Your WiFi AP name.
//#define WLAN_PASS       "z2xwSs6M"     // Your WiFi AP password.
#define WLAN_SSID       "UW-IoT110-R"    // Your WiFi AP name.
#define WLAN_PASS       "piIoT110"     // Your WiFi AP password.
#define AIO_USERNAME    "tlharv" // Adafruit IO username (see http://accounts.adafruit.com).
#define AIO_KEY         "ee89a3a7fe1547a6890ed6e28a8562e2"      // Adafruit IO key

// Configuration you can optionally change (but probably want to keep the same):
#define PIXEL_PIN       4                      // Pin connected to the NeoPixel data input.
#define PIXEL_COUNT     7                      // Number of NeoPixels.
#define PIXEL_TYPE      NEO_GRB + NEO_KHZ800   // Type of the NeoPixels (see strandtest example).
#define LIGHT_FEED_C    "Center"               // Name of the feed in Adafruit IO to listen for colors.
#define LIGHT_FEED_P    "Petals"
#define LIGHT_FEED_L    "Leaves"
#define AIO_SERVER      "io.adafruit.com"      // Adafruit IO server address.
#define AIO_SERVERPORT  1883                   // AIO server port.
#define PING_SEC        60                     // How many seconds to wait between MQTT pings.
                                               // Used to help keep the connection alive during
                                               // long periods of inactivity.

// Global state (you don't need to change this):
// Put strings in flash memory (required for MQTT library).
const char MQTT_SERVER[] PROGMEM    = AIO_SERVER;
const char MQTT_USERNAME[] PROGMEM  = AIO_USERNAME;
const char MQTT_PASSWORD[] PROGMEM  = AIO_KEY;

// Throughout this sketch, C = Center, P = Petal and L = Leaf
const char MQTT_PATH_C[] PROGMEM    = AIO_USERNAME "/feeds/" LIGHT_FEED_C; 
const char MQTT_PATH_P[] PROGMEM    = AIO_USERNAME "/feeds/" LIGHT_FEED_P; 
const char MQTT_PATH_L[] PROGMEM    = AIO_USERNAME "/feeds/" LIGHT_FEED_L; 

// Create ESP8266 wifi client, MQTT client, and feed subscriptions for Center, Petal and Leaf feeds
WiFiClient client;
Adafruit_MQTT_Client mqtt(&client, MQTT_SERVER, AIO_SERVERPORT, MQTT_USERNAME, MQTT_PASSWORD);
Adafruit_MQTT_Subscribe lightFeed_C = Adafruit_MQTT_Subscribe(&mqtt, MQTT_PATH_C);
Adafruit_MQTT_Subscribe lightFeed_P = Adafruit_MQTT_Subscribe(&mqtt, MQTT_PATH_P);
Adafruit_MQTT_Subscribe lightFeed_L = Adafruit_MQTT_Subscribe(&mqtt, MQTT_PATH_L);

// Create NeoPixel object
Adafruit_NeoPixel strip = Adafruit_NeoPixel(PIXEL_COUNT, PIXEL_PIN, PIXEL_TYPE);

// Other global variables:
uint32_t nextPing = 0;       // Next time a MQTT ping should be sent.
int pulsePeriodMS = 0;       // Period of time (in MS) for the pulse animation.
int C[3]; // array to store the three integer RGB values for the flower center color
int P[3]; // array to store the three integer RGB values for the flower petal color
int L[3]; // array to store the three integer RGB values for the flower leaf color


// -------  MQTT_connect, parseHexByte are unchanged from Toilet Light sketch ---------
// Explicit declaration of MQTT_connect function defined further below.
// Necessary because of bug/issue with recent Arduino builder & ESP8266.
void MQTT_connect();

// Function to parse a hex byte value from a string.  No different from the toilet light sketch.
// The passed in string MUST be at least 2 characters long!
// If the value can't be parsed then -1 is returned, otherwise the
// byte value is returned.
int parseHexByte(char* data) {
  char high = tolower(data[0]);
  char low = tolower(data[1]);
  uint8_t result = 0;
  // Parse the high nibble.
  if ((high >= '0') && (high <= '9')) {
    result += 16*(high-'0');
  }
  else if ((high >= 'a') && (high <= 'f')) {
    result += 16*(10+(high-'a'));
  }
  else {
    // Couldn't parse the high nibble.
    return -1;
  }
  // Parse the low nibble.
  if ((low >= '0') && (low <= '9')) {
    result += low-'0';
  }
  else if ((low >= 'a') && (low <= 'f')) {
    result += 10+(low-'a');
  }
  else {
    // Couldn't parse the low nibble.
    return -1;
  }
  return result;
}



void setup() {
  // Initialize serial output.
  Serial.begin(115200);
  delay(10);

  // Initialize NeoPixels and turn them off.
  strip.begin();
  lightPixels_ALL((0,0,0));
  delay(500);

  // Connect to WiFi access point.
  Serial.println(); Serial.println();
  Serial.print("Connecting to ");
  Serial.println(WLAN_SSID);
  WiFi.begin(WLAN_SSID, WLAN_PASS);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println();
  Serial.println("WiFi connected");
  Serial.println("IP address: "); Serial.println(WiFi.localIP());

  // Setup MQTT subscription.
  //mqtt.subscribe(&lightFeed);
  mqtt.subscribe(&lightFeed_C);
  mqtt.subscribe(&lightFeed_P);
  mqtt.subscribe(&lightFeed_L);
}

// Function to paint the flower
void PaintFlower(int C[3], int P[3], int L[3]) {
  strip.setPixelColor(0,L[0],L[1],L[2]);
  strip.setPixelColor(1,L[0],L[1],L[2]);
  strip.setPixelColor(2,L[0],L[1],L[2]);
  strip.setPixelColor(3,C[0],C[1],C[2]);
  strip.setPixelColor(4,C[0],C[1],C[2]);
  strip.setPixelColor(5,P[0],P[1],P[2]);
  strip.setPixelColor(6,P[0],P[1],P[2]);
  strip.show();
}

// Function to set all NeoPixels to a specified color.
void lightPixels_ALL(uint32_t color) {
  for (int i=0; i<PIXEL_COUNT; ++i) {
    strip.setPixelColor(i, color);
  }
  strip.show();
}



void loop() {
  
  // Ensure the connection to the MQTT server is alive (this will make the first
  // connection and automatically reconnect when disconnected).  See the MQTT_connect
  // function definition further below.
  MQTT_connect();

  // Check if any new data has been received from the light feed.
  Adafruit_MQTT_Subscribe *subscription;
  while ((subscription = mqtt.readSubscription(1000))) {
    if (subscription == &lightFeed_C) {
      // Received data from the Center light feed!
      // Parse the data to see how to change the light.
      char* data = (char*)lightFeed_C.lastread;  // C refers to feed for flower center color
      int dataLen = strlen(data);
      Serial.print("Got lightFeed_C = ");  Serial.println(data);
      if (dataLen < 1) {
        // Stop processing if not enough data was received.
        continue;
      }
      // Expect 6 more characters with the hex red, green, blue color.
      if (dataLen >= 7) {
        // Parse out the RGB color bytes.
        //int C_red = parseHexByte(&data[1]);
        C[0] = parseHexByte(&data[1]);  // Red component of center color
        C[1] = parseHexByte(&data[3]);  // Blue component of center color
        C[2] = parseHexByte(&data[5]);  // Green component of center color
        if ((C[0] < 0) || (C[1] < 0) || (C[2] < 0)) {
          // Couldn't parse the color, stop processing.
          Serial.println("Could not parse the color, so stop processing.");
          break; 
        }  // end parse if
      } // end dataLen if
    } // end subscription lightFeed_C if

    if (subscription == &lightFeed_P) {
      // Received data from the Center light feed!
      // Parse the data to see how to change the light.
      char* data = (char*)lightFeed_P.lastread;
      int dataLen = strlen(data);
      Serial.print("Got lightFeed_P = "); Serial.println(data);
      if (dataLen < 1) {
        // Stop processing if not enough data was received.
        continue;
      }
      // Expect 6 more characters with the hex red, green, blue color.
      if (dataLen >= 7) {
        // Parse out the RGB color bytes.
        P[0] = parseHexByte(&data[1]);  // Red component of petal color
        P[1] = parseHexByte(&data[3]);  // Red component of petal color
        P[2] = parseHexByte(&data[5]);  // Red component of petal color
        if ((P[0] < 0) || (P[1] < 0) || (P[2] < 0)) {
          // Couldn't parse the color, stop processing.
          Serial.println("Could not parse the color, so stop processing.");
          break; 
        }  // end parse if
      } // end dataLen if
    } // end subscription lightFeed_P if

    if (subscription == &lightFeed_L) {
      // Received data from the Center light feed!
      // Parse the data to see how to change the light.
      char* data = (char*)lightFeed_L.lastread;
      int dataLen = strlen(data);
      Serial.print("Got lightFeed_L = "); Serial.println(data);
      if (dataLen < 1) {
        // Stop processing if not enough data was received.
        continue;
      }
      // Expect 6 more characters with the hex red, green, blue color.
      if (dataLen >= 7) {
        // Parse out the RGB color bytes.
        L[0] = parseHexByte(&data[1]);  // Red component of leaf color
        L[1] = parseHexByte(&data[3]);  // Red component of leaf color
        L[2] = parseHexByte(&data[5]);  // Red component of leaf color
        if ((L[0] < 0) || (L[1] < 0) || (L[2] < 0)) {
          // Couldn't parse the color, stop processing.
          Serial.println("Could not parse the color, so stop processing.");
          break; 
        }  // end parse if
      } // end dataLen if
    } // end subscription lightFeed_L if

    // Now paint the flower!
    PaintFlower(C, P, L);  // IS THIS THE BEST LOCATION TO PLACE THIS FUNCTION?
    
    break;  // kicks us out of the While loop
    
  } // end while

  // Ping the MQTT server periodically to prevent the connection from being closed.
  if (millis() >= nextPing) {
    // Attempt to send a ping.
    if(! mqtt.ping()) {
      // Disconnect if the ping failed.  Next loop iteration a reconnect will be attempted.
      mqtt.disconnect();
    }
    // Set the time for the next ping.
    nextPing = millis() + PING_SEC*1000L;
  } // end ping if
} // end loop()

// Function to connect and reconnect as necessary to the MQTT server.
// Should be called in the loop function and it will take care of connecting.
void MQTT_connect() {
  int8_t ret;

  // Stop if already connected.
  if (mqtt.connected()) {
    return;
  }

  Serial.print("Connecting to MQTT... ");

  uint8_t retries = 3;
  while ((ret = mqtt.connect()) != 0) { // connect will return 0 for connected
       Serial.println(mqtt.connectErrorString(ret));
       Serial.println("Retrying MQTT connection in 5 seconds...");
       mqtt.disconnect();
       delay(5000);  // wait 5 seconds
       retries--;
       if (retries == 0) {
         // basically die and wait for WDT to reset me
         while (1);
       }
  }
  Serial.println("MQTT Connected!");
}

