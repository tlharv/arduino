// constants won't change. They're used here to set pin numbers:
const int blueButton = 9;     // the number of the pushbutton pin
const int blueLED = 8;      // the number of the LED pin

// variables will change:
int buttonState = 1;         // variable for reading the pushbutton status
int blueCMD = false; // variable for storing the toggle value of the blue button

void setup() {
  // initialize the LED pin as an output:
  pinMode(blueLED, OUTPUT);
  // initialize the pushbutton pin as an input:
  pinMode(blueButton, INPUT);
  digitalWrite(blueLED,LOW);
}

void loop() {
  // read the state of the pushbutton value:
  buttonState = digitalRead(blueButton);

  // check if the pushbutton is pressed. If it is, the buttonState goes LOW:
  if (buttonState == LOW) {
    // turn LED on:
    blueCMD = not(blueCMD);  // reverse the blue button command
    if (blueCMD) { // if value is TRUE
      digitalWrite(blueLED,HIGH);
      delay(500);
    }
    else {
      digitalWrite(blueLED,LOW);
      delay(500);
    }
  }
}
