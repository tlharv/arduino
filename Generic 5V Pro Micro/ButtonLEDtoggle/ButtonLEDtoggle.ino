// This sketch uses the 4-pin arcade buttons. For each button:
//    The red wire goes to the LED pin
//    The black wire goes to same ground as the Arduino
//    One yellow SPST wire goes to the button pin, with a 10k pull-up resistor to 5V
//    The other yellow SPST wire goes to ground
//    For non-red LEDs, consider putting a 150-ohm resistor in line with 5V just to protect it a little.

const int redButton = 9;     // the number of the pushbutton pin
const int redLED = 8;      // the number of the LED pin
const int greenButton = 6;
const int greenLED = 7;
const int blueButton = 10;
const int blueLED = 16;

// variables will change:
int redbuttonState = 1;         // variable for reading the pushbutton status
int greenbuttonState = 1;
int bluebuttonState = 1;
int redCMD = false; // variable for storing the toggle value of the blue button
int greenCMD = false;
int blueCMD = false;

void setup() {
  pinMode(redLED, OUTPUT);
  pinMode(redButton, INPUT);
  digitalWrite(redLED,LOW);
  
  pinMode(greenLED, OUTPUT);
  pinMode(greenButton, INPUT);
  digitalWrite(greenLED,LOW);

  pinMode(blueLED, OUTPUT);
  pinMode(blueButton, INPUT);
  digitalWrite(blueLED,LOW);
  
  Serial.begin(9600);
}

void loop() {
  // read the state of the pushbutton value:
  redbuttonState = digitalRead(redButton);
  greenbuttonState = digitalRead(greenButton);
  bluebuttonState = digitalRead(blueButton);
  
  // check if the pushbutton is pressed. If it is, the buttonState goes LOW:
  if (redbuttonState == LOW) {
    // turn LED on:
    redCMD = not(redCMD);  // reverse the blue button command
    if (redCMD) { // if value is TRUE
      digitalWrite(redLED,HIGH);
      delay(500);
    }
    else {
      digitalWrite(redLED,LOW);
      delay(500);
    }
  }
  
  if (greenbuttonState == LOW) {
    // turn LED on:
    greenCMD = not(greenCMD);  // reverse the blue button command
    if (greenCMD) { // if value is TRUE
      digitalWrite(greenLED,HIGH);
      Serial.println("Green light HIGH");
      delay(500);
    }
    else {
      digitalWrite(greenLED,LOW);
      Serial.println("Green light LOW");
      delay(500);
    }
  }

  if (bluebuttonState == LOW) {
    // turn LED on:
    blueCMD = not(blueCMD);  // reverse the blue button command
    if (blueCMD) { // if value is TRUE
      digitalWrite(blueLED,HIGH);
      Serial.println("blue light HIGH");
      delay(500);
    }
    else {
      digitalWrite(blueLED,LOW);
      Serial.println("blue light LOW");
      delay(500);
    }
  }
}
