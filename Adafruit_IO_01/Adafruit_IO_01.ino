#include <ESP8266WiFi.h>
#include "Adafruit_MQTT.h"
#include "Adafruit_MQTT_Client.h"
#include "DHT.h"

#define DHTPIN 5
#define DHTTYPE DHT22

// WiFi parameters
#define WLAN_SSID "Warp2"
#define WLAN_PASS "2e8e6tcvhh"

// Adafruit IO
#define AIO_SERVER "io.adafruit.com"
#define AIO_SERVERPORT 1883
#define AIO_USERNAME "tlharv"
#define AIO_KEY "AIO_KEY"

DHT dht(DHTPIN, DHTTYPE, 15);

const char TEMPERATURE_FEED[] PROGMEM = AIO_USERNAME "/feeds/temperature";


void setup() {
  // put your setup code here, to run once:
  dht.begin();
}

void loop() {
  // put your main code here, to run repeatedly:
  if(! mqtt.ping(3)) {
  // reconnect to adafruit io
  if(! mqtt.connected())
    connect();
  }  

  
  int humidity_data = (int)dht.readHumidity();
  int temperature_data = (int)dht.readTemperature();


}
