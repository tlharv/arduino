// Ref https://tttapa.github.io/ESP8266/Chap10%20-%20Simple%20Web%20Server.html

#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266mDNS.h>
#include <ESP8266WebServer.h>

ESP8266WiFiMulti wifiMulti; // Create instance of ESP8266WiFiMulti class

ESP8266WebServer server(80); // Create a webserver object that listens for HTTP request on port 80

const int LED = 12; // LED gets connected to Huzzah pin 2

// Create function prototypes for HTTP handlers
void handleRoot();
void handleNotFound();
void handleLED(); // new handler for the LED!

void setup(void)
{
  Serial.begin(9600);
  delay(10);

  // Set up LED pin
  pinMode(LED, OUTPUT);
  digitalWrite(LED,HIGH);

  // Set up WiFi connection
  wifiMulti.addAP("Warp2","z2xwSs6M");

  Serial.println("Connecting...");
  int i=0;
  while (wifiMulti.run() != WL_CONNECTED)
  {
    delay(250);
    Serial.print("*");
  }

  // When successful, print out info
  Serial.println("\n");
  Serial.print("Connected to "); Serial.println(WiFi.SSID());
  Serial.print("IP Address: "); Serial.println(WiFi.localIP());

  // Set up the mDNS responder for esp8266.local
  if (MDNS.begin("esp8266"))
  {
    Serial.println("mDNS responder started.");
  }
  else
  {
    Serial.println("Error setting up MDNS responder!");
  }

  // Set up handleRoot function when a client requests root URL /
  server.on("/", HTTP_GET, handleRoot);
  server.on("/LED", HTTP_POST, handleLED); // call the HandleLED function when a POST request is made to /LED
  server.onNotFound(handleNotFound); // when a client requests something other than /

  // Actually start the server
  server.begin();
  Serial.println("HTTP server started!");
}

void loop(void)
{
  // Continually check if new HTTP reuqest is received.
  // If handleClient detects new requests, it will automatically execute the right functions specified in setup.
  server.handleClient(); // Listen for HTTP requests from clients
}

// SUPPORTING FUNCTIONS

void handleRoot()
{
  // Send HTTP status 200 (ok) and send basic welcome text
  //server.send(200, "text/plain", "Ready Player One!");
  server.send(200, "text/html", "<form action=\"/LED\" method=\"POST\"><input type=\"submit\" value=\"Toggle LED\"></form>");
}

void handleNotFound()
{
  // Send HTTP status 404 (Not Found) when there's no handler for the URL in the request
  server.send(404, "text/plain", "404: Not found.");
}

void handleLED() {                          // If a POST request is made to URI /LED
  digitalWrite(LED,!digitalRead(LED));      // Change the state of the LED
  server.sendHeader("Location","/");        // Add a header to respond with a new location for the browser to go to the home page again
  server.send(303);                         // Send it back to the browser with an HTTP status 303 (See Other) to redirect
}
