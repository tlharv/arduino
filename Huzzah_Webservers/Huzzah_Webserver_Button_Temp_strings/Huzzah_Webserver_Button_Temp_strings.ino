// Ref https://tttapa.github.io/ESP8266/Chap10%20-%20Simple%20Web%20Server.html

#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266mDNS.h>
#include <ESP8266WebServer.h>
#include <Wire.h> // To use the Si7021 temp/humidity sensor
#include "NTPClient.h" // to get internet date and time
#include "WiFiUdp.h"    // to get internet date and time
#include "String.h" // Because we have to deal with strings

ESP8266WiFiMulti wifiMulti; // Create instance of ESP8266WiFiMulti class

ESP8266WebServer server(80); // Create a webserver object that listens for HTTP request on port 80

const int LED = 2; // Blue LED on Huzzah

#define si7021Addr 0x40

// Create function prototypes for HTTP handlers
void handleRoot();
void handleNotFound();
void handleReadings(); // new handler for the LED!

void setup(void)
{
  Serial.begin(9600);
  delay(10);

  // Set up LED pin
  pinMode(LED, OUTPUT);
  digitalWrite(LED,HIGH);  // Initial setup has the blue LED off

  // Set up communication with Si7021
  Wire.begin();
  Wire.beginTransmission(si7021Addr);
  Wire.endTransmission();
  delay(300);

  // Set up WiFi connection
  wifiMulti.addAP("Warp2","z2xwSs6M");

  Serial.println("Connecting...");
  int i=0;
  while (wifiMulti.run() != WL_CONNECTED)
  {
    delay(250);
    Serial.print("*");
  }

  // When successful, print out info
  Serial.println("\n");
  Serial.print("Connected to "); Serial.println(WiFi.SSID());
  Serial.print("IP Address: "); Serial.println(WiFi.localIP());

  // Set up the mDNS responder for esp8266.local
  if (MDNS.begin("esp8266"))
  {
    Serial.println("mDNS responder started.");
  }
  else
  {
    Serial.println("Error setting up MDNS responder!");
  }

  // Set up handleRoot function when a client requests root URL /
  server.on("/", HTTP_GET, handleRoot);
  server.on("/Readings", HTTP_POST, handleReadings); // call the HandleReadings function when a POST request is made to /Readings
  server.onNotFound(handleNotFound); // when a client requests something other than /

  // Actually start the server
  server.begin();
  Serial.println("HTTP server started!");
}

void loop(void)
{
  // Continually check if new HTTP request is received.
  // If handleClient detects new requests, it will automatically execute the right functions specified in setup.
  server.handleClient(); // Listen for HTTP requests from clients
}

// SUPPORTING FUNCTIONS

void handleRoot()
{
  // Send HTTP status 200 (ok) and send basic welcome text
  //server.send(200, "text/plain", "Ready Player One!");
  server.send(200, "text/html", "<form action=\"/Readings\" method=\"POST\"><input type=\"submit\" value=\"Get Readings\"></form>");
}

void handleNotFound()
{
  // Send HTTP status 404 (Not Found) when there's no handler for the URL in the request
  server.send(404, "text/plain", "404: Not found.");
}

void handleReadings() {                     // If a POST request is made to URI /Readings
  // Get info from Si7021 sensor
  unsigned int data[2];
  Wire.beginTransmission(si7021Addr);
  Wire.write(0xF5); // send humidity measurement command
  Wire.endTransmission();
  delay(500);
  // Request 2 bytes of data
  Wire.requestFrom(si7021Addr, 2);
  // Read 2 bytes of data to get humidity
  if(Wire.available() == 2)
  {
    data[0] = Wire.read();
    data[1] = Wire.read();
  }
 
  // Convert the data
  float humidity  = ((data[0] * 256.0) + data[1]);
  humidity = ((125 * humidity) / 65536.0) - 6;
 
  Wire.beginTransmission(si7021Addr);
  // Send temperature measurement command
  Wire.write(0xF3);
  Wire.endTransmission();
  delay(500);
 
  // Request 2 bytes of data
  Wire.requestFrom(si7021Addr, 2);
 
  // Read 2 bytes of data for temperature
  if(Wire.available() == 2)
  {
    data[0] = Wire.read();
    data[1] = Wire.read();
  }
 
  // Convert the data
  float temp  = ((data[0] * 256.0) + data[1]);
  float celsTemp = ((175.72 * temp) / 65536.0) - 46.85;
  float fahrTemp = celsTemp * 1.8 + 32;
  String StempF;
  String Shumid;
  
  String html = "<html><body><h1>Current Conditions</h1><p>Temperature = ";
    
  // convert the float values to string values to be used in the html script
  char fahrTemp_string[10]; // a buffer to hold the converted string
  char humid_string[10];
  snprintf(fahrTemp_string, sizeof(fahrTemp_string), "%6.1f", fahrTemp);
  snprintf(humid_string, sizeof(humid_string), "%6.1f", humidity);
  StempF = fahrTemp_string;
  Shumid = humid_string;
  html += fahrTemp_string;
  html += " deg F</p>";
  html += "<p>Humidity is ";
  html += Shumid;
  html += " % RH</p>";
  html += "</body></html>";
  Serial.println(html);
      
  server.send(200, "text/html", html);
  server.sendHeader("Location","/");        // Add a header to respond with a new location for the browser to go to the home page again
  server.send(303);                         // Send it back to the browser with an HTTP status 303 (See Other) to redirect
}
