/*********
  Rui Santos
  Complete project details at http://randomnerdtutorials.com  
  http://randomnerdtutorials.com/esp8266-web-server-with-arduino-ide/

        +-------------------------+
     TX |  O  O  GND              |
  CH_PD |  O  O  GPIO-2           |
  RESET |  O  O  GPIO-0           |
    VCC |  O  O  RX               |
        +-------------------------+

*********/

#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>

MDNSResponder mdns;

// Replace with your network credentials
const char* ssid = "Warp2";
const char* password = "z2xwSs6M";

String WebServerHeader = "ESP8266-07 Tester";
String Socket00Name = "GPIO-00: ";
String Socket02Name = "GPIO-02: ";
String Socket04Name = "GPIO-04: ";
String Socket05Name = "GPIO-05: ";
String Socket12Name = "GPIO-12: ";
String Socket13Name = "GPIO-13: ";
String Socket14Name = "GPIO-14: ";
String Socket15Name = "GPIO-15: ";
String Socket00Value = "  currently OFF";
String Socket02Value = "  currently OFF";
String Socket04Value = "  currently OFF";
String Socket05Value = "  currently OFF";
String Socket12Value = "  currently OFF";
String Socket13Value = "  currently OFF";
String Socket14Value = "  currently OFF";
String Socket15Value = "  currently OFF";
ESP8266WebServer server(80);

String webPage = "";

int gpio00_pin = 0;
int gpio02_pin = 2;
int gpio04_pin = 4;
int gpio05_pin = 5;
int gpio12_pin = 12;
int gpio13_pin = 13;
int gpio14_pin = 14;
int gpio15_pin = 15;

void setup(void){
  webPage += "<h1>" + WebServerHeader + "</h1>";
  webPage += "<p>" + Socket00Name + "<a href=\"socket00On\"><button>ON</button></a>&nbsp;<a href=\"socket00Off\"><button>OFF</button></a><label for='lblSoc00'>" + Socket00Value + "</label></p>";
  webPage += "<p>" + Socket02Name + "<a href=\"socket02On\"><button>ON</button></a>&nbsp;<a href=\"socket02Off\"><button>OFF</button></a><label for='lblSoc02'>" + Socket02Value + "</label></p>";
  webPage += "<p>" + Socket04Name + "<a href=\"socket04On\"><button>ON</button></a>&nbsp;<a href=\"socket04Off\"><button>OFF</button></a><label for='lblSoc04'>" + Socket04Value + "</label></p>";
  webPage += "<p>" + Socket05Name + "<a href=\"socket05On\"><button>ON</button></a>&nbsp;<a href=\"socket05Off\"><button>OFF</button></a><label for='lblSoc05'>" + Socket05Value + "</label></p>";
  webPage += "<p>" + Socket12Name + "<a href=\"socket12On\"><button>ON</button></a>&nbsp;<a href=\"socket12Off\"><button>OFF</button></a><label for='lblSoc12'>" + Socket12Value + "</label></p>";
  webPage += "<p>" + Socket13Name + "<a href=\"socket13On\"><button>ON</button></a>&nbsp;<a href=\"socket13Off\"><button>OFF</button></a><label for='lblSoc13'>" + Socket13Value + "</label></p>";
  webPage += "<p>" + Socket14Name + "<a href=\"socket14On\"><button>ON</button></a>&nbsp;<a href=\"socket14Off\"><button>OFF</button></a><label for='lblSoc14'>" + Socket14Value + "</label></p>";
  webPage += "<p>" + Socket15Name + "<a href=\"socket15On\"><button>ON</button></a>&nbsp;<a href=\"socket15Off\"><button>OFF</button></a><label for='lblSoc15'>" + Socket15Value + "</label></p>";
  
  // preparing GPIOs
  pinMode(gpio00_pin, OUTPUT);
  digitalWrite(gpio00_pin, LOW);
  pinMode(gpio02_pin, OUTPUT);
  digitalWrite(gpio02_pin, LOW);
  pinMode(gpio04_pin, OUTPUT);
  digitalWrite(gpio04_pin, LOW);
  pinMode(gpio05_pin, OUTPUT);
  digitalWrite(gpio05_pin, LOW);
  pinMode(gpio12_pin, OUTPUT);
  digitalWrite(gpio12_pin, LOW);
  pinMode(gpio13_pin, OUTPUT);
  digitalWrite(gpio13_pin, LOW);
  pinMode(gpio14_pin, OUTPUT);
  digitalWrite(gpio14_pin, LOW);
  pinMode(gpio15_pin, OUTPUT);
  digitalWrite(gpio15_pin, LOW);
  
  delay(1000);
  Serial.begin(115200);
  WiFi.begin(ssid, password);

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  
  if (mdns.begin("esp8266", WiFi.localIP())) {
    // Serial.println("MDNS responder started");
  }
  
  server.on("/", [](){
    server.send(200, "text/html", webPage);
  });
  
  server.on("/socket00On", [](){
    Socket00Value = "  currently ON";
    webPage = "<h1>" + WebServerHeader + "</h1>";
    webPage += "<p>" + Socket00Name + "<a href=\"socket00On\"><button>ON</button></a>&nbsp;<a href=\"socket00Off\"><button>OFF</button></a><label for='lblSoc00'>" + Socket00Value + "</label></p>";
    webPage += "<p>" + Socket02Name + "<a href=\"socket02On\"><button>ON</button></a>&nbsp;<a href=\"socket02Off\"><button>OFF</button></a><label for='lblSoc02'>" + Socket02Value + "</label></p>";
    webPage += "<p>" + Socket04Name + "<a href=\"socket04On\"><button>ON</button></a>&nbsp;<a href=\"socket04Off\"><button>OFF</button></a><label for='lblSoc04'>" + Socket04Value + "</label></p>";
    webPage += "<p>" + Socket05Name + "<a href=\"socket05On\"><button>ON</button></a>&nbsp;<a href=\"socket05Off\"><button>OFF</button></a><label for='lblSoc05'>" + Socket05Value + "</label></p>";
    webPage += "<p>" + Socket12Name + "<a href=\"socket12On\"><button>ON</button></a>&nbsp;<a href=\"socket12Off\"><button>OFF</button></a><label for='lblSoc12'>" + Socket12Value + "</label></p>";
    webPage += "<p>" + Socket13Name + "<a href=\"socket13On\"><button>ON</button></a>&nbsp;<a href=\"socket13Off\"><button>OFF</button></a><label for='lblSoc13'>" + Socket13Value + "</label></p>";
    webPage += "<p>" + Socket14Name + "<a href=\"socket14On\"><button>ON</button></a>&nbsp;<a href=\"socket14Off\"><button>OFF</button></a><label for='lblSoc14'>" + Socket14Value + "</label></p>";
    webPage += "<p>" + Socket15Name + "<a href=\"socket15On\"><button>ON</button></a>&nbsp;<a href=\"socket15Off\"><button>OFF</button></a><label for='lblSoc15'>" + Socket15Value + "</label></p>";
    server.send(200, "text/html", webPage);
    digitalWrite(gpio00_pin, HIGH);
    delay(1000);
  });
  server.on("/socket02On", [](){
    Socket02Value = "  currently ON";
    webPage = "<h1>" + WebServerHeader + "</h1>";
    webPage += "<p>" + Socket00Name + "<a href=\"socket00On\"><button>ON</button></a>&nbsp;<a href=\"socket00Off\"><button>OFF</button></a><label for='lblSoc00'>" + Socket00Value + "</label></p>";
    webPage += "<p>" + Socket02Name + "<a href=\"socket02On\"><button>ON</button></a>&nbsp;<a href=\"socket02Off\"><button>OFF</button></a><label for='lblSoc02'>" + Socket02Value + "</label></p>";
    webPage += "<p>" + Socket04Name + "<a href=\"socket04On\"><button>ON</button></a>&nbsp;<a href=\"socket04Off\"><button>OFF</button></a><label for='lblSoc04'>" + Socket04Value + "</label></p>";
    webPage += "<p>" + Socket05Name + "<a href=\"socket05On\"><button>ON</button></a>&nbsp;<a href=\"socket05Off\"><button>OFF</button></a><label for='lblSoc05'>" + Socket05Value + "</label></p>";
    webPage += "<p>" + Socket12Name + "<a href=\"socket12On\"><button>ON</button></a>&nbsp;<a href=\"socket12Off\"><button>OFF</button></a><label for='lblSoc12'>" + Socket12Value + "</label></p>";
    webPage += "<p>" + Socket13Name + "<a href=\"socket13On\"><button>ON</button></a>&nbsp;<a href=\"socket13Off\"><button>OFF</button></a><label for='lblSoc13'>" + Socket13Value + "</label></p>";
    webPage += "<p>" + Socket14Name + "<a href=\"socket14On\"><button>ON</button></a>&nbsp;<a href=\"socket14Off\"><button>OFF</button></a><label for='lblSoc14'>" + Socket14Value + "</label></p>";
    webPage += "<p>" + Socket15Name + "<a href=\"socket15On\"><button>ON</button></a>&nbsp;<a href=\"socket15Off\"><button>OFF</button></a><label for='lblSoc15'>" + Socket15Value + "</label></p>";
    server.send(200, "text/html", webPage);
    digitalWrite(gpio02_pin, HIGH);
    delay(1000); 
  });
  server.on("/socket04On", [](){
    Socket04Value = "  currently ON";
    webPage = "<h1>" + WebServerHeader + "</h1>";
    webPage += "<p>" + Socket00Name + "<a href=\"socket00On\"><button>ON</button></a>&nbsp;<a href=\"socket00Off\"><button>OFF</button></a><label for='lblSoc00'>" + Socket00Value + "</label></p>";
    webPage += "<p>" + Socket02Name + "<a href=\"socket02On\"><button>ON</button></a>&nbsp;<a href=\"socket02Off\"><button>OFF</button></a><label for='lblSoc02'>" + Socket02Value + "</label></p>";
    webPage += "<p>" + Socket04Name + "<a href=\"socket04On\"><button>ON</button></a>&nbsp;<a href=\"socket04Off\"><button>OFF</button></a><label for='lblSoc04'>" + Socket04Value + "</label></p>";
    webPage += "<p>" + Socket05Name + "<a href=\"socket05On\"><button>ON</button></a>&nbsp;<a href=\"socket05Off\"><button>OFF</button></a><label for='lblSoc05'>" + Socket05Value + "</label></p>";
    webPage += "<p>" + Socket12Name + "<a href=\"socket12On\"><button>ON</button></a>&nbsp;<a href=\"socket12Off\"><button>OFF</button></a><label for='lblSoc12'>" + Socket12Value + "</label></p>";
    webPage += "<p>" + Socket13Name + "<a href=\"socket13On\"><button>ON</button></a>&nbsp;<a href=\"socket13Off\"><button>OFF</button></a><label for='lblSoc13'>" + Socket13Value + "</label></p>";
    webPage += "<p>" + Socket14Name + "<a href=\"socket14On\"><button>ON</button></a>&nbsp;<a href=\"socket14Off\"><button>OFF</button></a><label for='lblSoc14'>" + Socket14Value + "</label></p>";
    webPage += "<p>" + Socket15Name + "<a href=\"socket15On\"><button>ON</button></a>&nbsp;<a href=\"socket15Off\"><button>OFF</button></a><label for='lblSoc15'>" + Socket15Value + "</label></p>";
    server.send(200, "text/html", webPage);
    digitalWrite(gpio04_pin, HIGH);
    delay(1000);
  });
  server.on("/socket05On", [](){
    Socket05Value = "  currently ON";
    webPage = "<h1>" + WebServerHeader + "</h1>";
    webPage += "<p>" + Socket00Name + "<a href=\"socket00On\"><button>ON</button></a>&nbsp;<a href=\"socket00Off\"><button>OFF</button></a><label for='lblSoc00'>" + Socket00Value + "</label></p>";
    webPage += "<p>" + Socket02Name + "<a href=\"socket02On\"><button>ON</button></a>&nbsp;<a href=\"socket02Off\"><button>OFF</button></a><label for='lblSoc02'>" + Socket02Value + "</label></p>";
    webPage += "<p>" + Socket04Name + "<a href=\"socket04On\"><button>ON</button></a>&nbsp;<a href=\"socket04Off\"><button>OFF</button></a><label for='lblSoc04'>" + Socket04Value + "</label></p>";
    webPage += "<p>" + Socket05Name + "<a href=\"socket05On\"><button>ON</button></a>&nbsp;<a href=\"socket05Off\"><button>OFF</button></a><label for='lblSoc05'>" + Socket05Value + "</label></p>";
    webPage += "<p>" + Socket12Name + "<a href=\"socket12On\"><button>ON</button></a>&nbsp;<a href=\"socket12Off\"><button>OFF</button></a><label for='lblSoc12'>" + Socket12Value + "</label></p>";
    webPage += "<p>" + Socket13Name + "<a href=\"socket13On\"><button>ON</button></a>&nbsp;<a href=\"socket13Off\"><button>OFF</button></a><label for='lblSoc13'>" + Socket13Value + "</label></p>";
    webPage += "<p>" + Socket14Name + "<a href=\"socket14On\"><button>ON</button></a>&nbsp;<a href=\"socket14Off\"><button>OFF</button></a><label for='lblSoc14'>" + Socket14Value + "</label></p>";
    webPage += "<p>" + Socket15Name + "<a href=\"socket15On\"><button>ON</button></a>&nbsp;<a href=\"socket15Off\"><button>OFF</button></a><label for='lblSoc15'>" + Socket15Value + "</label></p>";
    server.send(200, "text/html", webPage);
    digitalWrite(gpio05_pin, HIGH);
    delay(1000); 
  });

  server.on("/socket12On", [](){
    Socket12Value = "  currently ON";
    webPage = "<h1>" + WebServerHeader + "</h1>";
    webPage += "<p>" + Socket00Name + "<a href=\"socket00On\"><button>ON</button></a>&nbsp;<a href=\"socket00Off\"><button>OFF</button></a><label for='lblSoc00'>" + Socket00Value + "</label></p>";
    webPage += "<p>" + Socket02Name + "<a href=\"socket02On\"><button>ON</button></a>&nbsp;<a href=\"socket02Off\"><button>OFF</button></a><label for='lblSoc02'>" + Socket02Value + "</label></p>";
    webPage += "<p>" + Socket04Name + "<a href=\"socket04On\"><button>ON</button></a>&nbsp;<a href=\"socket04Off\"><button>OFF</button></a><label for='lblSoc04'>" + Socket04Value + "</label></p>";
    webPage += "<p>" + Socket05Name + "<a href=\"socket05On\"><button>ON</button></a>&nbsp;<a href=\"socket05Off\"><button>OFF</button></a><label for='lblSoc05'>" + Socket05Value + "</label></p>";
    webPage += "<p>" + Socket12Name + "<a href=\"socket12On\"><button>ON</button></a>&nbsp;<a href=\"socket12Off\"><button>OFF</button></a><label for='lblSoc12'>" + Socket12Value + "</label></p>";
    webPage += "<p>" + Socket13Name + "<a href=\"socket13On\"><button>ON</button></a>&nbsp;<a href=\"socket13Off\"><button>OFF</button></a><label for='lblSoc13'>" + Socket13Value + "</label></p>";
    webPage += "<p>" + Socket14Name + "<a href=\"socket14On\"><button>ON</button></a>&nbsp;<a href=\"socket14Off\"><button>OFF</button></a><label for='lblSoc14'>" + Socket14Value + "</label></p>";
    webPage += "<p>" + Socket15Name + "<a href=\"socket15On\"><button>ON</button></a>&nbsp;<a href=\"socket15Off\"><button>OFF</button></a><label for='lblSoc15'>" + Socket15Value + "</label></p>";
    server.send(200, "text/html", webPage);
    digitalWrite(gpio12_pin, HIGH);
    delay(1000);
  });
  server.on("/socket13On", [](){
    Socket13Value = "  currently ON";
    webPage = "<h1>" + WebServerHeader + "</h1>";
    webPage += "<p>" + Socket00Name + "<a href=\"socket00On\"><button>ON</button></a>&nbsp;<a href=\"socket00Off\"><button>OFF</button></a><label for='lblSoc00'>" + Socket00Value + "</label></p>";
    webPage += "<p>" + Socket02Name + "<a href=\"socket02On\"><button>ON</button></a>&nbsp;<a href=\"socket02Off\"><button>OFF</button></a><label for='lblSoc02'>" + Socket02Value + "</label></p>";
    webPage += "<p>" + Socket04Name + "<a href=\"socket04On\"><button>ON</button></a>&nbsp;<a href=\"socket04Off\"><button>OFF</button></a><label for='lblSoc04'>" + Socket04Value + "</label></p>";
    webPage += "<p>" + Socket05Name + "<a href=\"socket05On\"><button>ON</button></a>&nbsp;<a href=\"socket05Off\"><button>OFF</button></a><label for='lblSoc05'>" + Socket05Value + "</label></p>";
    webPage += "<p>" + Socket12Name + "<a href=\"socket12On\"><button>ON</button></a>&nbsp;<a href=\"socket12Off\"><button>OFF</button></a><label for='lblSoc12'>" + Socket12Value + "</label></p>";
    webPage += "<p>" + Socket13Name + "<a href=\"socket13On\"><button>ON</button></a>&nbsp;<a href=\"socket13Off\"><button>OFF</button></a><label for='lblSoc13'>" + Socket13Value + "</label></p>";
    webPage += "<p>" + Socket14Name + "<a href=\"socket14On\"><button>ON</button></a>&nbsp;<a href=\"socket14Off\"><button>OFF</button></a><label for='lblSoc14'>" + Socket14Value + "</label></p>";
    webPage += "<p>" + Socket15Name + "<a href=\"socket15On\"><button>ON</button></a>&nbsp;<a href=\"socket15Off\"><button>OFF</button></a><label for='lblSoc15'>" + Socket15Value + "</label></p>";
    server.send(200, "text/html", webPage);
    digitalWrite(gpio13_pin, HIGH);
    delay(1000); 
  });
  server.on("/socket14On", [](){
    Socket14Value = "  currently ON";
    webPage = "<h1>" + WebServerHeader + "</h1>";
    webPage += "<p>" + Socket00Name + "<a href=\"socket00On\"><button>ON</button></a>&nbsp;<a href=\"socket00Off\"><button>OFF</button></a><label for='lblSoc00'>" + Socket00Value + "</label></p>";
    webPage += "<p>" + Socket02Name + "<a href=\"socket02On\"><button>ON</button></a>&nbsp;<a href=\"socket02Off\"><button>OFF</button></a><label for='lblSoc02'>" + Socket02Value + "</label></p>";
    webPage += "<p>" + Socket04Name + "<a href=\"socket04On\"><button>ON</button></a>&nbsp;<a href=\"socket04Off\"><button>OFF</button></a><label for='lblSoc04'>" + Socket04Value + "</label></p>";
    webPage += "<p>" + Socket05Name + "<a href=\"socket05On\"><button>ON</button></a>&nbsp;<a href=\"socket05Off\"><button>OFF</button></a><label for='lblSoc05'>" + Socket05Value + "</label></p>";
    webPage += "<p>" + Socket12Name + "<a href=\"socket12On\"><button>ON</button></a>&nbsp;<a href=\"socket12Off\"><button>OFF</button></a><label for='lblSoc12'>" + Socket12Value + "</label></p>";
    webPage += "<p>" + Socket13Name + "<a href=\"socket13On\"><button>ON</button></a>&nbsp;<a href=\"socket13Off\"><button>OFF</button></a><label for='lblSoc13'>" + Socket13Value + "</label></p>";
    webPage += "<p>" + Socket14Name + "<a href=\"socket14On\"><button>ON</button></a>&nbsp;<a href=\"socket14Off\"><button>OFF</button></a><label for='lblSoc14'>" + Socket14Value + "</label></p>";
    webPage += "<p>" + Socket15Name + "<a href=\"socket15On\"><button>ON</button></a>&nbsp;<a href=\"socket15Off\"><button>OFF</button></a><label for='lblSoc15'>" + Socket15Value + "</label></p>";
    server.send(200, "text/html", webPage);
    digitalWrite(gpio14_pin, HIGH);
    delay(1000);
  });
  server.on("/socket15On", [](){
    Socket15Value = "  currently ON";
    webPage = "<h1>" + WebServerHeader + "</h1>";
    webPage += "<p>" + Socket00Name + "<a href=\"socket00On\"><button>ON</button></a>&nbsp;<a href=\"socket00Off\"><button>OFF</button></a><label for='lblSoc00'>" + Socket00Value + "</label></p>";
    webPage += "<p>" + Socket02Name + "<a href=\"socket02On\"><button>ON</button></a>&nbsp;<a href=\"socket02Off\"><button>OFF</button></a><label for='lblSoc02'>" + Socket02Value + "</label></p>";
    webPage += "<p>" + Socket04Name + "<a href=\"socket04On\"><button>ON</button></a>&nbsp;<a href=\"socket04Off\"><button>OFF</button></a><label for='lblSoc04'>" + Socket04Value + "</label></p>";
    webPage += "<p>" + Socket05Name + "<a href=\"socket05On\"><button>ON</button></a>&nbsp;<a href=\"socket05Off\"><button>OFF</button></a><label for='lblSoc05'>" + Socket05Value + "</label></p>";
    webPage += "<p>" + Socket12Name + "<a href=\"socket12On\"><button>ON</button></a>&nbsp;<a href=\"socket12Off\"><button>OFF</button></a><label for='lblSoc12'>" + Socket12Value + "</label></p>";
    webPage += "<p>" + Socket13Name + "<a href=\"socket13On\"><button>ON</button></a>&nbsp;<a href=\"socket13Off\"><button>OFF</button></a><label for='lblSoc13'>" + Socket13Value + "</label></p>";
    webPage += "<p>" + Socket14Name + "<a href=\"socket14On\"><button>ON</button></a>&nbsp;<a href=\"socket14Off\"><button>OFF</button></a><label for='lblSoc14'>" + Socket14Value + "</label></p>";
    webPage += "<p>" + Socket15Name + "<a href=\"socket15On\"><button>ON</button></a>&nbsp;<a href=\"socket15Off\"><button>OFF</button></a><label for='lblSoc15'>" + Socket15Value + "</label></p>";
    server.send(200, "text/html", webPage);
    digitalWrite(gpio15_pin, HIGH);
    delay(1000); 
  });

//======================

  server.on("/socket00Off", [](){
    Socket00Value = "  currently OFF";
    webPage = "<h1>" + WebServerHeader + "</h1>";
    webPage += "<p>" + Socket00Name + "<a href=\"socket00On\"><button>ON</button></a>&nbsp;<a href=\"socket00Off\"><button>OFF</button></a><label for='lblSoc00'>" + Socket00Value + "</label></p>";
    webPage += "<p>" + Socket02Name + "<a href=\"socket02On\"><button>ON</button></a>&nbsp;<a href=\"socket02Off\"><button>OFF</button></a><label for='lblSoc02'>" + Socket02Value + "</label></p>";
    webPage += "<p>" + Socket04Name + "<a href=\"socket04On\"><button>ON</button></a>&nbsp;<a href=\"socket04Off\"><button>OFF</button></a><label for='lblSoc04'>" + Socket04Value + "</label></p>";
    webPage += "<p>" + Socket05Name + "<a href=\"socket05On\"><button>ON</button></a>&nbsp;<a href=\"socket05Off\"><button>OFF</button></a><label for='lblSoc05'>" + Socket05Value + "</label></p>";
    webPage += "<p>" + Socket12Name + "<a href=\"socket12On\"><button>ON</button></a>&nbsp;<a href=\"socket12Off\"><button>OFF</button></a><label for='lblSoc12'>" + Socket12Value + "</label></p>";
    webPage += "<p>" + Socket13Name + "<a href=\"socket13On\"><button>ON</button></a>&nbsp;<a href=\"socket13Off\"><button>OFF</button></a><label for='lblSoc13'>" + Socket13Value + "</label></p>";
    webPage += "<p>" + Socket14Name + "<a href=\"socket14On\"><button>ON</button></a>&nbsp;<a href=\"socket14Off\"><button>OFF</button></a><label for='lblSoc14'>" + Socket14Value + "</label></p>";
    webPage += "<p>" + Socket15Name + "<a href=\"socket15On\"><button>ON</button></a>&nbsp;<a href=\"socket15Off\"><button>OFF</button></a><label for='lblSoc15'>" + Socket15Value + "</label></p>";
    server.send(200, "text/html", webPage);
    digitalWrite(gpio00_pin, LOW);
    delay(1000);
  });
  server.on("/socket02Off", [](){
    Socket02Value = "  currently OFF";
    webPage = "<h1>" + WebServerHeader + "</h1>";
    webPage += "<p>" + Socket00Name + "<a href=\"socket00On\"><button>ON</button></a>&nbsp;<a href=\"socket00Off\"><button>OFF</button></a><label for='lblSoc00'>" + Socket00Value + "</label></p>";
    webPage += "<p>" + Socket02Name + "<a href=\"socket02On\"><button>ON</button></a>&nbsp;<a href=\"socket02Off\"><button>OFF</button></a><label for='lblSoc02'>" + Socket02Value + "</label></p>";
    webPage += "<p>" + Socket04Name + "<a href=\"socket04On\"><button>ON</button></a>&nbsp;<a href=\"socket04Off\"><button>OFF</button></a><label for='lblSoc04'>" + Socket04Value + "</label></p>";
    webPage += "<p>" + Socket05Name + "<a href=\"socket05On\"><button>ON</button></a>&nbsp;<a href=\"socket05Off\"><button>OFF</button></a><label for='lblSoc05'>" + Socket05Value + "</label></p>";
    webPage += "<p>" + Socket12Name + "<a href=\"socket12On\"><button>ON</button></a>&nbsp;<a href=\"socket12Off\"><button>OFF</button></a><label for='lblSoc12'>" + Socket12Value + "</label></p>";
    webPage += "<p>" + Socket13Name + "<a href=\"socket13On\"><button>ON</button></a>&nbsp;<a href=\"socket13Off\"><button>OFF</button></a><label for='lblSoc13'>" + Socket13Value + "</label></p>";
    webPage += "<p>" + Socket14Name + "<a href=\"socket14On\"><button>ON</button></a>&nbsp;<a href=\"socket14Off\"><button>OFF</button></a><label for='lblSoc14'>" + Socket14Value + "</label></p>";
    webPage += "<p>" + Socket15Name + "<a href=\"socket15On\"><button>ON</button></a>&nbsp;<a href=\"socket15Off\"><button>OFF</button></a><label for='lblSoc15'>" + Socket15Value + "</label></p>";
    server.send(200, "text/html", webPage);
    digitalWrite(gpio02_pin, LOW);
    delay(1000); 
  });
  server.on("/socket04Off", [](){
    Socket04Value = "  currently OFF";
    webPage = "<h1>" + WebServerHeader + "</h1>";
    webPage += "<p>" + Socket00Name + "<a href=\"socket00On\"><button>ON</button></a>&nbsp;<a href=\"socket00Off\"><button>OFF</button></a><label for='lblSoc00'>" + Socket00Value + "</label></p>";
    webPage += "<p>" + Socket02Name + "<a href=\"socket02On\"><button>ON</button></a>&nbsp;<a href=\"socket02Off\"><button>OFF</button></a><label for='lblSoc02'>" + Socket02Value + "</label></p>";
    webPage += "<p>" + Socket04Name + "<a href=\"socket04On\"><button>ON</button></a>&nbsp;<a href=\"socket04Off\"><button>OFF</button></a><label for='lblSoc04'>" + Socket04Value + "</label></p>";
    webPage += "<p>" + Socket05Name + "<a href=\"socket05On\"><button>ON</button></a>&nbsp;<a href=\"socket05Off\"><button>OFF</button></a><label for='lblSoc05'>" + Socket05Value + "</label></p>";
    webPage += "<p>" + Socket12Name + "<a href=\"socket12On\"><button>ON</button></a>&nbsp;<a href=\"socket12Off\"><button>OFF</button></a><label for='lblSoc12'>" + Socket12Value + "</label></p>";
    webPage += "<p>" + Socket13Name + "<a href=\"socket13On\"><button>ON</button></a>&nbsp;<a href=\"socket13Off\"><button>OFF</button></a><label for='lblSoc13'>" + Socket13Value + "</label></p>";
    webPage += "<p>" + Socket14Name + "<a href=\"socket14On\"><button>ON</button></a>&nbsp;<a href=\"socket14Off\"><button>OFF</button></a><label for='lblSoc14'>" + Socket14Value + "</label></p>";
    webPage += "<p>" + Socket15Name + "<a href=\"socket15On\"><button>ON</button></a>&nbsp;<a href=\"socket15Off\"><button>OFF</button></a><label for='lblSoc15'>" + Socket15Value + "</label></p>";
    server.send(200, "text/html", webPage);
    digitalWrite(gpio04_pin, LOW);
    delay(1000);
  });
  server.on("/socket05Off", [](){
    Socket05Value = "  currently OFF";
    webPage = "<h1>" + WebServerHeader + "</h1>";
    webPage += "<p>" + Socket00Name + "<a href=\"socket00On\"><button>ON</button></a>&nbsp;<a href=\"socket00Off\"><button>OFF</button></a><label for='lblSoc00'>" + Socket00Value + "</label></p>";
    webPage += "<p>" + Socket02Name + "<a href=\"socket02On\"><button>ON</button></a>&nbsp;<a href=\"socket02Off\"><button>OFF</button></a><label for='lblSoc02'>" + Socket02Value + "</label></p>";
    webPage += "<p>" + Socket04Name + "<a href=\"socket04On\"><button>ON</button></a>&nbsp;<a href=\"socket04Off\"><button>OFF</button></a><label for='lblSoc04'>" + Socket04Value + "</label></p>";
    webPage += "<p>" + Socket05Name + "<a href=\"socket05On\"><button>ON</button></a>&nbsp;<a href=\"socket05Off\"><button>OFF</button></a><label for='lblSoc05'>" + Socket05Value + "</label></p>";
    webPage += "<p>" + Socket12Name + "<a href=\"socket12On\"><button>ON</button></a>&nbsp;<a href=\"socket12Off\"><button>OFF</button></a><label for='lblSoc12'>" + Socket12Value + "</label></p>";
    webPage += "<p>" + Socket13Name + "<a href=\"socket13On\"><button>ON</button></a>&nbsp;<a href=\"socket13Off\"><button>OFF</button></a><label for='lblSoc13'>" + Socket13Value + "</label></p>";
    webPage += "<p>" + Socket14Name + "<a href=\"socket14On\"><button>ON</button></a>&nbsp;<a href=\"socket14Off\"><button>OFF</button></a><label for='lblSoc14'>" + Socket14Value + "</label></p>";
    webPage += "<p>" + Socket15Name + "<a href=\"socket15On\"><button>ON</button></a>&nbsp;<a href=\"socket15Off\"><button>OFF</button></a><label for='lblSoc15'>" + Socket15Value + "</label></p>";
    server.send(200, "text/html", webPage);
    digitalWrite(gpio05_pin, LOW);
    delay(1000); 
  });

  server.on("/socket12Off", [](){
    Socket12Value = "  currently OFF";
    webPage = "<h1>" + WebServerHeader + "</h1>";
    webPage += "<p>" + Socket00Name + "<a href=\"socket00On\"><button>ON</button></a>&nbsp;<a href=\"socket00Off\"><button>OFF</button></a><label for='lblSoc00'>" + Socket00Value + "</label></p>";
    webPage += "<p>" + Socket02Name + "<a href=\"socket02On\"><button>ON</button></a>&nbsp;<a href=\"socket02Off\"><button>OFF</button></a><label for='lblSoc02'>" + Socket02Value + "</label></p>";
    webPage += "<p>" + Socket04Name + "<a href=\"socket04On\"><button>ON</button></a>&nbsp;<a href=\"socket04Off\"><button>OFF</button></a><label for='lblSoc04'>" + Socket04Value + "</label></p>";
    webPage += "<p>" + Socket05Name + "<a href=\"socket05On\"><button>ON</button></a>&nbsp;<a href=\"socket05Off\"><button>OFF</button></a><label for='lblSoc05'>" + Socket05Value + "</label></p>";
    webPage += "<p>" + Socket12Name + "<a href=\"socket12On\"><button>ON</button></a>&nbsp;<a href=\"socket12Off\"><button>OFF</button></a><label for='lblSoc12'>" + Socket12Value + "</label></p>";
    webPage += "<p>" + Socket13Name + "<a href=\"socket13On\"><button>ON</button></a>&nbsp;<a href=\"socket13Off\"><button>OFF</button></a><label for='lblSoc13'>" + Socket13Value + "</label></p>";
    webPage += "<p>" + Socket14Name + "<a href=\"socket14On\"><button>ON</button></a>&nbsp;<a href=\"socket14Off\"><button>OFF</button></a><label for='lblSoc14'>" + Socket14Value + "</label></p>";
    webPage += "<p>" + Socket15Name + "<a href=\"socket15On\"><button>ON</button></a>&nbsp;<a href=\"socket15Off\"><button>OFF</button></a><label for='lblSoc15'>" + Socket15Value + "</label></p>";
    server.send(200, "text/html", webPage);
    digitalWrite(gpio12_pin, LOW);
    delay(1000);
  });
  server.on("/socket13Off", [](){
    Socket13Value = "  currently OFF";
    webPage = "<h1>" + WebServerHeader + "</h1>";
    webPage += "<p>" + Socket00Name + "<a href=\"socket00On\"><button>ON</button></a>&nbsp;<a href=\"socket00Off\"><button>OFF</button></a><label for='lblSoc00'>" + Socket00Value + "</label></p>";
    webPage += "<p>" + Socket02Name + "<a href=\"socket02On\"><button>ON</button></a>&nbsp;<a href=\"socket02Off\"><button>OFF</button></a><label for='lblSoc02'>" + Socket02Value + "</label></p>";
    webPage += "<p>" + Socket04Name + "<a href=\"socket04On\"><button>ON</button></a>&nbsp;<a href=\"socket04Off\"><button>OFF</button></a><label for='lblSoc04'>" + Socket04Value + "</label></p>";
    webPage += "<p>" + Socket05Name + "<a href=\"socket05On\"><button>ON</button></a>&nbsp;<a href=\"socket05Off\"><button>OFF</button></a><label for='lblSoc05'>" + Socket05Value + "</label></p>";
    webPage += "<p>" + Socket12Name + "<a href=\"socket12On\"><button>ON</button></a>&nbsp;<a href=\"socket12Off\"><button>OFF</button></a><label for='lblSoc12'>" + Socket12Value + "</label></p>";
    webPage += "<p>" + Socket13Name + "<a href=\"socket13On\"><button>ON</button></a>&nbsp;<a href=\"socket13Off\"><button>OFF</button></a><label for='lblSoc13'>" + Socket13Value + "</label></p>";
    webPage += "<p>" + Socket14Name + "<a href=\"socket14On\"><button>ON</button></a>&nbsp;<a href=\"socket14Off\"><button>OFF</button></a><label for='lblSoc14'>" + Socket14Value + "</label></p>";
    webPage += "<p>" + Socket15Name + "<a href=\"socket15On\"><button>ON</button></a>&nbsp;<a href=\"socket15Off\"><button>OFF</button></a><label for='lblSoc15'>" + Socket15Value + "</label></p>";
    server.send(200, "text/html", webPage);
    digitalWrite(gpio13_pin, LOW);
    delay(1000); 
  });
  server.on("/socket14Off", [](){
    Socket14Value = "  currently OFF";
    webPage = "<h1>" + WebServerHeader + "</h1>";
    webPage += "<p>" + Socket00Name + "<a href=\"socket00On\"><button>ON</button></a>&nbsp;<a href=\"socket00Off\"><button>OFF</button></a><label for='lblSoc00'>" + Socket00Value + "</label></p>";
    webPage += "<p>" + Socket02Name + "<a href=\"socket02On\"><button>ON</button></a>&nbsp;<a href=\"socket02Off\"><button>OFF</button></a><label for='lblSoc02'>" + Socket02Value + "</label></p>";
    webPage += "<p>" + Socket04Name + "<a href=\"socket04On\"><button>ON</button></a>&nbsp;<a href=\"socket04Off\"><button>OFF</button></a><label for='lblSoc04'>" + Socket04Value + "</label></p>";
    webPage += "<p>" + Socket05Name + "<a href=\"socket05On\"><button>ON</button></a>&nbsp;<a href=\"socket05Off\"><button>OFF</button></a><label for='lblSoc05'>" + Socket05Value + "</label></p>";
    webPage += "<p>" + Socket12Name + "<a href=\"socket12On\"><button>ON</button></a>&nbsp;<a href=\"socket12Off\"><button>OFF</button></a><label for='lblSoc12'>" + Socket12Value + "</label></p>";
    webPage += "<p>" + Socket13Name + "<a href=\"socket13On\"><button>ON</button></a>&nbsp;<a href=\"socket13Off\"><button>OFF</button></a><label for='lblSoc13'>" + Socket13Value + "</label></p>";
    webPage += "<p>" + Socket14Name + "<a href=\"socket14On\"><button>ON</button></a>&nbsp;<a href=\"socket14Off\"><button>OFF</button></a><label for='lblSoc14'>" + Socket14Value + "</label></p>";
    webPage += "<p>" + Socket15Name + "<a href=\"socket15On\"><button>ON</button></a>&nbsp;<a href=\"socket15Off\"><button>OFF</button></a><label for='lblSoc15'>" + Socket15Value + "</label></p>";
    server.send(200, "text/html", webPage);
    digitalWrite(gpio14_pin, LOW);
    delay(1000);
  });
  server.on("/socket15Off", [](){
    Socket15Value = "  currently OFF";
    webPage = "<h1>" + WebServerHeader + "</h1>";
    webPage += "<p>" + Socket00Name + "<a href=\"socket00On\"><button>ON</button></a>&nbsp;<a href=\"socket00Off\"><button>OFF</button></a><label for='lblSoc00'>" + Socket00Value + "</label></p>";
    webPage += "<p>" + Socket02Name + "<a href=\"socket02On\"><button>ON</button></a>&nbsp;<a href=\"socket02Off\"><button>OFF</button></a><label for='lblSoc02'>" + Socket02Value + "</label></p>";
    webPage += "<p>" + Socket04Name + "<a href=\"socket04On\"><button>ON</button></a>&nbsp;<a href=\"socket04Off\"><button>OFF</button></a><label for='lblSoc04'>" + Socket04Value + "</label></p>";
    webPage += "<p>" + Socket05Name + "<a href=\"socket05On\"><button>ON</button></a>&nbsp;<a href=\"socket05Off\"><button>OFF</button></a><label for='lblSoc05'>" + Socket05Value + "</label></p>";
    webPage += "<p>" + Socket12Name + "<a href=\"socket12On\"><button>ON</button></a>&nbsp;<a href=\"socket12Off\"><button>OFF</button></a><label for='lblSoc12'>" + Socket12Value + "</label></p>";
    webPage += "<p>" + Socket13Name + "<a href=\"socket13On\"><button>ON</button></a>&nbsp;<a href=\"socket13Off\"><button>OFF</button></a><label for='lblSoc13'>" + Socket13Value + "</label></p>";
    webPage += "<p>" + Socket14Name + "<a href=\"socket14On\"><button>ON</button></a>&nbsp;<a href=\"socket14Off\"><button>OFF</button></a><label for='lblSoc14'>" + Socket14Value + "</label></p>";
    webPage += "<p>" + Socket15Name + "<a href=\"socket15On\"><button>ON</button></a>&nbsp;<a href=\"socket15Off\"><button>OFF</button></a><label for='lblSoc15'>" + Socket15Value + "</label></p>";
    server.send(200, "text/html", webPage);
    digitalWrite(gpio15_pin, LOW);
    delay(1000); 
  });
  
  server.begin();
  // Serial.println("HTTP server started");
}
 
void loop(void){
  server.handleClient();
} 
