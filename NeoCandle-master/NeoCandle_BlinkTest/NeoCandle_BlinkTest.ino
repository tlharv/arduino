// This basic sketch is to ensure my Neopixel PCB is working correctly.
// Once uploaded, it flashes blue and red lights like a police car.

// Once you've uploaded ArduinoISP to the Uno, set Board to ATtiny85 @ 8MHz and
// set Programmer to Arduino as ISP

#include <Adafruit_NeoPixel.h>
#define PIN 0  // Must be 0 to work on my neopixel boards.


Adafruit_NeoPixel strip = Adafruit_NeoPixel(2, PIN, NEO_GRB + NEO_KHZ800);
// Despite the NEO_GRB setting,
//     first color is red
//     second color is green
//     third color is blue

void setup() {
  strip.begin();
  strip.show();
}


void loop() {
  strip.setPixelColor(0, 255, 0, 0);
  strip.setPixelColor(1,0,0,0);
  strip.show();
  delay(200);
  strip.setPixelColor(0,0,0,0);
  strip.setPixelColor(1, 0, 0, 255);
  strip.show();
  delay(200);
}


