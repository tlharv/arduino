#include <Adafruit_NeoPixel.h>

#define PIN 0

// color variables: mix RGB (0-255) for desired yellow
int redPx = 255;
int grnPx = 135; //110-120 for 5v, 135 for 3.3v
int bluePx = 15; //10 for 5v, 15 for 3.3v

// variables for pulsing effect
//int heartbeats, hbcycle, hbval, hbdelta;


Adafruit_NeoPixel strip = Adafruit_NeoPixel(1, PIN, NEO_GRB + NEO_KHZ800);

void setup() {
  strip.begin();
  strip.show();
  // set pixel to off
  strip.setPixelColor(0,0,0,0);  // start with nothing lit
  strip.show();
}

void loop() {
  // strip.setPixelColor(0, redPx, grnPx, bluePx);

  // employ an unused analog pin to generate random seed
  randomSeed(analogRead(2));
  int A = random(100,135);
  int B = random(100,135);
  int C = random(100,135);
  
  strip.setPixelColor(0,redPx,grnPx,bluePx);
  strip.show();
  Ramp(0,A,B);
  Ramp(0,B,C);
  Ramp(0,C,A);
  //Ramp(0,A,B);
  //delay(50);
  //Ramp(0,B,C);
  //delay(50);
  //Ramp(0,C,0);
}


void Ramp(int bulb,int fromLevel, int toLevel)  // Fade from one level to another level
{
  int fadeRate = 5;

  //Serial.print("Fading from "); Serial.print (fromLevel); Serial.print(" to "); Serial.println(toLevel);

  //strip.setPixelColor(0,redPx,grnPx,bluePx);  //red only
  //strip.show();
  
  if (fromLevel > toLevel) {
    for (int i = fromLevel; i>toLevel; i--)
    {
      strip.setPixelColor(bulb,redPx,i,bluePx);
      strip.show();
      delay(fadeRate);
    }
  }
  if (fromLevel < toLevel) {
    for (int i = fromLevel; i<toLevel; i++)
    {
      strip.setPixelColor(bulb,redPx,i,bluePx);
      strip.show();
      delay(fadeRate);
    }
  }  
}
