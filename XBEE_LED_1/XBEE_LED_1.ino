/*
 *  XBee Analog Receive
 *  Read an analog value from an XBEE API frame and set the brightness of an LED accordingly.
 *  
 *  Arduino will employ XBEE_A (the Coordinator) and have the LED connected to an analog pin.
 *  Remote will employ XBEE_B (the router) and will receive and use inputs entered into the XBEE_B console.
 *  
 *  Refer to Pg 479 of Arduino Cookbook (problem 14.5)
 */

const int ledPin = 9;

void setup() {
  Serial.begin(9600);
  pinMode(ledPin, OUTPUT);
  configureRadio();
}

boolean configureRadio() {
  // Put the radio in command mode
  Serial.flush();
  Serial.print("+++");
  delay(100);
  String ok_response = "OK\r"; // the response we expect
  
}

void loop() {
  // put your main code here, to run repeatedly:

}
