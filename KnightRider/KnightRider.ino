
const int NumOfLEDs = 7;
int pinArray[NumOfLEDs];
int count = 0;
int timer = 30;

void setup(){
  for (count=0;count<NumOfLEDs;count++) {
    pinArray[count] = count+3;
    pinMode(pinArray[count], OUTPUT);
  }
}

void loop() {
  for (count=0;count<(NumOfLEDs-1);count++) {
   digitalWrite(pinArray[count], HIGH);
   delay(timer);
   digitalWrite(pinArray[count + 1], HIGH);
   delay(timer);
   digitalWrite(pinArray[count], LOW);
   delay(timer*2);
  }
  for (count=(NumOfLEDs-1);count>0;count--) {
   digitalWrite(pinArray[count], HIGH);
   delay(timer);
   digitalWrite(pinArray[count - 1], HIGH);
   delay(timer);
   digitalWrite(pinArray[count], LOW);
   delay(timer*2);
  }
}
