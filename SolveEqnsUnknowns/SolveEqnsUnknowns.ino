#include <MatrixMath.h>

long startTime = millis(); // the value returned from millis when the sketch begins
long duration;  // variable to store the duration

#define N (18)  // Dimension of matrix, up to 18

float A[N][N]; // the matrix of random numbers
float B[N];    // identity matrix
float C[N];    // solution matrix

float max = 10; // maximum random matrix entry range

void setup()
{
  Serial.begin(9600);
  Serial.println("Beginning now...");
  randomSeed(analogRead(0));
  
  // Initialize matrix
  for (int i = 0; i < N; i++)
  {
    for (int j = 0; j < N; j++)
    {
      A[i][j] = random(max);
    }
    B[i] = random(max);
  }
}

void loop()
{
  Matrix.Print((float*)A,N,N,"Coefficient matrix");
  Matrix.Print((float*)B,N,1,"Identity matrix");
  Matrix.Invert((float*)A,N);
  Matrix.Print((float*)A,N,N,"Inverted coefficient matrix");
  
  //void MatrixMath::Multiply(float* A, float* B, int m, int p, int n, float* C)
	// A = input matrix (m x p)
	// B = input matrix (p x n)
	// m = number of rows in A
	// p = number of columns in A = number of rows in B
	// n = number of columns in B
	// C = output matrix = A*B (m x n)
  
  Matrix.Multiply((float*)A,(float*)B,N,N,1,(float*)C);
  Matrix.Print((float*)C,N,1,"Solution matrix");
  duration = millis() - startTime;
  Serial.print("Process took "); 
  Serial.print(duration); 
  Serial.println(" milliseconds.");
  while(1);
}

