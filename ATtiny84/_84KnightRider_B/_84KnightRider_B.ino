
// Larson scanner set up for ATtiny84

// ATMEL ATTINY84 / ARDUINO
// This diagram is for the Tiny core
//
//                           +-\/-+
//                     VCC  1|    |14  GND
//             (D  0)  PB0  2|    |13  AREF (D 10)
//             (D  1)  PB1  3|    |12  PA1  (D  9)
//                     PB3  4|    |11  PA2  (D  8)
//  PWM  INT0  (D  2)  PB2  5|    |10  PA3  (D  7)
//  PWM        (D  3)  PA7  6|    |9   PA4  (D  6)
//  PWM        (D  4)  PA6  7|    |8   PA5  (D  5)        PWM
//                           +----+
//
// PWM = Pulse Width Modulation (good for analog out signals like glowing LEDs)

#include <avr/sleep.h>

const int NumOfLEDs = 10;
// const int MaxScanLoops = 3;
const int ScanBlinkDelay = 100;
const int GreenLightPin = 10;
int pinArray[NumOfLEDs];
int count = 0;
int timer = 30;

void setup(){
  for (count=0;count<NumOfLEDs;count++) {
    pinArray[count] = count;
    pinMode(pinArray[count], OUTPUT);
  }
  pinMode(GreenLightPin, OUTPUT);
}

void loop() {
  // Turn on Status Green light
  digitalWrite(GreenLightPin, HIGH);
  IdleChip(3000);
  
  //Turn off green light
  digitalWrite(GreenLightPin, LOW);
  
  // Do the Larson Scan 3 times and pause for one second
  LarsonScan(3);
  for(count=0; count<NumOfLEDs; count++)
  {
    digitalWrite(pinArray[count], LOW);
  }
  IdleChip(1000);
  
  // Randomly flash individual LEDs 100 times then pause for another second
  RandomFlash(100);
  IdleChip(1000);
  
  // Flash all LEDs twice, on 500 ms, off 500 ms
  FlashAll(500, 2);
  
  // Light all LEDs and turn them off one by one
  for(count=0; count<NumOfLEDs; count++)
  {
    digitalWrite(pinArray[count], HIGH);
  }
  delay(1000);
  for(count=0; count<NumOfLEDs; count++)
  {
    digitalWrite(pinArray[count], LOW);
    IdleChip(250);
  }
  IdleChip(1000); 
}

void IdleChip(unsigned long duration)
{
  unsigned long start = millis();
  set_sleep_mode(SLEEP_MODE_IDLE);  // choose idle mode for the chip
  sleep_enable();  // this puts the chip into sleep mode, but doesn't command it to sleep yet
  while (millis() - start <= duration) {
    sleep_mode();  // put chip in idle mode
  }
  sleep_disable();
}

void LarsonScan(int NumOfLoops)
{
  // Perform Larson Scan
  int ScanLoops = 0;
  do
  {
    for (count=0;count<(NumOfLEDs-1);count++) {
      digitalWrite(pinArray[count], HIGH);
      IdleChip(timer);
      digitalWrite(pinArray[count + 1], HIGH);
      IdleChip(timer);
      digitalWrite(pinArray[count], LOW);
      IdleChip(timer*2);
    }
    for (count=(NumOfLEDs-1);count>0;count--) {
      digitalWrite(pinArray[count], HIGH);
      IdleChip(timer);
      digitalWrite(pinArray[count - 1], HIGH);
      IdleChip(timer);
      digitalWrite(pinArray[count], LOW);
      IdleChip(timer*2);
    }
    ScanLoops++;
  } while (ScanLoops < NumOfLoops);
  // Now clear out all LEDs
  for(count=0; count<NumOfLEDs; count++)
  {
    digitalWrite(pinArray[count], LOW);
  }
}

void RandomFlash(int NumOfRandomFlashes)
{
  // start flashing random LEDs
  for (int count=0; count<NumOfRandomFlashes; count++)
  {
    int i = random(NumOfLEDs);
    digitalWrite(pinArray[i],HIGH);
    delay(ScanBlinkDelay);
    digitalWrite(pinArray[i], LOW);
  }
}

void FlashAll(int duration, int NumOfFlashes)
{
  // Flash all LEDs
  int x = 0;
  do
  {
    for(count=0; count<NumOfLEDs; count++)
    {
      digitalWrite(pinArray[count],HIGH);
    }
    IdleChip(duration);
    for(count=0; count<NumOfLEDs; count++)
    {
      digitalWrite(pinArray[count],LOW);
    }
    IdleChip(duration);
    x++;
  } while (x < NumOfFlashes);
}
