/* This is the main processor program for the justice league computer.
It is written to be put onto an ATtiny84

// This diagram is for the Tiny core
//
//                           +-\/-+
//                     VCC  1|    |14  GND
//             (D  0)  PB0  2|    |13  AREF (D 10)
//             (D  1)  PB1  3|    |12  PA1  (D  9)
//                     PB3  4|    |11  PA2  (D  8)
//  PWM  INT0  (D  2)  PB2  5|    |10  PA3  (D  7)
//  PWM        (D  3)  PA7  6|    |9   PA4  (D  6)
//  PWM        (D  4)  PA6  7|    |8   PA5  (D  5)        PWM
//                           +----+
//
// PWM = Pulse Width Modulation (good for analog out signals like glowing LEDs

D0: used for big green LED
D1: used for big red LED
D2: used for LCD
D3: used for LCD
D4: used for LCD
D5: used for LCD
D6: open
D7: open
D8: open
D9: used for LCD
D10: used for LCD
*/

#include <LiquidCrystal.h>

// Constants for the number of rows and columns in the LCD
const int numRows = 2;
const int numCols = 16;
//const int BigGreenLED = 0;
//const int BigRedLED = 1;

// initialize the library with the numbers of the interface pins
LiquidCrystal lcd(10, 9, 5, 4, 3, 2);

/* This is the pin arrangement needed, going from pin 1 to 16 on the LCD
Pin 1 (LED-) goes to Uno GND, also to GND pin of rotary pot to adjust LCD dimmer
Pin 2 (LED+) goes to Uno 5V, also to 5V pin of rotary pot to adjust LCD dimmer
Pin 3 (DB7) goes to output of rotary pot to adjust LCD dimmer
Pin 4 (DB6) goes to ATtiny84 D10, physical pin 13
Pin 5 (DB5) goes to GND
Pin 6 (DB4) goes to ATtiny84 D9, physical pin 12
Pin 7 (DB3) is not used
Pin 8 (DB2) is not used
Pin 9 (DB1) is not used
Pin 10 (DB0) is not used
Pin 11 (E) goes to ATtiny84 D5, physical pin 8
Pin 12 (RW) goes to ATtiny84 D4, physical pin 7
Pin 13 (RS) goes to ATtiny84 D3, physical pin 6
Pin 14 (V0) goes to ATtiny84 D2, physical pin 5
Pin 15 (VDD) goes to 5V
Pin 16 (VSS) goes to a 220 ohm resistor, then to Uno GND
*/


void setup()
{
  lcd.begin (numCols, numRows);
  //pinMode (BigGreenLED, OUTPUT);
  //pinMode (BigRedLED, OUTPUT);
}

void loop()
{
  // Reset the system
  int RandMsg = random(12);
  int MinimumDelay = 30000;
  int RandTimeDelay = random(30000);
  //digitalWrite(BigGreenLED, HIGH);
  //digitalWrite(BigRedLED, LOW);
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("System Idle");
  
  // Wait up to a minute for the first danger to come in
  delay(MinimumDelay+RandTimeDelay);
  //digitalWrite(BigGreenLED, LOW);
  //digitalWrite(BigRedLED, HIGH);
  lcd.clear();
  delay(7000);
  switch (RandMsg)
  {
    case 0:
      lcd.setCursor(0,0);
      lcd.print("WONDER WOMAN HAS");
      lcd.setCursor(0,1);
      lcd.print("PANTIES IN TWIST");
      break;
    case 1:
      lcd.setCursor(0,0);
      lcd.print("AQUAMAN NEEDS");
      lcd.setCursor(0,1);
      lcd.print("HELP RIGHT AWAY");
      break;
    case 2:
      lcd.setCursor(0,0);
      lcd.print("GIANT IGUANA IS");
      lcd.setCursor(0,1);
      lcd.print("WRECKING SEATTLE");
      break;
    case 3:
      lcd.setCursor(0,0);
      lcd.print("ASTEROID HEADING");
      lcd.setCursor(0,1);
      lcd.print("TOWARD THE EARTH");
      break;
    case 4:
      lcd.setCursor(0,0);
      lcd.print("HUGE TORNADO");
      lcd.setCursor(0,1);
      lcd.print("OVER MALTBY ELEM");
      break;
    case 5:
      lcd.setCursor(0,0);
      lcd.print("VOLCANO ERUPTING");
      lcd.setCursor(0,1);
      lcd.print("PEOPLE IN DANGER");
      break;
    case 6:
      lcd.setCursor(0,0);
      lcd.print("LEX LUTHOR HAS");
      lcd.setCursor(0,1);
      lcd.print("ESCAPED PRISON!");
      break;
    case 7:
      lcd.setCursor(0,0);
      lcd.print("RIDDLER ROBBING");
      lcd.setCursor(0,1);
      lcd.print("WOODINVILLE BANK");
      break;
    case 8:
      lcd.setCursor(0,0);
      lcd.print("TIDAL WAVE HEADS");
      lcd.setCursor(0,1);
      lcd.print("TOWARD HAWAII");
      break;
    case 9:
      lcd.setCursor(0,0);
      lcd.print("HUGE WHIRLPOOL");
      lcd.setCursor(0,1);
      lcd.print("IN PACIFIC OCEAN");
      break;
    case 10:
      lcd.setCursor(0,0);
      lcd.print("MONSTER CHICKEN");
      lcd.setCursor(0,1);
      lcd.print("ATTACKING KANSAS");
      break;
    case 11:
      lcd.setCursor(0,0);
      lcd.print("BIZARRO STEALING");
      lcd.setCursor(0,1);
      lcd.print("ALL TEDDY BEARS");
      break;
    default:
      lcd.setCursor(0,0);
      lcd.print("CAL POLY SLO");
      lcd.setCursor(0,1);
      lcd.print("MECH ENG 1991");
  }
  delay(30000);
}

