// This diagram is for the Tiny core
//
//                           +-\/-+
//                     VCC  1|    |14  GND
//             (D  0)  PB0  2|    |13  AREF (D 10)
//             (D  1)  PB1  3|    |12  PA1  (D  9)
//                     PB3  4|    |11  PA2  (D  8)
//  PWM  INT0  (D  2)  PB2  5|    |10  PA3  (D  7)
//  PWM        (D  3)  PA7  6|    |9   PA4  (D  6)
//  PWM        (D  4)  PA6  7|    |8   PA5  (D  5)        PWM
//                           +----+
//
// PWM = Pulse Width Modulation (good for analog out signals like glowing LEDs

D0: used for big green LED
D1: used for big red LED
D2: used for LCD
D3: used for LCD
D4: used for LCD
D5: used for LCD
D6: open
D7: open
D8: open
D9: used for LCD
D10: used for LCD
*/


void setup() {
  // put your setup code here, to run once:

}

void loop() {
  // put your main code here, to run repeatedly:

}
