// ATMEL ATTINY84 / ARDUINO
// This diagram is for the Tiny core
//
//                           +-\/-+
//                     VCC  1|    |14  GND
//             (D  0)  PB0  2|    |13  AREF (D 10)
//             (D  1)  PB1  3|    |12  PA1  (D  9)
//                     PB3  4|    |11  PA2  (D  8)
//  PWM  INT0  (D  2)  PB2  5|    |10  PA3  (D  7)
//  PWM        (D  3)  PA7  6|    |9   PA4  (D  6)
//  PWM        (D  4)  PA6  7|    |8   PA5  (D  5)        PWM
//                           +----+
//
// PWM = Pulse Width Modulation (good for analog out signals like glowing LEDs

#include <OneWire.h>
#include <DallasTemperature.h>
#include <avr/sleep.h>
#include <avr/wdt.h>

#define WDTO_2S   7  // secret code for defining a watchdog timer for 2 seconds, to be used with wdt_enable()
#define WDTO_4S   8  // secret code for defining a watchdog timer for 4 seconds, to be used with wdt_enable()
 
// Data wire is plugged into D7 on the ATtiny84, physical pin 10
#define ONE_WIRE_BUS 7

// Define your output LED pins
const int redPin = 2;   // analog out, to D2 for red LED, pin 5 on ATtiny84
const int greenPin = 3; // analog out, to D3 for green LED, pin 6 on ATtiny84
const int bluePin = 4;  // analog out, to D4 for blue LED, pin 7 on ATtiny84
const int LL = 17; // Lower Limit in Celsius of acceptable temperature range; below this the light is blue
const int UL = 22; // Upper Limit in Celsius of acceptable temperature range; above this the light is red

// IMPORTANT NOTE: the ATtiny chip must have the 8MHz board bootloaded for using the onewire temp sensor.

// Setup a oneWire instance to communicate with any OneWire devices 
// (not just Maxim/Dallas temperature ICs)
OneWire oneWire(ONE_WIRE_BUS);
 
// Pass our oneWire reference to Dallas Temperature.
DallasTemperature sensors(&oneWire);
 
void setup(void)
{
  pinMode(bluePin, OUTPUT);
  pinMode(redPin, OUTPUT);
  pinMode(greenPin, OUTPUT);
  wdtSetup();  // this sets up the watchdog timer

  // Start up the library
  sensors.begin();
}
 
 
void loop(void)
{
  int TempC;
  
  // call sensors.requestTemperatures() to issue a global temperature request to all devices on the bus
  sensors.requestTemperatures(); // Send the command to get temperatures
  
  TempC = sensors.getTempCByIndex(0); 
 
 // Decide what to do with the temperature reading

  if (TempC < LL) // then light the blue LED - it's cold!
  {
    analogWrite(greenPin, 0);
    analogWrite(redPin, 0);
    GlowLightOnce(bluePin);
  }
  else if (TempC >= LL && TempC <= UL) // then light the green LED
  {
    analogWrite(bluePin, 0);
    analogWrite(redPin, 0);
    GlowLightOnce(greenPin);
  }
  else // light the red LED
  {
    analogWrite(bluePin, 0);
    analogWrite(greenPin, 0);
    GlowLightOnce(redPin);
  }
  
  // DEFINE SLEEP MODE
  wdt_enable(WDTO_2S);  // Set and start the WDT for the argument's duration
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);  // choose your preferred sleep mode for the chip
  sleep_enable(); // this puts the chip into, sleep mode, but doesn't command it to sleep yet.
  // energy saving move: change pin modes to input
  pinMode(bluePin, INPUT);  
  pinMode(redPin, INPUT);
  pinMode(greenPin, INPUT);
  sleep_mode();  // Last statement of loop() - this command actually puts the chip to sleep.

}

// Watchdog Timer Interrupt
ISR (WDT_vect) {
}

void GlowLightOnce(int bulb)  // subroutine applies one fade in - fade out blink of an LED
{
  int brightness = 0;
  int fadeAmount = 5;
  int totalcount = 0;
    do
    {
      analogWrite(bulb, brightness);
      brightness = brightness + fadeAmount;
      totalcount++;
      if (brightness == 255)
      {
        fadeAmount = -fadeAmount;
      }
      IdleChip(35);
    } while (totalcount < 103);
    //IdleChip(1000);  // pause for a second while the bulb is dark
}

void IdleChip(unsigned long duration)
{
  unsigned long start = millis();
  set_sleep_mode(SLEEP_MODE_IDLE);  // choose idle mode for the chip
  sleep_enable();                              // this puts the chip into sleep mode, but doesn't command it to sleep yet
  while (millis() - start <= duration) {
    sleep_mode();  // put chip in idle mode
  }
  sleep_disable();
}

void wdtSetup() // Set up watchdog timer
{
  cli(); // disable all interrupts
  MCUSR = 0;  // Reset Micro Controller Unit Status Register
  // WDTCR register gives you access to the ATtiny85 WDT.
  // Bit 4 = Watchdog Change Enable.  Must be cleared to change or clear the WDE bit.
  // Bit 3 = WDT bit.  Must be cleared to set WDP3 through WDP0.
  WDTCR |= B00011000; // clear WDTCSR register bits 3 & 4
  WDTCR = B01000111;  // Set the WDP3 through WDP0 bits, to set the WDT prescale to two seconds.
  sei(); // enables all interrupts
}
