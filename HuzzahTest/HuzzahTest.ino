/*
 *  Simple HTTP get webclient test
 */

#include <ESP8266WiFi.h>
#include "SparkFun_Si7021_Breakout_Library.h"
#include <Wire.h>


const char* ssid     = "Warp2";
const char* password = "z2xwSs6M";

float humidity = 0;
float tempf = 0;
Weather sensor;


void setup() {
  Serial.begin(115200);
  delay(100);

  // We start by connecting to a WiFi network

  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  
  WiFi.begin(ssid, password);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");  
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  Serial.print("Netmask: ");
  Serial.println(WiFi.subnetMask());
  Serial.print("Gateway: ");
  Serial.println(WiFi.gatewayIP());

  sensor.begin();
}

int value = 0;

void loop() {
  delay(5000);
  ++value;

  getWeather();
  printInfo();
  delay(3000);
  
}

void getWeather()
{
  humidity = sensor.getRH();
  tempf = sensor.getTempF();
}

void printInfo()
{
  Serial.print("Temp: ");
  Serial.print(tempf);
  Serial.print("F, ");

  Serial.print("Humidity: ");
  Serial.print(humidity);
  Serial.println("%");
}
