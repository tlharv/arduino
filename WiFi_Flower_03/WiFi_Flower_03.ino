// Smart Toilet Light with ESP8266
//
// Use a feed in Adafruit IO to control the color and animation of a neopixel.

#include <ESP8266WiFi.h>
#include "Adafruit_MQTT.h"
#include "Adafruit_MQTT_Client.h"
#include "Adafruit_NeoPixel.h"


// Configuration that you _must_ fill in:
#define WLAN_SSID       "Warp2"    // Your WiFi AP name.
#define WLAN_PASS       "2e8e6tcvhh"     // Your WiFi AP password.
#define AIO_USERNAME    "tlharv" // Adafruit IO username (see http://accounts.adafruit.com).
#define AIO_KEY         "691fd8103744453c991bd315d23c89e2"      // Adafruit IO key

// Configuration you can optionally change (but probably want to keep the same):
#define PIXEL_PIN       2                      // Pin connected to the NeoPixel data input.
#define PIXEL_COUNT     8                      // Number of NeoPixels.
#define PIXEL_TYPE      NEO_GRB + NEO_KHZ800   // Type of the NeoPixels (see strandtest example).
#define LIGHT_FEED_R    "RedFlowerFeed"        // Name of the feed in Adafruit IO to listen for colors.
#define LIGHT_FEED_G    "GreenFlowerFeed"      // Green color feed
#define LIGHT_FEED_B    "BlueFlowerFeed"       // Blue color feed
#define AIO_SERVER      "io.adafruit.com"      // Adafruit IO server address.
#define AIO_SERVERPORT  1883                   // AIO server port.
#define PING_SEC        60                     // How many seconds to wait between MQTT pings.
                                               // Used to help keep the connection alive during
                                               // long periods of inactivity.

// Global state (you don't need to change this):
// Put strings in flash memory (required for MQTT library).
const char MQTT_SERVER[] PROGMEM    = AIO_SERVER;
const char MQTT_USERNAME[] PROGMEM  = AIO_USERNAME;
const char MQTT_PASSWORD[] PROGMEM  = AIO_KEY;
const char MQTT_PATH_R[] PROGMEM      = AIO_USERNAME "/feeds/" LIGHT_FEED_R;
const char MQTT_PATH_G[] PROGMEM      = AIO_USERNAME "/feeds/" LIGHT_FEED_G;
const char MQTT_PATH_B[] PROGMEM      = AIO_USERNAME "/feeds/" LIGHT_FEED_B;

// Create ESP8266 wifi client, MQTT client, and feed subscription.
WiFiClient client;
Adafruit_MQTT_Client mqtt(&client, MQTT_SERVER, AIO_SERVERPORT, MQTT_USERNAME, MQTT_PASSWORD);
Adafruit_MQTT_Subscribe lightFeed_R = Adafruit_MQTT_Subscribe(&mqtt, MQTT_PATH_R);
Adafruit_MQTT_Subscribe lightFeed_G = Adafruit_MQTT_Subscribe(&mqtt, MQTT_PATH_G);
Adafruit_MQTT_Subscribe lightFeed_B = Adafruit_MQTT_Subscribe(&mqtt, MQTT_PATH_B);

// Create NeoPixel object
Adafruit_NeoPixel strip = Adafruit_NeoPixel(PIXEL_COUNT, PIXEL_PIN, PIXEL_TYPE);

// Other global state:
uint32_t nextPing = 0;       // Next time a MQTT ping should be sent.
int red = 0;                 // RGB color for the current animation.
int green = 0;
int blue = 0;
int animation = 0;           // Current animation (0 = none, 1 = pulse, 2 = rainbow cycle).
int pulsePeriodMS = 0;       // Period of time (in MS) for the pulse animation.

char* data_R, data_G, data_B;  // char values brought over from IO page
int R_val, G_val, B_val;   // converted values to be used in lighting the LED


// Explicit declaration of MQTT_connect function defined further below.
// Necessary because of bug/issue with recent Arduino builder & ESP8266.
void MQTT_connect();

// Function to parse a hex byte value from a string.
// The passed in string MUST be at least 2 characters long!
// If the value can't be parsed then -1 is returned, otherwise the
// byte value is returned.
int parseHexByte(char* data) {
  char high = tolower(data[0]);
  char low = tolower(data[1]);
  uint8_t result = 0;
  // Parse the high nibble.
  if ((high >= '0') && (high <= '9')) {
    result += 16*(high-'0');
  }
  else if ((high >= 'a') && (high <= 'f')) {
    result += 16*(10+(high-'a'));
  }
  else {
    // Couldn't parse the high nibble.
    return -1;
  }
  // Parse the low nibble.
  if ((low >= '0') && (low <= '9')) {
    result += low-'0';
  }
  else if ((low >= 'a') && (low <= 'f')) {
    result += 10+(low-'a');
  }
  else {
    // Couldn't parse the low nibble.
    return -1;
  }
  return result;
}


void setup() {
  // Initialize serial output.
  Serial.begin(115200);
  delay(10);
  Serial.println("Smart flower Light with ESP8266");

  // Initialize NeoPixels and turn them off.
  strip.begin();
  Serial.println("NeoPixel strip initialized.");
  lightPixels(strip.Color(0, 0, 0));

  // Connect to WiFi access point.
  Serial.println(); Serial.println();
  Serial.print("Connecting to ");
  Serial.println(WLAN_SSID);
  WiFi.begin(WLAN_SSID, WLAN_PASS);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println();
  Serial.println("WiFi connected");
  Serial.println("IP address: "); Serial.println(WiFi.localIP());

  // Setup MQTT subscriptions.
  mqtt.subscribe(&lightFeed_R);
  mqtt.subscribe(&lightFeed_G);
  mqtt.subscribe(&lightFeed_B);
  Serial.println("MQTT subscriptions complete.");
}


// Function to set all the NeoPixels to the specified color.
void lightPixels(uint32_t color) {
  for (int i=0; i<PIXEL_COUNT; ++i) {
    strip.setPixelColor(i, color);
  }
  strip.show();
}

void LightEmUp(uint8_t R, uint8_t G, uint8_t B) {
  Serial.print("Time to light the pixels using ("); 
  Serial.print(R); Serial.print(","); Serial.print(G); Serial.print(","); Serial.print(B); Serial.println(")");
  // First blank out the strip
  lightPixels(strip.Color(0, 0, 0));
  Serial.println("Colors cleared.");
  delay(500);
  lightPixels(strip.Color(R, G, B));
  Serial.println("New color applied.");
}

int datalen(char* data) {
  //do something neat
  int datalen = strlen(data);
  int firstdigit,seconddigit,thirddigit = 0;
  Serial.print("Length of entry is "); Serial.println(datalen);
  return datalen;
}

void loop() {
  
  // Ensure the connection to the MQTT server is alive (this will make the first
  // connection and automatically reconnect when disconnected).  See the MQTT_connect
  // function definition further below.
  MQTT_connect();

  /*  REFERENCE
      char* data_R, data_G, data_B;  // char values brought over from IO page
      int R_val, G_val, B_val;   // converted values to be used in lighting the LED
  */
  
  // Check if any new data has been received from the light feed.
  Adafruit_MQTT_Subscribe *subscription;
  while ((subscription = mqtt.readSubscription(10))) {
    if (subscription == &lightFeed_R) {
      // Received data from the red light feed!
      char* data_R = (char*)lightFeed_R.lastread;  // data_R is the numerical value (0-255) sent for the Red part of RGB)
      Serial.print("Received R= ");
      Serial.println(data_R);
    } // end of If

    if (subscription == &lightFeed_G) {
      // Received data from the red light feed!
      char* data_G = (char*)lightFeed_G.lastread;  // data_G is the numerical value (0-255) sent for the Green part of RGB)
      Serial.print("Received G= ");
      Serial.println(data_G);
    } // end of If

    if (subscription == &lightFeed_B) {
      // Received data from the red light feed!
      char* data_B = (char*)lightFeed_B.lastread;  // data_B is the numerical value (0-255) sent for the Blue part of RGB)
      Serial.print("Received B= ");
      Serial.println(data_B);
    } // end of If


    // Now convert the char arrays data_R, data_G and data_B into integers
    //R_val = atoi(&data_R[1]);
    //Serial.print("R_val = "); Serial.println(R_val);
    
    //int G_val = atoi(&data_G[1]);
    //Serial.print("G_val = "); Serial.println(G_val);
    
    //B_val = atoi(&data_B[1]);
    //Serial.print("B_val = "); Serial.println(B_val);

    //LightEmUp(data_R, data_G, data_B);
    LightEmUp(R_val, G_val, B_val);
    
    lightPixels((0,0,0));
    delay(500);

    for (int i=0; i<PIXEL_COUNT; ++i) {
      strip.setPixelColor(i, (int(data_R), int(data_G), int(data_B)));
    }
    strip.show();
    
  break; // so the loop doesn't go on forever
  } // end of While loop


  // Ping the MQTT server periodically to prevent the connection from being closed.
  if (millis() >= nextPing) {
    // Attempt to send a ping.
    if(! mqtt.ping()) {
      // Disconnect if the ping failed.  Next loop iteration a reconnect will be attempted.
      mqtt.disconnect();
    }
    // Set the time for the next ping.
    nextPing = millis() + PING_SEC*1000L;
  } // end of if for ping loop
} // end of void loop()

// Function to connect and reconnect as necessary to the MQTT server.
// Should be called in the loop function and it will take care of connecting.
void MQTT_connect() {
  int8_t ret;

  // Stop if already connected.
  if (mqtt.connected()) {
    return;
  }

  Serial.print("Connecting to MQTT... ");

  uint8_t retries = 3;
  while ((ret = mqtt.connect()) != 0) { // connect will return 0 for connected
       Serial.println(mqtt.connectErrorString(ret));
       Serial.println("Retrying MQTT connection in 5 seconds...");
       mqtt.disconnect();
       delay(5000);  // wait 5 seconds
       retries--;
       if (retries == 0) {
         // basically die and wait for WDT to reset me
         while (1);
       }
  }
  Serial.println("MQTT Connected!");
}

