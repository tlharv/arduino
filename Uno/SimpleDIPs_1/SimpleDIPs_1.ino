// This sketch assumes that one end of each DIP switch goes to ground, and the other end goes to Vcc through a 10K pullup resistor.
// The input pin checks the signal at the pullup resistor.


// Arduino pins used for the switches

#define S1 2
#define S2 3

const long oneSecond = 1000;
const long oneMinute = oneSecond * 60;

// State of each switch (0 or 1)
int s1state;
int s2state;
int TS; // Timer Setting (1, 5, 10 or 15 minutees)

void setup() {

  // pins for switches are inputs
  pinMode(S1, INPUT);
  pinMode(S2, INPUT);

  // set up serial port
  Serial.begin(9600);
  
  // Read DIP switches and determine timer setting
  s1state = 1-digitalRead(S1);
  s2state = 1-digitalRead(S2);
  int reading = 10*s1state + s2state;
  TS = TimerSetting(reading);
}

void loop() {
  Serial.print("The timer is set for ");
  Serial.print(TS);
  Serial.print(" minutes");
  Serial.println();
  delay(1000);
}

int TimerSetting(int r) {
  int TimerSetting;
  switch (r)
  {
    case 0:  // Switches at 00
      TimerSetting = 1;
      break;
    case 1:  // Switches at 01
      TimerSetting = 5;
      break;
    case 10: // Switches at 10
      TimerSetting = 10;
      break;
    case 11: // Switches at 11
      TimerSetting = 15;
      break;
  }
  return TimerSetting;
}
