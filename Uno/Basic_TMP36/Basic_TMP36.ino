

// Refer to 6.8 in Arduino Cookbook for temperature equations
//
// This sketch simply lights a blue LED if the temperature falls below 65F.
// Otherwise, if above 65F the red LED is lit.

const int inPin = 0; // analog pin for temp sensor
const int bluePin = 3;
const int greenPin = 5;
const int redPin = 7;
const int LL = 64; // below this temp the blue LED lights
const int ML = 68; // above this temp the red LED lights

void setup()
{
  Serial.begin(9600);
  // pinMode(bluePin, OUTPUT);
  pinMode(redPin, OUTPUT);
}

void loop()
{
  int value = analogRead(inPin);
  float millivolts = (value / 1024.0) * 5000;
  float celsius = (millivolts / 10.0) - 50; // this equation from a guy on the SparkFun site
  float fahrenheit = (celsius *9)/5 + 32;
  
  if (fahrenheit < LL) // then light the blue LED - it's cold!
  {
    digitalWrite(bluePin, HIGH);
    digitalWrite(greenPin, LOW);
    digitalWrite(redPin, LOW);
  }
  else if (fahrenheit >= LL && fahrenheit < ML) // then light the green LED
  {
    digitalWrite(bluePin, LOW);
    digitalWrite(greenPin, HIGH);
    digitalWrite(redPin, LOW);
  }
  else // light the red LED
  {
    digitalWrite(bluePin, LOW);
    digitalWrite(greenPin, LOW);
    digitalWrite(redPin, HIGH);
  }
  
  Serial.print("Fahrenheit = "); Serial.println(fahrenheit);
  // Serial.println();
  
  delay(1000);
}

