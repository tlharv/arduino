// For the breakout, you can use any (2 or) 3 pins

#define cs   10
#define dc   9
#define rst  8  // you can also connect this to the Arduino reset

#include <Adafruit_GFX.h>    // Core graphics library
#include <Adafruit_ST7735.h> // Hardware-specific library
#include <SPI.h>

// set up DHT11 temp/humidity sensor
#include "DHT.h"
#define DHTPIN 7
#define DHTTYPE DHT11
DHT dht(DHTPIN, DHTTYPE);


#if defined(__SAM3X8E__)
    #undef __FlashStringHelper::F(string_literal)
    #define F(string_literal) string_literal
#endif

Adafruit_ST7735 tft = Adafruit_ST7735(cs, dc, rst);

int x; 
float maxTf,minTf,maxH,minH;
  

void setup(void) {
  tft.initR(INITR_BLACKTAB);   // initialize a ST7735S chip, black tab

  // Power-on splash screen
  ShowSplash();
  dht.begin();
  delay(2000);
  
  x = 0;
  maxTf = 0;
  minTf = 0;
  maxH = 0;
  minH = 0;
    
  // Take initial readings for max/min values
  float h = dht.readHumidity();
  float tc = dht.readTemperature();
  float tf = (9.0 * tc / 5.0) + 32.0;
  maxTf = tf;
  minTf = maxTf;
  maxH = h;
  minH = maxH;
}

void loop() {
  // need to blank out before reprinting
  tft.fillScreen(ST7735_BLACK);
  x += 1;

  // Draw the frame
  drawFrame();
  tft.setCursor(0,90);
  
  // Take temperature and humidity readings
  float h = dht.readHumidity();
  float tc = dht.readTemperature();
  float tf = (9.0 * tc / 5.0) + 32.0;
  double dpc = dewPoint(tc,h); // dew point in centigrade
  double dpf = (9.0 * dpc / 5.0) + 32; // dew point in Fahrenheit
  
  // update max & min Temp and Humidity readings
  if (tf >= maxTf) maxTf = tf;
  if (tf < minTf) minTf = tf;
  if (h >= maxH) maxH = h;
  if (h < minH) minH = h;
    
  // Draw the text
  tft.setCursor(0,35);
  tft.setTextSize(1);
  tft.setTextColor(ST7735_CYAN);
  tft.print("    Temp = ");
  tft.print(tf);
  tft.println("F");
  tft.println("");
  tft.print("  Humidity = ");
  tft.print(h);
  tft.println("%");
  tft.println("");
  tft.setTextColor(ST7735_MAGENTA);
  tft.print("  Dew point = ");
  tft.print(dpf);
  tft.println("F");
  
  // Print out max/min values
  tft.setCursor(0,100);
  tft.setTextColor(ST7735_GREEN);
  tft.print("Max Temp = ");
  tft.println(maxTf);
  tft.print("Min Temp = ");
  tft.println(minTf);
  tft.println("");
  tft.print("Max Humidity = ");
  tft.println(maxH);
  tft.print("Min Humidity = ");
  tft.println(minH);
  
  delay(10000);
  //tft.setCursor(0,70);
  //tft.print("");
}

void drawFrame() {
  tft.fillScreen(ST7735_BLACK);
  tft.setCursor(0,10);
  tft.setTextSize(1);
  tft.setTextColor(ST7735_YELLOW);
  tft.println("       Current");
  tft.println("      Conditions");
  tft.println("");
  tft.drawLine(5,5,123,5,ST7735_RED);
  tft.drawLine(5,5,5,80,ST7735_RED);
  tft.drawLine(5,80,123,80,ST7735_RED);
  tft.drawLine(123,80,123,5,ST7735_RED);
  tft.drawLine(5,30,123,30,ST7735_RED);
}

void ShowSplash() {
  tft.fillScreen(ST7735_GREEN);
  tft.setCursor(0,50);
  tft.setTextColor(ST7735_BLUE);
  tft.setTextSize(1);
  tft.println(" POWER ON");
  delay(1000);
}

double dewPoint(double celsius, double humidity) {
  // (1) Saturation Vapor Pressure = ESGG(T)
  double RATIO = 373.15 / (273.15 + celsius);
  double RHS = -7.90298 * (RATIO - 1);
  RHS += 5.02808 * log10(RATIO);
  RHS += -1.3816E-7 * (pow(10, (11.344 * (1 - 1/RATIO ))) - 1);
  RHS += 8.1328E-3 * (pow(10, (-3.49149 * (RATIO - 1))) - 1);
  RHS += log10(1013.246);
  // factor -3 is to adjust units - Vapor Pressure SVP * humidity
  double VP = pow(10, RHS - 3) * humidity;
  
  // (2) DEWPOINT = F(Vapor Pressure)
  double T = log(VP/0.61078); // temp var
  return (241.88 * T) / (17.558 - T);
}

