
/* 
 Stepper Motor Control - one revolution
 
 This program drives a unipolar or bipolar stepper motor. 
 The motor is attached to digital pins 8 - 11 of the Arduino.
*/

#include <Stepper.h>

const int sensorPin = 0; // light sensor plugs into Analog input A0 on Uno
const int LightLevelTrigger = 400; // sensitivity of light trigger

const int RPM = 20; //Stepper motor speed in rpm
const int Revolutions = 3; // number of times you want the motor to turn
const int stepsPerRevolution = 200;  // change this to fit the number of steps per revolution
                                     // for your motor

// initialize the stepper library on pins 8 through 11:
Stepper myStepper(stepsPerRevolution, 8,9,10,11);            

void setup() {
  // set the speed:
  myStepper.setSpeed(RPM);
  // initialize the serial port:
  Serial.begin(9600);
}

void loop() {
  // step revolutions  in one direction:
  int lightLevel = analogRead(sensorPin); // read the analog input
  Serial.println(lightLevel);
  pinMode (8,INPUT);
  pinMode (9, INPUT);
  pinMode (10,INPUT);
  pinMode (11, INPUT);
  
  if (lightLevel > LightLevelTrigger)
  {
    //Serial.println("clockwise");
    pinMode (8,OUTPUT);
    pinMode (9, OUTPUT);
    pinMode (10,OUTPUT);
    pinMode (11, OUTPUT);
    myStepper.step(Revolutions * stepsPerRevolution);
  }
  delay(2000);
  
  // step one revolution in the other direction:
  //Serial.println("counterclockwise");
  //myStepper.step(-stepsPerRevolution);
  //delay(500); 
}

