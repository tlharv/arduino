// For the breakout, you can use any (2 or) 3 pins

#define cs   10
#define dc   9
#define rst  8  // you can also connect this to the Arduino reset

#include <Adafruit_GFX.h>    // Core graphics library
#include <Adafruit_ST7735.h> // Hardware-specific library
#include <SPI.h>

// set up DHT11 temp/humidity sensor
#include "DHT.h"
#define DHTPIN 2
#define DHTTYPE DHT11
DHT dht(DHTPIN, DHTTYPE);


#if defined(__SAM3X8E__)
    #undef __FlashStringHelper::F(string_literal)
    #define F(string_literal) string_literal
#endif

Adafruit_ST7735 tft = Adafruit_ST7735(cs, dc, rst);

int x; 
  

void setup(void) {
  Serial.begin(9600);
  Serial.print("hello!");
  tft.initR(INITR_BLACKTAB);   // initialize a ST7735S chip, black tab
  Serial.println("init");

  uint16_t time = millis();
  tft.fillScreen(ST7735_BLACK);
  time = millis() - time;

  Serial.println(time, DEC);
  delay(500);

  // Power-on splash screen
  drawFrame();
  delay(1000);
  x = 0;
  
  dht.begin();
}

void loop() {
  float h = dht.readHumidity();
  float tc = dht.readTemperature();
  float tf = (9.0 * tc / 5.0) + 32.0;
  // need to blank out before reprinting
  tft.fillScreen(ST7735_BLACK);
  x += 1;
  drawFrame();
  tft.setCursor(0,90);
  tft.setTextSize(1);
  tft.setTextColor(ST7735_WHITE);
  tft.print("Humidity: ");
  tft.print(h);
  tft.println(" %");
  tft.println("");
  tft.print("Temp: ");
  tft.print(tf);
  tft.println("F");
  delay(10000);
  //tft.setCursor(0,70);
  //tft.print("");
}

void drawFrame() {
  tft.fillScreen(ST7735_BLACK);
  tft.setCursor(0,30);
  tft.setTextSize(1);
  tft.setTextColor(ST7735_YELLOW);
  tft.println("       Current");
  tft.println("      Conditions");
  tft.println("");
  tft.setTextColor(ST7735_CYAN);
  tft.println("     Temp = 68 F");
  tft.println("     Humid = 20%");
  tft.drawLine(10,10,118,10,ST7735_RED);
  tft.drawLine(10,10,10,80,ST7735_RED);
  tft.drawLine(10,80,118,80,ST7735_RED);
  tft.drawLine(118,80,118,10,ST7735_RED);
  
  // Draw planets
  tft.fillCircle(64,120,2,ST7735_YELLOW); // Sun
  tft.drawPixel(70,110,ST7735_WHITE); // Mercury
  tft.drawPixel(55,135,ST7735_MAGENTA); // Venus
  tft.drawPixel(62,148,ST7735_BLUE); // Earth
  tft.drawPixel(110,154,ST7735_RED); // Mars
}


