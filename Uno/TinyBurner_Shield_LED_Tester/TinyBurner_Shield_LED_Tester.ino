const int greenLED = 13;

const int redLED = 8;
const int blueLED = 7;
const int yellowLED = 9;
const int waiter = 1000;

void setup()
{
  pinMode(redLED, OUTPUT);
  pinMode(blueLED, OUTPUT);
  pinMode(yellowLED, OUTPUT);
  pinMode(greenLED, OUTPUT);
}

void loop()
{
  digitalWrite(redLED, HIGH);
  delay(waiter);
  digitalWrite(blueLED, HIGH);
  delay(waiter);
  digitalWrite(yellowLED, HIGH);
  delay(waiter);
  digitalWrite(greenLED, HIGH);
  delay(waiter);

  digitalWrite(redLED, LOW);
  delay(waiter);
  digitalWrite(blueLED, LOW);
  delay(waiter);
  digitalWrite(yellowLED, LOW);
  delay(waiter);
  digitalWrite(greenLED, LOW);
  delay(waiter);
}
