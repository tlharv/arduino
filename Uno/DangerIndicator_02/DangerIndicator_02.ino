int led[6];
const int OnWait = 450;
const int OffWait = 2000;

void setup()
{
  for (int i=0; i<6; i++)
  {
    led[i] = i+2;
    pinMode(led[i], OUTPUT);
  }
}

void loop()
{
  for (int i=0; i<6; i++)
  {
    digitalWrite(led[i], HIGH);
  }
  delay(OnWait);
  for (int j=0; j<6; j++)
  {
    digitalWrite(led[j], LOW);
  }
  delay(OffWait);
}

