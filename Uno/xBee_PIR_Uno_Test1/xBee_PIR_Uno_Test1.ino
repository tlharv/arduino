/*  This sketch will read the state of a PIR sensor (digital input to an Uno)
 *  and, if HIGH, will transmit a "1" over an xBee router to an xBee coordinator.
 *  
 */

#include <XBee.h>

// Create the XBee object
XBee xbee = XBee();
uint8_t payload[] = { 0 };
// SH + SL Address of receiving XBee
XBeeAddress64 addr64 = XBeeAddress64(0x0013a200, 0x4164D656);
ZBTxRequest zbTx = ZBTxRequest(addr64, payload, sizeof(payload));
ZBTxStatusResponse txStatus = ZBTxStatusResponse();


const int PIR = 4; // digital in, to D4
int flag = 0;  // flag for motion sensing

int statusLed = 13;
int errorLed = 13;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(PIR, INPUT);
  xbee.setSerial(Serial);
}


void flashLed(int pin, int times, int wait) {

  for (int i = 0; i < times; i++) {
    digitalWrite(pin, HIGH);
    delay(wait);
    digitalWrite(pin, LOW);

    if (i + 1 < times) {
      delay(wait);
    }
  }
}

void loop() {
  // put your main code here, to run repeatedly:
  int val = digitalRead(PIR);
  if (val == HIGH) // val is high whenever the PIR is lit up
  {
    flag = 1;
    Serial.println("Motion detected!");
    delay(6000);
    // insert code to send a "1" to the xBee coordinator
    payload[0] = 1;
    xbee.send(zbTx);
    // after sending a tx request, we expect a status response
    // wait up to half second for the status response
    if (xbee.readPacket(500)) {
      // got a response!

      // should be a znet tx status              
      if (xbee.getResponse().getApiId() == ZB_TX_STATUS_RESPONSE) {
        xbee.getResponse().getZBTxStatusResponse(txStatus);

        // get the delivery status, the fifth byte
        if (txStatus.getDeliveryStatus() == SUCCESS) {
          // success.  time to celebrate
          //flashLed(statusLed, 5, 50);
        } else {
          // the remote XBee did not receive our packet. is it powered on?
          //flashLed(errorLed, 3, 500);
        }
      }
    } else if (xbee.getResponse().isError()) {
      //nss.print("Error reading packet.  Error code: ");  
      //nss.println(xbee.getResponse().getErrorCode());
    } else {
      // local XBee did not provide a timely TX Status Response -- should not happen
      //flashLed(errorLed, 2, 50);
    }  
  }

  flag = 0; // reset the flag
}
