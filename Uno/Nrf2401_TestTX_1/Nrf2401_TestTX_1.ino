#include <Nrf2401.h>

// Pin Connections
// DR1 -> 2 (digital pin 2)
// CD -> 3  Chip Detect
// CS -> 4  
// CLK -> 5
// DAT -> 6

/*
Pin   Label   Description               Uno Pin
---   -----   ------------------------  -------
 1    GND     Ground                      GND
 2    VCC     Voltage (must be 3.3VDC)    3V3
 3    CE      Chip Enable                  D2
 4    CSN     ???                          D3
 5    SCK     ???                          D4
 6    MOSI    Master Out Slave In          D5
 7    MISO    Master In Slave Out          D6
 8    IRQ     ???                          D7
*/

Nrf2401 Radio;

// CODE FOR TRANSMISSION UNIT
// Send a 3 byte message every 2 seconds to the device at address 1.

void setup()
{
  Radio.remoteAddress = 1;
  Radio.txMode(3);
  Radio.data[0] = 22;
  Radio.data[1] = 33;
  Radio.data[2] = 44;
}

void loop()
{
  Radio.write();
  delay(2000);
}

