#include <Adafruit_GFX.h>
#include <Adafruit_ST7735.h>
#include <SPI.h>

#define TFT_CS 10
#define TFT_RST 9
#define TFT_DC 8

// For 1.44" and 1.8" TFT with ST7735 use:
Adafruit_ST7735 tft = Adafruit_ST7735(TFT_CS, TFT_DC, TFT_RST);

void setup() {
  tft.initR(INITR_144GREENTAB);   // initialize a ST7735S chip, green tab
  delay(100);

  // Send initialization text to TFT display
  float TempF = 72.0;
    
  tft.fillScreen(ST7735_BLACK); // blank out the screen
  tft.setCursor(0,0);
  tft.setTextColor(ST7735_YELLOW);
  tft.setTextSize(1);
  tft.print("Temp: "); 
  tft.setTextColor(ST7735_CYAN);
  tft.print(TempF); 
  tft.setTextColor(ST7735_YELLOW);
  char deg = char(167); // degree symbol
  tft.print(deg); tft.println("F");
}

void loop() {
  // put your main code here, to run repeatedly:

}
