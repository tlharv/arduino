/******************************************************************************
  SparkFun Si7021 and Adafruit 1.44" TFT sketch 
  Tom Harvey
  Original Creation Date: Dec 25, 2020
  This sketch prints the temperature and humidity to a TFT screen.

  The library used in this example can be found here:
  https://github.com/sparkfun/Si7021_Breakout/tree/master/Libraries

  Si7021 Hardware Connections:
      (-) ------------------- GND
      (+) ------------------- 3.3V pin on TFT display
       CL ------------------- SCL
       DA ------------------- SDA

  TFT Display hardware connections:
      Vin ------------------- 5VDC on Arduino
      3.3V ------------------ to (+) pin on Si7021
      GND ------------------- GND on Arduino
      SCK ------------------- D13 on Arduino
      MISO ------------------ None
      MOSI ------------------ D11 on Arduino
      TFT_CS ---------------- D10 on Arduino
      RST ------------------- D9 on Arduino
      D/C ------------------- D8 on Arduino
      CARD_CS --------------- None
      LITE ------------------ None
*******************************************************************************/
#include "SparkFun_Si7021_Breakout_Library.h"
#include <Wire.h>

float humidity = 0;
float tempf = 0;
int power = A3;
int GND = A2;

//Create Instance of HTU21D or SI7021 temp and humidity sensor and MPL3115A2 barrometric sensor
Weather sensor;

// Libraries for Graphics screen
#include <Adafruit_GFX.h>
#include <Adafruit_ST7735.h>
#include <SPI.h>

#define TFT_CS 10
#define TFT_RST 9
#define TFT_DC 8

#define TempF_color ST7735_MAGENTA
#define Humid_color ST7735_GREEN

// For 1.44" and 1.8" TFT with ST7735 use:
Adafruit_ST7735 tft = Adafruit_ST7735(TFT_CS, TFT_DC, TFT_RST);

// Various variables
int x;
float maxTempF, minTempF, maxHumid, minHumid;
float Tdatapoint[128];
float Hdatapoint[128];
const long oneSecond = 1000;
const long oneMinute = oneSecond * 60;
const int totalPoints = 128;


//---------------------------------------------------------------
void setup()
{
    Serial.begin(9600);   // open serial over USB at 9600 baud

    pinMode(power, OUTPUT);
    pinMode(GND, OUTPUT);

    digitalWrite(power, HIGH);
    digitalWrite(GND, LOW);

    // OR use this initializer (uncomment) if using a 1.44" TFT:
    tft.initR(INITR_144GREENTAB);   // initialize a ST7735S chip, green tab
    delay(100);

    // Send initialization text to TFT display
    tft.fillScreen(ST7735_BLACK); // blank out the screen
    tft.setCursor(0,0);
    tft.setTextColor(ST7735_CYAN);
    tft.setTextSize(1);
    tft.print("TFT ready: "); tft.print(tft.width()); tft.print("x"); tft.println(tft.height());
    tft.println();
    
    //Initialize the I2C sensors and ping them
    sensor.begin();
    tft.println("I2C sensors pinged");

    // Get initial readings and set max/min values to them
    getWeather();
    maxTempF = tempf;
    minTempF = tempf;
    maxHumid = humidity;
    minHumid = humidity;
    tft.println("Initial readings");

    // initialize array
    for (int x = totalPoints-1; x >=0; x--) { // go from end to beginning just for fun
      Tdatapoint[x] = 0;
      Hdatapoint[x] = 0;
    }

    delay(500);

}
//---------------------------------------------------------------
void loop()
{
  // This code repeats over and over until power is cut

  // Take a weather reading
  getWeather(); // Now we have a current tempF and humidity value

  // Update max & min temp and humidity readings
  if (tempf >= maxTempF) maxTempF = tempf;
  if (tempf <= minTempF) minTempF = tempf;
  if (humidity >= maxHumid) maxHumid = humidity;
  if (humidity <= minHumid) minHumid = humidity;
  
  // blank out the screen
  tft.fillScreen(ST7735_BLACK);

  drawAxes();
  drawGrid();


// NEW HEADER TO SHOW BOTH TEMP & HUMIDITY
  tft.setCursor(0,2);
  tft.setTextColor(TempF_color);
  tft.setTextSize(1);
  tft.print(tempf,1); tft.print(" F");
  
  tft.setCursor(0,12);
  tft.setTextColor(Humid_color);
  tft.setTextSize(1);
  tft.print(humidity,1); tft.print("% RH");
  
  drawUpArrow(80,2,ST7735_RED); // This also calls the position
  
  tft.setCursor(90,2);
  tft.setTextSize(1);
  tft.setTextColor(ST7735_RED);
  tft.print(maxTempF,1); tft.println("F");
  
  drawDownArrow(80,13,ST7735_CYAN);
  
  tft.setCursor(90,13);
  tft.setTextColor(ST7735_CYAN);
  tft.print(minTempF,1); tft.println("F");

  // Add this current temp value to the highest array element
  Tdatapoint[totalPoints-1] = tempf; // current temp is on right-hand side of the screen
  Hdatapoint[totalPoints-1] = humidity;
  
  // Plot all of the points
  for (int x = 0; x < totalPoints; x++) {  // go from right to left
      tft.drawPixel(x,128-Tdatapoint[x],TempF_color);
      tft.drawPixel(x,128-Hdatapoint[x],Humid_color);
  }
  
  // shift array elements one to the left
  for (int x = 0; x < totalPoints-1; x++) {
    Tdatapoint[x] = Tdatapoint[x+1];
    Hdatapoint[x] = Hdatapoint[x+1];
  }

  delay(1 * oneMinute);  // 11 minute delay makes the graph span one 24 hour period

  
}

void drawAxes() {
  // Set up axes so that horizontal range is 126 cycles and vertical range is 100 degrees F
  tft.drawFastHLine(2,126,126,ST7735_RED);  // horizontal axis
  tft.drawFastVLine(2,27,100,ST7735_RED);  // vertical axis
  tft.setCursor(0,120);
  tft.setTextSize(1);
  tft.setTextColor(ST7735_WHITE);
  tft.println("0");  // show scale min = 0 fahrenheit
  // tft.setCursor(0,16);
  // tft.println("100"); // show scale max = +100 fahrenheit
  tft.drawFastHLine(2,(127-32),126,ST7735_CYAN); // horizontal line at freezing
}

void drawGrid() {
  // Draw grid of 10 pixel increments
  int increment = 10;
  uint16_t color = ST7735_BLUE;
  
  // origin is at 3,126 for horizontal lines
  uint16_t Ox = 2;
  uint16_t Oy = 127;
  for (int x = 1; x < 11; x++) {
    tft.drawFastHLine(Ox,Oy-(x*increment),125,color);
  }
  Ox = 1;
  Oy = 27;
  for (int x = 1; x < 13; x++) {
    tft.drawFastVLine(Ox+(x*increment),Oy,100,color);
  }
}


//---------------------------------------------------------------
void getWeather()
{
  // Measure Relative Humidity from the HTU21D or Si7021
  humidity = sensor.getRH();

  // Measure Temperature from the HTU21D or Si7021
  tempf = sensor.getTempF();
  // Temperature is measured every time RH is requested.
  // It is faster, therefore, to read it from previous RH
  // measurement with getTemp() instead with readTemp()
}
//---------------------------------------------------------------

void drawUpArrow(uint16_t Ox,uint16_t Oy, uint16_t color) {
  tft.drawPixel(Ox+3,Oy,color);   // tip of arrowhead
  tft.drawPixel(Ox+2,Oy+1,color);
  tft.drawPixel(Ox+3,Oy+1,color);
  tft.drawPixel(Ox+4,Oy+1,color);
  tft.drawPixel(Ox+1,Oy+2,color);
  tft.drawPixel(Ox+2,Oy+2,color);
  tft.drawPixel(Ox+3,Oy+2,color);
  tft.drawPixel(Ox+4,Oy+2,color);
  tft.drawPixel(Ox+5,Oy+2,color);
  tft.drawPixel(Ox+3,Oy+3,color);
  tft.drawPixel(Ox+3,Oy+4,color);
  tft.drawPixel(Ox+3,Oy+5,color); // bottom of arrow

}

void drawDownArrow(uint16_t Ox,uint16_t Oy, uint16_t color) {
  tft.drawPixel(Ox+3,Oy+6,color);  // tip of arrowhead
  tft.drawPixel(Ox+2,Oy+5,color);
  tft.drawPixel(Ox+3,Oy+5,color);
  tft.drawPixel(Ox+4,Oy+5,color);
  tft.drawPixel(Ox+1,Oy+4,color);
  tft.drawPixel(Ox+2,Oy+4,color);
  tft.drawPixel(Ox+3,Oy+4,color);
  tft.drawPixel(Ox+4,Oy+4,color);
  tft.drawPixel(Ox+5,Oy+4,color);
  tft.drawPixel(Ox+3,Oy+3,color);
  tft.drawPixel(Ox+3,Oy+2,color);
  tft.drawPixel(Ox+3,Oy+1,color);
}
