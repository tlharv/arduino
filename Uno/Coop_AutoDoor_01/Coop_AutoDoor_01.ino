#include <Stepper.h>

#define STEPS 200

Stepper stepper(STEPS, 2, 3, 4, 5);

int steps = 0;

void setup()
{
  // set the speed of the motor to 30 rpm
  stepper.setSpeed(30);
  Serial.begin(9600);
}

void loop()
{
  if (Serial.available()) {
    char ch = Serial.read();
    
    if (isDigit(ch)) // is ch a number?
    {
      steps = steps * 10 + ch - '0'; // yes, accumulate the value
    }
    else if (ch == '+')
    {
      stepper.step(steps);
      steps = 0;
    }
    else if (ch == '-') {
      stepper.step(steps * -1);
      steps = 0;
    }
  }
}
