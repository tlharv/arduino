/*
  Web Server
 
 A simple web server that shows the value of the analog input pins.
 using an Arduino Wiznet Ethernet shield. 
 
 Circuit:
 * Ethernet shield attached to pins 10, 11, 12, 13
 * Analog inputs attached to pins A0 through A5 (optional)
 
 created 18 Dec 2009
 by David A. Mellis
 modified 9 Apr 2012
 by Tom Igoe
 
 */

#include <SPI.h>
#include <Ethernet.h>
// set up DHT11 temp/humidity sensor
#include "DHT.h"
#define DHTPIN 2
#define DHTTYPE DHT11
DHT dht(DHTPIN, DHTTYPE);

// Enter a MAC address and IP address for your controller below.
// The IP address will be dependent on your local network:
byte mac[] = { 
  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
IPAddress ip(192,168,254,87);

// Initialize the Ethernet server library
// with the IP address and port you want to use 
// (port 80 is default for HTTP):
EthernetServer server(8128);

float maxTf,minTf,maxH,minH;


void setup() {
 // Open serial communications and wait for port to open:
  Serial.begin(9600);
   while (!Serial) {
    ; // wait for serial port to connect. Needed for Leonardo only
  }


  // start the Ethernet connection and the server:
  Ethernet.begin(mac, ip);
  server.begin();
  Serial.print("server is at ");
  Serial.println(Ethernet.localIP());
  
  dht.begin();
  delay(2000);
  
  maxTf = 0;
  minTf = 0;
  maxH = 0;
  minH = 0;
  // Take initial readings for max/min values
  float h = dht.readHumidity();
  float tc = dht.readTemperature();
  float tf = (9.0 * tc / 5.0) + 32.0;
  maxTf = tf;
  minTf = maxTf;
  maxH = h;
  minH = maxH;
}


void loop() {
  // listen for incoming clients
  EthernetClient client = server.available();
  if (client) {
    Serial.println("new client");
    // an http request ends with a blank line
    boolean currentLineIsBlank = true;
    while (client.connected()) {
      if (client.available()) {
        char c = client.read();
        Serial.write(c);
        // if you've gotten to the end of the line (received a newline
        // character) and the line is blank, the http request has ended,
        // so you can send a reply
        if (c == '\n' && currentLineIsBlank) {
          // send a standard http response header
          client.println("HTTP/1.1 200 OK");
          client.println("Content-Type: text/html");
          client.println("Connnection: close");
          client.println();
          client.println("<!DOCTYPE HTML>");
          client.println("<html>");
          // add a meta refresh tag, so the browser pulls again every 20 seconds:
          client.println("<meta http-equiv=\"refresh\" content=\"20\">");
          
           // Take temperature and humidity readings
          float h = dht.readHumidity();
          float tc = dht.readTemperature();
          float tf = (9.0 * tc / 5.0) + 32.0;
          //double dpc = dewPoint(tc,h); // dew point in centigrade
          //double dpf = (9.0 * dpc / 5.0) + 32; // dew point in Fahrenheit
  
          // update max & min Temp and Humidity readings
          if (tf >= maxTf) maxTf = tf;
          if (tf < minTf) minTf = tf;
          if (h >= maxH) maxH = h;
          if (h < minH) minH = h;
          
          client.print("Conditions at the Harvey House");
          client.println("<br />"); 
          client.print("-----------------------------------------------------");
          client.println("<br />"); 
          client.print("Current temperature is ");
          client.print(tf);
          client.println("<br />"); 
          client.print("Current humidity is ");
          client.print(h);
          client.println("<br />");
          client.println("<br />");
          client.print("Max temperature so far has been ");
          client.print(maxTf);
          client.println("<br />"); 
          client.print("Min temperature so far has been ");
          client.print(minTf);
          client.println("<br />"); 
          client.println("<br />"); 
          client.print("Max humidity so far has been ");
          client.print(maxH);
          client.println("<br />"); 
          client.print("Min humidity so far has been ");
          client.print(minH);
          client.println("<br />"); 
          client.println("</html>");
          break;
        }
        if (c == '\n') {
          // you're starting a new line
          currentLineIsBlank = true;
        } 
        else if (c != '\r') {
          // you've gotten a character on the current line
          currentLineIsBlank = false;
        }
      }
    }
    // give the web browser time to receive the data
    delay(1);
    // close the connection:
    client.stop();
    Serial.println("client disonnected");
  }
}

