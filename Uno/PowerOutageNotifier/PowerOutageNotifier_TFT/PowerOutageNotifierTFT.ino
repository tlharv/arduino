// This sketch is to drive the power outage logger
// Display is a 1.44" TFT color display (Adafruit 2088)

#include <Adafruit_GFX.h>    // Core graphics library
#include <Adafruit_ST7735.h> // Hardware-specific library for ST7735
//#include <Adafruit_ST7789.h> // Hardware-specific library for ST7789
#include <SPI.h>


#define TFT_SCK 13  // SPI clock input pin
#define TFT_MISO 12 // SPI Master In Slave Out pin, used for the SD card
#define TFT_MOSI 11 // SPI Master Out Slave In pin, used to send data to the SD card & TFT
#define TFT_TCS 10  // TFT SPI chip select pin
#define TFT_RST 9   // TFT reset pin
#define TFT_DC 8    // TFT SPI data or command selector pin

// For 1.44" and 1.8" TFT with ST7735 use:
Adafruit_ST7735 tft = Adafruit_ST7735(TFT_TCS, TFT_DC, TFT_RST);



void setup() {
  // Initialize the TFT
  tft.initR(INITR_144GREENTAB); // Init ST7735R chip, green tab

  // Say hello
  tft.fillScreen(ST77XX_BLACK);
  testdrawtext("READY PLAYER 1", ST77XX_YELLOW);
    
}

void loop() {
  // put your main code here, to run repeatedly:

}

void testdrawtext(char *text, uint16_t color) {
  tft.setCursor(0, 0);
  tft.setTextColor(color);
  tft.setTextWrap(true);
  tft.print(text);
}
