 /***************************************************
  Uno_AdafruitTFT_01: Contains just the basic TFT output
  Uno_AdafruitTFT_02: Added DS18B20 temperature sensor and output
  Uno_AdafruitTFT_03: Added IR Receiver
  
 ****************************************************/

#include <Adafruit_GFX.h>    // Core graphics library
#include <Adafruit_ST7735.h> // Hardware-specific library
#include <SPI.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <IRremote.h>

// Data wire is plugged into D7 on the ATMega328P-PU, physical pin 13
#define ONE_WIRE_BUS 7
OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors (&oneWire);

// Configure the IR Receiving mechanism
int RECV_PIN = 6; // IR signal goes to D6, or pin 12 on ATMega328P-PU
IRrecv irrecv(RECV_PIN);
decode_results results;
#define POWER 0x10EFD827 
#define A 0x10EFF807 
#define B 0x10EF7887
#define C 0x10EF58A7
#define UP 0x10EFA05F
#define DOWN 0x10EF00FF
#define LEFT 0x10EF10EF
#define RIGHT 0x10EF807F
#define SELECT 0x10EF20DF

// For the breakout, you can use any 2 or 3 pins
// These pins will also work for the 1.8" TFT shield
#define TFT_CS     10
#define TFT_RST    9  // you can also connect this to the Arduino reset
                      // in which case, set this #define pin to 0!
#define TFT_DC     8

// Option 1 (recommended): must use the hardware SPI pins
// (for UNO thats sclk = 13 and sid = 11) and pin 10 must be
// an output. This is much faster - also required if you want
// to use the microSD card (see the image drawing example)
Adafruit_ST7735 tft = Adafruit_ST7735(TFT_CS,  TFT_DC, TFT_RST);

// Option 2: use any pins but a little slower!
#define TFT_SCLK 13   // set these to be whatever pins you like!
#define TFT_MOSI 11   // set these to be whatever pins you like!
//Adafruit_ST7735 tft = Adafruit_ST7735(TFT_CS, TFT_DC, TFT_MOSI, TFT_SCLK, TFT_RST);


void setup(void) {
  Serial.begin(9600);

  // Use this initializer if you're using a 1.8" TFT
  //tft.initR(INITR_BLACKTAB);   // initialize a ST7735S chip, black tab
  // Use this initializer (uncomment) if you're using a 1.44" TFT
  tft.initR(INITR_144GREENTAB);   // initialize a ST7735S chip, green tab
  tft.fillScreen(ST7735_BLACK); // blank out the screen
  tft.setCursor(0,0);
  tft.setTextColor(ST7735_CYAN);
  tft.setTextSize(1);
  tft.print("TFT ready "); tft.print(tft.width()); tft.print("x"); tft.println(tft.height());
  
  // Serial.println("TFT display STS7735S chip initialized.");
  // Start the IR receiver

  sensors.begin();  // start up the Temperature sensor
  tft.setTextColor(ST7735_MAGENTA);
  tft.println("Temp sensor ready.");

  irrecv.enableIRIn();
  tft.setTextColor(ST7735_GREEN);
  tft.println("IR receiver ready.");

  // large block of text
  // tft.fillScreen(ST7735_BLACK);
  // testdrawtext("test", ST7735_WHITE);
  // delay(8000);

  // tft.fillScreen(ST7735_BLACK);
  delay(5000);

  // tft.print("Current Temp ");
  // tft.print(TempC);
  // tft.println(TempF);
}

void loop() {
  float TempC, TempF;
  
  pinMode(ONE_WIRE_BUS, OUTPUT);
  
  // call sensors.requestTemperatures() to issue a global temp request to all devices on the bus
  sensors.requestTemperatures();
  pinMode(ONE_WIRE_BUS, INPUT);
  TempC = sensors.getTempCByIndex(0);
  TempF = (9.0 * TempC / 5.0) + 32.0;
  drawFrame();
  
  // Display the output
  //tft.fillScreen(ST7735_BLACK);
  tft.setCursor(28,40);
  tft.setTextColor(ST7735_GREEN);
  tft.setTextSize(1);
  tft.print("Temp = ");
  //tft.print(TempC);
  tft.println(TempF);
  
  if (irrecv.decode(&results)) 
  {
    if (results.value == POWER) 
    {
      Serial.println("POWER");    
    }
     if (results.value == A) 
    {
      Serial.println("A");    
    }
     if (results.value == B) 
    {
      Serial.println("B");  
    }
     if (results.value == C) 
    {
      Serial.println("C");
    }
     if (results.value == UP) 
    {
      Serial.println("UP");
    }
     if (results.value == DOWN) 
    {
      Serial.println("DOWN");
    }
     if (results.value == LEFT) 
    {
      Serial.println("LEFT");
    }
     if (results.value == RIGHT) 
    {
      Serial.println("RIGHT");
    }
     if (results.value == SELECT) 
    {
      Serial.println("SELECT");  
    }
    irrecv.resume();
  }
  delay(1000);
}


void testdrawtext(char *text, uint16_t color) {
  tft.setCursor(0, 0);
  tft.setTextColor(color);
  tft.setTextWrap(true);
  tft.print(text);
}

void drawFrame() {
  tft.fillScreen(ST7735_BLACK);
  tft.setCursor(0,10);
  tft.setTextSize(1);
  tft.setTextColor(ST7735_YELLOW);
  tft.println("       Current");
  tft.println("      Conditions");
  tft.println("");
  tft.drawLine(5,5,123,5,ST7735_RED);
  tft.drawLine(5,5,5,80,ST7735_RED);
  tft.drawLine(5,80,123,80,ST7735_RED);
  tft.drawLine(123,80,123,5,ST7735_RED);
  tft.drawLine(5,30,123,30,ST7735_RED);
}
