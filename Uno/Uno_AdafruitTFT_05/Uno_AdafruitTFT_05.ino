 /***************************************************
  Uno_AdafruitTFT_01: Contains just the basic TFT output
  Uno_AdafruitTFT_02: Added DS18B20 temperature sensor and output
  Uno_AdafruitTFT_03: Added IR Receiver
  Uno_AdafruitTFT_04: Added Graph axes
  Uno_AdafruitTFT_05: Added max/min temp and realtime temp plotting
  
 ****************************************************/

#include <Adafruit_GFX.h>    // Core graphics library
#include <Adafruit_ST7735.h> // Hardware-specific library
#include <SPI.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <IRremote.h>

// Data wire is plugged into D7 on the ATMega328P-PU, physical pin 13
#define ONE_WIRE_BUS 7
OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors (&oneWire);

// Configure the IR Receiving mechanism
int RECV_PIN = 6; // IR signal goes to D6, or pin 12 on ATMega328P-PU
IRrecv irrecv(RECV_PIN);
decode_results results;
#define POWER 0x10EFD827 
#define A 0x10EFF807 
#define B 0x10EF7887
#define C 0x10EF58A7
#define UP 0x10EFA05F
#define DOWN 0x10EF00FF
#define LEFT 0x10EF10EF
#define RIGHT 0x10EF807F
#define SELECT 0x10EF20DF

// For the breakout, you can use any 2 or 3 pins
// These pins will also work for the 1.8" TFT shield
#define TFT_CS     10
#define TFT_RST    9  // you can also connect this to the Arduino reset
                      // in which case, set this #define pin to 0!
#define TFT_DC     8

// Option 1 (recommended): must use the hardware SPI pins
// (for UNO thats sclk = 13 and sid = 11) and pin 10 must be
// an output. This is much faster - also required if you want
// to use the microSD card (see the image drawing example)
Adafruit_ST7735 tft = Adafruit_ST7735(TFT_CS,  TFT_DC, TFT_RST);

// Option 2: use any pins but a little slower!
#define TFT_SCLK 13   // set these to be whatever pins you like!
#define TFT_MOSI 11   // set these to be whatever pins you like!
//Adafruit_ST7735 tft = Adafruit_ST7735(TFT_CS, TFT_DC, TFT_MOSI, TFT_SCLK, TFT_RST);

const long oneSecond = 1000;
const long oneMinute = oneSecond * 60;

float TempC, TempF, maxTempF, minTempF;

float datapoint[128];
long counter = 0;

void setup(void) {
  Serial.begin(9600);
  
  // Initialize TFT display
  tft.initR(INITR_144GREENTAB);   // initialize a ST7735S chip, green tab
  delay(100);
  // uint8_t Rotation = 1; // 0 = no rotation, 1 = 90 deg, 2 = 180 deg and 3 = 270 deg.
  // tft.setRotation(Rotation);
  tft.fillScreen(ST7735_BLACK); // blank out the screen
  tft.setCursor(0,0);
  tft.setTextColor(ST7735_CYAN);
  tft.setTextSize(1);
  tft.print("TFT ready: "); tft.print(tft.width()); tft.print("x"); tft.println(tft.height());
  
  // Initialize temperature sensor and take first readings to baseline max/min temps
  sensors.begin();  
  pinMode(ONE_WIRE_BUS, INPUT);
  tft.setTextColor(ST7735_MAGENTA);
  tft.println("Temp sensor ready.");
  sensors.requestTemperatures();
  TempC = sensors.getTempCByIndex(0);
  TempF = (9.0 * TempC / 5.0) + 32.0;
  maxTempF = TempF;
  minTempF = TempF;

  // Initialize IR receiver
  irrecv.enableIRIn();
  tft.setTextColor(ST7735_GREEN);
  tft.println("IR receiver ready.");

  delay(1000);
}

void loop() {
  //float TempC, TempF;
  
  pinMode(ONE_WIRE_BUS, OUTPUT);
  
  // call sensors.requestTemperatures() to issue a global temp request to all devices on the bus
  sensors.requestTemperatures();
  //pinMode(ONE_WIRE_BUS, INPUT);
  TempC = sensors.getTempCByIndex(0);
  TempF = (9.0 * TempC / 5.0) + 32.0;

  // update max & min Temp and Humidity readings
  if (TempF >= maxTempF) maxTempF = TempF;
  if (TempF < minTempF) minTempF = TempF;
  
  tft.fillScreen(ST7735_BLACK); // blank out the screen

  drawFrame();
  drawAxes();
  drawGrid();
  tft.drawFastHLine(2,(126-77),126,ST7735_GREEN); // horizontal line at 78 degrees

  
  // Display the output
  //tft.fillScreen(ST7735_BLACK);
  tft.setCursor(0,2);
  tft.setTextColor(ST7735_YELLOW);
  tft.setTextSize(1);
  tft.print("Temp");
  tft.setCursor(0,12);
  tft.setTextSize(2);
  tft.println(TempF);
  drawUpArrow(80,2,ST7735_RED);
  tft.setCursor(90,2);
  tft.setTextSize(1);
  tft.setTextColor(ST7735_RED);
  tft.println(maxTempF);
  drawDownArrow(80,13,ST7735_CYAN);
  tft.setCursor(90,13);
  tft.setTextColor(ST7735_CYAN);
  tft.println(minTempF);
  
  // Add this temp value to the array
  datapoint[counter] = TempF;
  Serial.print("datapoint["); Serial.print(counter); Serial.print("] = "); Serial.println(datapoint[counter]);
  //plotTemp(counter,datapoint[counter]);
  
  // Plot all of the points
  for (int x = 0; x < counter; x++) {
    plotTemp(x,datapoint[x]);
  }
  counter++;
  if (counter == 128)
  {
    counter = 0;
  }
  

  // Decode any signal received from the IR sensor  
  if (irrecv.decode(&results)) 
  {
    if (results.value == POWER) 
    {
      Serial.println("POWER");    
    }
     if (results.value == A) 
    {
      Serial.println("A");    
    }
     if (results.value == B) 
    {
      Serial.println("B");  
    }
     if (results.value == C) 
    {
      Serial.println("C");
    }
     if (results.value == UP) 
    {
      Serial.println("UP");
    }
     if (results.value == DOWN) 
    {
      Serial.println("DOWN");
    }
     if (results.value == LEFT) 
    {
      Serial.println("LEFT");
    }
     if (results.value == RIGHT) 
    {
      Serial.println("RIGHT");
    }
     if (results.value == SELECT) 
    {
      Serial.println("SELECT");  
    }
    irrecv.resume();
  }
  
   delay(1 * oneMinute);  // 11 minute delay makes the graph span one 24 hour period
   // delay(2 * oneSecond); // good for testing
}


void testdrawtext(char *text, uint16_t color) {
  tft.setCursor(0, 0);
  tft.setTextColor(color);
  tft.setTextWrap(true);
  tft.print(text);
}

void drawFrame() {
  uint16_t OriginX,OriginY;
  uint16_t BigBoxHeight,BigBoxWidth;
  
  tft.fillScreen(ST7735_BLACK);

  //OriginX = 16;
  //OriginY = 2;
  //BigBoxWidth = tft.width() - (2*OriginX);
  //BigBoxHeight = 40;
  //tft.drawRect(OriginX,OriginY,BigBoxWidth,BigBoxHeight,ST7735_BLUE);
  
  //tft.setCursor(0,7);
  //tft.setTextSize(1);
  //tft.setTextColor(ST7735_YELLOW);
  //tft.println("       Current");
  //tft.println("      Conditions");
}

void drawAxes() {
  // Set up axes so that horizontal range is 126 cycles and vertical range is 100 degrees F
  tft.drawFastHLine(2,126,126,ST7735_RED);  // horizontal axis
  tft.drawFastVLine(2,27,100,ST7735_RED);  // vertical axis
  tft.setCursor(0,120);
  tft.setTextSize(1);
  tft.setTextColor(ST7735_WHITE);
  tft.println("0");  // show scale min = 0 fahrenheit
  // tft.setCursor(0,16);
  // tft.println("100"); // show scale max = +100 fahrenheit
  tft.drawFastHLine(2,(126-32),126,ST7735_CYAN); // horizontal line at freezing
}

void plotTemp(int counter,float Temp) {
  // This function plots a point on the screen according to the counter value
  tft.drawPixel(2+counter,128-Temp,ST7735_YELLOW);
}

void drawGrid() {
  // Draw grid of 10 pixel increments
  int increment = 10;
  uint16_t color = ST7735_BLUE;
  
  // origin is at 3,126 for horizontal lines
  uint16_t Ox = 2;
  uint16_t Oy = 127;
  for (int x = 1; x < 11; x++) {
    tft.drawFastHLine(Ox,Oy-(x*increment),125,color);
  }
  Ox = 1;
  Oy = 27;
  for (int x = 1; x < 13; x++) {
    tft.drawFastVLine(Ox+(x*increment),Oy,100,color);
  }
}

void drawUpArrow(uint16_t Ox,uint16_t Oy, uint16_t color) {
  tft.drawPixel(Ox+3,Oy,color);   // tip of arrowhead
  tft.drawPixel(Ox+2,Oy+1,color);
  tft.drawPixel(Ox+3,Oy+1,color);
  tft.drawPixel(Ox+4,Oy+1,color);
  tft.drawPixel(Ox+1,Oy+2,color);
  tft.drawPixel(Ox+2,Oy+2,color);
  tft.drawPixel(Ox+3,Oy+2,color);
  tft.drawPixel(Ox+4,Oy+2,color);
  tft.drawPixel(Ox+5,Oy+2,color);
  tft.drawPixel(Ox+3,Oy+3,color);
  tft.drawPixel(Ox+3,Oy+4,color);
  tft.drawPixel(Ox+3,Oy+5,color); // bottom of arrow

}

void drawDownArrow(uint16_t Ox,uint16_t Oy, uint16_t color) {
  tft.drawPixel(Ox+3,Oy+6,color);  // tip of arrowhead
  tft.drawPixel(Ox+2,Oy+5,color);
  tft.drawPixel(Ox+3,Oy+5,color);
  tft.drawPixel(Ox+4,Oy+5,color);
  tft.drawPixel(Ox+1,Oy+4,color);
  tft.drawPixel(Ox+2,Oy+4,color);
  tft.drawPixel(Ox+3,Oy+4,color);
  tft.drawPixel(Ox+4,Oy+4,color);
  tft.drawPixel(Ox+5,Oy+4,color);
  tft.drawPixel(Ox+3,Oy+3,color);
  tft.drawPixel(Ox+3,Oy+2,color);
  tft.drawPixel(Ox+3,Oy+1,color);
}
