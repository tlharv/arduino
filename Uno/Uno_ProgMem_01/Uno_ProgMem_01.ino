/* ir-distance_Progmem sketch
 * prints distance & changes LED flash rate
 * depending on distance from IR sensor
 * uses progmem for table */

#include <avr/pgmspace.h> // needed when using Progmem

// table entries are distances in steps of 250 millivolts
const int TABLE_ENTRIES = 12;
const int firstElement = 250; // first entry is 250 mV
const int interval = 250; // millivolts between each element

// the following is the definition of the table in Program Memory
const int distanceP[TABLE_ENTRIES] PROGMEM ={
  150,140,130,100,60,50,40,35,30,25,20,15 };

// This function reads from Program Memory at the given index
int getTableEntry(int index)
{
  int value = pgm_read_word(&distanceP[index]);
  return value;
}

void setup()
{
}

void loop()
{
}

