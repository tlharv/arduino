// 8-Bit latching shift register 74HC595

int SER_Pin = 4;
int RCLK_Pin = 5;
int SRCLK_Pin = 6;

//Define how many shift registers we're using
#define number_of_74HC595s 1

//Define total number of shift pins (always will be 8 per chip)
#define numOfRegisterPins number_of_74HC595s * 8

boolean registers[numOfRegisterPins];

void setup()
{
  pinMode(SER_Pin, OUTPUT);
  pinMode(RCLK_Pin, OUTPUT);
  pinMode(SRCLK_Pin, OUTPUT);
  
  // reset all register pins
  clearRegisters();
  writeRegisters();
}

//set all register pins to LOW
void clearRegisters()
{
  for (int i = numOfRegisterPins - 1; i>=0; i--)
  {
    registers[i] = LOW;
  }
}

//set and display registers
//Only call AFTER all values are set how you would like (slow otherwise)
void writeRegisters()
{
  digitalWrite(RCLK_Pin, LOW);
  
  for (int i = numOfRegisterPins-1; i>=0; i--)
  {
    digitalWrite(SRCLK_Pin, LOW);
    int val = registers[i];
    digitalWrite(SER_Pin, val);
    digitalWrite(SRCLK_Pin, HIGH);
  }
  digitalWrite(RCLK_Pin, HIGH);
}

//set an individual pin HIGH or LOW
void setRegisterPin(int index, int value)
{
  registers[index] = value;
}

void loop()
{
  int i = random(numOfRegisterPins);
  
  for (int count=0; count<numOfRegisterPins; count++)
  {
    if (count== i)
    {
      setRegisterPin(count,HIGH);
    }
    else
    {
      setRegisterPin(count,LOW);
    }
  }
  
  writeRegisters(); // must be called to display changes
  delay(170);
}
