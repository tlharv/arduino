/* Test sketch for Adafruit PM2.5 sensor with UART or I2C */

#include "Adafruit_PM25AQI.h"
int GRNLED = 5;
int YELLED = 7;
int REDLED = 6;
int BLULED = 4;

// If your PM2.5 is UART only, for UNO and others (without hardware serial) 
// we must use software serial...
// pin #2 is IN from sensor (TX pin on sensor), leave pin #3 disconnected
// comment these two lines if using hardware serial
#include <SoftwareSerial.h>
SoftwareSerial pmSerial(2, 3);

Adafruit_PM25AQI aqi = Adafruit_PM25AQI();

void setup() {
  // Wait for serial monitor to open
  Serial.begin(115200);
  while (!Serial) delay(10);

  Serial.println("Adafruit PMSA003I Air Quality Sensor");

  // Wait one second for sensor to boot up!
  delay(1000);

  // If using serial, initialize it and set baudrate before starting!
  // Uncomment one of the following
  //Serial1.begin(9600);
  pmSerial.begin(9600);

  // There are 3 options for connectivity!
  //if (! aqi.begin_I2C()) {      // connect to the sensor over I2C
  //if (! aqi.begin_UART(&Serial1)) { // connect to the sensor over hardware serial
  if (! aqi.begin_UART(&pmSerial)) { // connect to the sensor over software serial 
    Serial.println("Could not find PM 2.5 sensor!");
    while (1) delay(10);
  }

  Serial.println("PM25 found!");

  pinMode(GRNLED,OUTPUT);
  pinMode(YELLED,OUTPUT);
  pinMode(REDLED,OUTPUT);
  pinMode(BLULED,OUTPUT);
}

void loop() {
  digitalWrite(REDLED,LOW);
  PM25_AQI_Data data;

  //digitalWrite(13,HIGH);
  //delay(500);
  //digitalWrite(13,LOW);
  
  if (! aqi.read(&data)) {
    Serial.println("Could not read from AQI");
    delay(500);  // try again in a bit!
    return;
  }
  Serial.println("AQI reading success");

  Serial.println();
  Serial.println(F("---------------------------------------"));
  Serial.println(F("Concentration Units (standard)"));
  Serial.println(F("---------------------------------------"));
  Serial.print(F("PM 1.0: ")); Serial.print(data.pm10_standard);
  Serial.print(F("\t\tPM 2.5: ")); Serial.print(data.pm25_standard);
  Serial.print(F("\t\tPM 10: ")); Serial.println(data.pm100_standard);
  Serial.println(F("Concentration Units (environmental)"));
  Serial.println(F("---------------------------------------"));
  Serial.print(F("PM 1.0: ")); Serial.print(data.pm10_env);
  Serial.print(F("\t\tPM 2.5: ")); Serial.print(data.pm25_env);
  Serial.print(F("\t\tPM 10: ")); Serial.println(data.pm100_env);
  Serial.println(F("---------------------------------------"));
  Serial.print(F("Particles > 0.3um / 0.1L air:")); Serial.println(data.particles_03um);
  Serial.print(F("Particles > 0.5um / 0.1L air:")); Serial.println(data.particles_05um);
  Serial.print(F("Particles > 1.0um / 0.1L air:")); Serial.println(data.particles_10um);
  Serial.print(F("Particles > 2.5um / 0.1L air:")); Serial.println(data.particles_25um);
  Serial.print(F("Particles > 5.0um / 0.1L air:")); Serial.println(data.particles_50um);
  Serial.print(F("Particles > 10 um / 0.1L air:")); Serial.println(data.particles_100um);
  Serial.println(F("---------------------------------------"));

  int PM25 = data.pm25_env;
  //int PM10 = data.pm10_env;

  if (PM25 < 51) {               // GOOD
    digitalWrite(GRNLED,HIGH);
    digitalWrite(YELLED,LOW);
    digitalWrite(REDLED,LOW);
    digitalWrite(BLULED,LOW);
  }

  if (PM25 > 50 && PM25 < 101) { // Moderate
    digitalWrite(GRNLED,LOW);
    digitalWrite(YELLED,HIGH);
    digitalWrite(REDLED,LOW);
    digitalWrite(BLULED,LOW);
  }

  if (PM25 > 100 && PM25 < 201) { // Captures Unhealthy for sensitive groups and overall unhealthy
    digitalWrite(GRNLED,LOW);
    digitalWrite(YELLED,LOW);
    digitalWrite(REDLED,HIGH);
    digitalWrite(BLULED,LOW);
  }


  if (PM25 > 200) { // captures Very Unhealthy and Hazardous
    digitalWrite(GRNLED,LOW);
    digitalWrite(YELLED,LOW);
    digitalWrite(REDLED,LOW);
    digitalWrite(BLULED,HIGH);
  }
  delay(5000);

  // Clear the LEDs so you know if the routine has hung or not
  digitalWrite(GRNLED,LOW);
  digitalWrite(YELLED,LOW);
  digitalWrite(REDLED,LOW);
  digitalWrite(BLULED,LOW);
  delay(100);
}
