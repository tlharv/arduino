const int DelayOn = 600;
const int DelayOff = 500;
int latchPin = 5;
int clockPin = 6;
int dataPin = 4;
const int piezoPin = 7;

byte leds = 0;

void setup() 
{
  pinMode(latchPin, OUTPUT);
  pinMode(dataPin, OUTPUT);  
  pinMode(clockPin, OUTPUT);
}

void loop() 
{
  leds = 0;
  updateShiftRegister();
  delay(DelayOff);
  for (int i = 0; i < 6; i++)
  {
    bitSet(leds, i);
    updateShiftRegister();
    //delay(200);
  }
  tone(piezoPin, 294);
  delay(DelayOn);
  noTone(piezoPin);
}

void updateShiftRegister()
{
   digitalWrite(latchPin, LOW);
   shiftOut(dataPin, clockPin, LSBFIRST, leds);
   digitalWrite(latchPin, HIGH);
}


