/******************************************************************************

  This sketch prints the temperature and humidity the Serial port.

  The library used in this example can be found here:
  https://github.com/sparkfun/Si7021_Breakout/tree/master/Libraries

  Hardware Connections:
      HTU21D ------------- Photon
      (-) ------------------- GND
      (+) ------------------- 3.3V (VCC)
       CL ------------------- SCL
       DA ------------------- SDA

*******************************************************************************/
#include "SparkFun_Si7021_Breakout_Library.h"
#include <Wire.h>
#include <XBee.h>

// Create the XBee object
XBee xbee = XBee();

uint8_t payload[] = { 0, 0 };

// SH + SL Address of receiving XBee
XBeeAddress64 addr64 = XBeeAddress64(0x0013a200, 0x4164D656);
ZBTxRequest zbTx = ZBTxRequest(addr64, payload, sizeof(payload));
ZBTxStatusResponse txStatus = ZBTxStatusResponse();

float humidity = 0;
float tempf = 0;

//Create Instance of SI7021 temp and humidity sensor
Weather sensor;

//---------------------------------------------------------------
void setup()
{
    Serial.begin(9600);   // open serial over USB at 9600 baud

    //Initialize the I2C sensors and ping them
    sensor.begin();

    xbee.setSerial(Serial);

}
//---------------------------------------------------------------
void loop()
{
    //Get readings from all sensors
    humidity = sensor.getRH();
    tempf = sensor.getTempF();

    payload[0] = tempf;
    payload[1] = humidity;
    xbee.send(zbTx);
    
    //getWeather();
    //printInfo();
    
    
    delay(5000);

}
//---------------------------------------------------------------
void getWeather()
{
  // Measure Relative Humidity from the HTU21D or Si7021
  humidity = sensor.getRH();

  // Measure Temperature from the HTU21D or Si7021
  tempf = sensor.getTempF();
  // Temperature is measured every time RH is requested.
  // It is faster, therefore, to read it from previous RH
  // measurement with getTemp() instead with readTemp()
}
//---------------------------------------------------------------
void printInfo()
{
//This function prints the weather data out to the default Serial Port

  Serial.print("Payload sent!");
  Serial.print("   Temp:");
  Serial.print(tempf);
  Serial.print("F, ");

  Serial.print("Humidity:");
  Serial.print(humidity);
  Serial.print("%   ");
  
  Serial.print("payload[0]=");
  Serial.print(payload[0]);
  Serial.print(", payload[1]=");
  Serial.println(payload[1]);

}
