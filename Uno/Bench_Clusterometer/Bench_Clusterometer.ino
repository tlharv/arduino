#include <OneWire.h>
#include <DallasTemperature.h>

// ATMEL ATTINY85 / ARDUINO
//
//                  +-\/-+
// Ain0 (D 5) PB5  1|    |8  Vcc
// Ain3 (D 3) PB3  2|    |7  PB2 (D 2) Ain1
// Ain2 (D 4) PB4  3|    |6  PB1 (D 1) pwm1
//            GND  4|    |5  PB0 (D 0) pwm0
//                  +----+
//

#define redPin 4   // analog out, to PB0 for red LED, pin 5 on ATtiny85
#define bluePin 5  // analog out, to PB1 for blue LED, pin 6 on ATtiny85
#define greenPin 7 // analog out, to PB4 for green LED, pin 3 on ATtiny85

#define ONE_WIRE_BUS 11

#include<avr/sleep.h>


// Setup a oneWire instance to communicate with any OneWire device
OneWire oneWire(ONE_WIRE_BUS);

// Pass our oneWire reference to Dallas Temperature
DallasTemperature sensors(&oneWire);

float Temp_F;

void setup(void)
{
  // Start up the library
  sensors.begin();
  pinMode(bluePin, OUTPUT);
  pinMode(redPin, OUTPUT);
  pinMode(greenPin, OUTPUT);
  Serial.begin(9600);
  Serial.println("Breadboard Clusterometer");
}

void loop(void)
{
  // call sensors.requestTemperature() to issue a global temperature request to all devices on the bus
  sensors.requestTemperatures();
  Temp_F = sensors.getTempFByIndex(0);
  Serial.println(Temp_F);
  
  // if temp is between 64-68, flash green.  If under, flash blue.  If over, flash red.
  if (Temp_F < 65.0) {
    GlowLightOnce(bluePin);
  }
  else if (Temp_F > 70.0) {
    GlowLightOnce(redPin);
  }
  else {
    GlowLightOnce(greenPin);
  }

}



void GlowLightOnce(int bulb)  // subroutine applies one fade in - fade out blink of an LED
{
  int brightness = 0;
  int fadeAmount = 5;
  int totalcount = 0;

  do
  {
    analogWrite(bulb, brightness);
    brightness = brightness + fadeAmount;
    totalcount++;
    if (brightness == 255)
    {
      fadeAmount = -fadeAmount;
    }
    delay(35);
  } while (totalcount < 103);
  IdleChip(1000);
}


void IdleChip(unsigned long duration)
{
  unsigned long start = millis();
  set_sleep_mode(SLEEP_MODE_IDLE);  // choose idle mode for the chip
  sleep_enable(); // this puts the chip into sleep mode, but doesn't command it to sleep yet
  while (millis() - start <= duration) {
    sleep_mode();  // put chip in idle mode
  }
  sleep_disable();
}

