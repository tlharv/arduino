#include <Adafruit_NeoPixel.h>

#define PIN 6

// Parameter 1 = number of pixels in strip
// Parameter 2 = Arduino pin number (most are valid)
// Parameter 3 = pixel type flags, add together as needed:
//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
//   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
//   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
//   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)
Adafruit_NeoPixel strip = Adafruit_NeoPixel(2, PIN, NEO_GRB + NEO_KHZ800);

// IMPORTANT: To reduce NeoPixel burnout risk, add 1000 uF capacitor across
// pixel power leads, add 300 - 500 Ohm resistor on first pixel's data input
// and minimize distance between Arduino and first pixel.  Avoid connecting
// on a live circuit...if you must, connect GND first.

uint32_t red = strip.Color(255,0,0);
uint32_t green = strip.Color(0,255,0);
uint32_t blue = strip.Color(0,0,255);
uint32_t magenta = strip.Color(255,0,255);
uint32_t cyan = strip.Color(0,255,255);
uint32_t white = strip.Color(255,255,255);
uint32_t paleyellow = strip.Color(255,255,0);
uint32_t off = strip.Color(0,0,0);

uint32_t orange = strip.Color(255,102,0);
uint32_t pumpkin = strip.Color(255,60,0);
uint32_t gold = strip.Color(255,180,0);
uint32_t pink = strip.Color(255,114,86);

void setup() {
  strip.begin();
  strip.show(); // Initialize all pixels to 'off'
}

void loop() {
  for(int i=0; i<150; i++)
  {
    strip.setBrightness(i);
    strip.setPixelColor(0,pumpkin);
    strip.setPixelColor(1,pumpkin);
    strip.show();
    delay(15);
  }
  //delay(1500);
  //strip.setPixelColor(0,green);
  //strip.setPixelColor(1,green);
  //strip.show();
  //delay(1500);
  //strip.setPixelColor(0,blue);
  //strip.setPixelColor(1,blue);
  //strip.show();
  //delay(1500);
}



