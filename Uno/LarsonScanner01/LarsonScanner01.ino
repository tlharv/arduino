
int pin[10];
const int phasedelay = 150;
const int NumOfLEDs = 10;

void setup()
{
  for (int i=0; i<NumOfLEDs; i++)
  {
    pin[i] = i+3;
    pinMode(pin[i],OUTPUT);
  }
}

void loop()
{
  for (int j=0; j<NumOfLEDs; j++)
  {
    digitalWrite(pin[j], HIGH);
    delay(phasedelay);
    digitalWrite(pin[j], LOW);
  }
}
