#include <OneWire.h>
#include <avr/sleep.h>

// pin assignments are for Arduino Uno
const int redPin = 3;   // digital pin 3
const int greenPin = 5;  // digital pin 5
const int bluePin = 6;  // digital pin 6
const int oneWirePower = 4; // digital pin 4
const int LL = 63; // Lower Limit of acceptable temperature range; below this the light is blue
const int UL = 73; // Upper Limit of acceptable temperature range; above this the light is red
OneWire  ds(10);  // on digital pin 10


void setup(void) {
  pinMode(bluePin, OUTPUT);
  pinMode(redPin, OUTPUT);
  pinMode(greenPin, OUTPUT);
  pinMode(oneWirePower, OUTPUT);
  wdtSetup();  // this sets up the watchdog timer
  Serial.println ("Watchdog timer set up");
}


void loop(void) {
  byte i;
  byte present = 0;
  byte data[12];
  byte addr[8];
  int Temp;
  
  digitalWrite(13, HIGH); // Chip is awake
  digitalWrite(oneWirePower, HIGH);  // send power to the temp sensor
  
// GET THE TEMPERATURE FROM THE DALLAS ONE-WIRE TEMP SENSOR
  if ( !ds.search(addr)) {  // if address search fails, 
      ds.reset_search();
      return;
  }
  ds.reset();
  ds.select(addr);
  ds.write(0x44,1); // start conversion, with parasite power on at the end
  
  // This delay is used with the one-wire sensor for parasitic power.
  IdleChip(750);
  
  present = ds.reset();
  ds.select(addr);    
  ds.write(0xBE);         // Read Scratchpad
  for ( i = 0; i < 9; i++) // we need 9 bytes
  {           
    data[i] = ds.read();
  }
  
  //take the two bytes from the response relating to temperature
  Temp=(data[1]<<8)+data[0];
  Temp=Temp>>4; //divide by 16 to get pure Celsius readout
  Temp=Temp*1.8+32; // comment this line out to get Celsius
  digitalWrite(oneWirePower, LOW);  // cease sending power to the temp sensor
  
  
// NOW LIGHT THE LED BASED ON THE TEMPERATURE READ
  if (Temp < LL) // then light the blue LED - it's cold!
  {
    analogWrite(greenPin, 0);
    analogWrite(redPin, 0);
    GlowLightOnce(bluePin);
  }
  else if (Temp >= LL && Temp <= UL) // then light the green LED
  {
    analogWrite(bluePin, 0);
    analogWrite(redPin, 0);
    GlowLightOnce(greenPin);
  }
  else // light the red LED
  {
    analogWrite(bluePin, 0);
    analogWrite(greenPin, 0);
    GlowLightOnce(redPin);
  }
  
  // DEFINE SLEEP MODE
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);  // choose your preferred sleep mode for the chip
  sleep_enable(); // this puts the chip into, sleep mode, but doesn't command it to sleep yet.
  // Define conditions by which chip should awake (watchdog timer for 1 second)

  // Go to sleep...
  digitalWrite(13, LOW); // turn LED off to indicate sleep
  sleep_mode();  // Last statement of loop() - this command actually puts the chip to sleep.
}

// Watchdog Timer Interrupt
ISR (WDT_vect) {
  // put whatever you want to have happen at regular WDT intervals in here.
}


void GlowLightOnce(int bulb)  // subroutine applies one fade in - fade out blink of an LED
{
  int brightness = 0;
  int fadeAmount = 5;
  int totalcount = 0;

  do
  {
    analogWrite(bulb, brightness);
    brightness = brightness + fadeAmount;
    totalcount++;
    if (brightness == 255)
    {
      fadeAmount = -fadeAmount;
    }
    //delay(35);
    IdleChip(35);
  } while (totalcount < 103);
}

void wdtSetup() // Set up watchdog timer
{
  cli(); // disable all interrupts
  MCUSR = 0;  // mystery statement, but is required
  // WDTCSR register gives you access to the Arduino WDT.
  // Bit 4 = Watchdog Change Enable.  Must be cleared to change or clear the WDE bit.
  // Bit 3 = WDT bit.  Must be cleared to set WDP3 through WDP0.
  
//  Watchdog Timer Prescale Select

//WDP3 WDP2 WDP1 WDP0         Number of WDT     Typical Time-out at
//                                              Oscillator Cycles     VCC = 5.0V

//   0         0      0        0                2K (2048) cycles        0.016 s
//   0         0      0        1                4K (4096) cycles        0.032 s
//   0         0      1        0                8K (8192) cycles        0.064 s
//   0         0      1        1               16K (16384) cycles       0.125 s
//   0         1      0        0               32K (32768) cycles       0.25 s
//   0         1      0        1               64K (65536) cycles       0.5 s
//   0         1      1        0              128K (131072) cycles      1.0 s
//   0         1      1        1              256K (262144) cycles      2.0 s
//   1         0      0        0              512K (524288) cycles      4.0 s
//   1         0      0        1             1024K (1048576) cycles     8.0 s

  WDTCSR |= B00011000; // clear WDTCSR register bits 3 & 4
  WDTCSR = B01000111;  // Set the WDP3 through WDP0 bits, to set the WDT prescale to two seconds.
  sei(); // enables all interrupts
}

void IdleChip(unsigned long duration)
{
  unsigned long start = millis();
  set_sleep_mode(SLEEP_MODE_IDLE);  // choose idle mode for the chip
  sleep_enable();                              // this puts the chip into sleep mode, but doesn't command it to sleep yet
  digitalWrite(13, LOW);  // turn LED off to indicate sleep
  while (millis() - start <= duration) {
    sleep_mode();  // put chip in idle mode
  }
  sleep_disable();
  digitalWrite(13, HIGH); // turn LED on to indicate waking up
}
