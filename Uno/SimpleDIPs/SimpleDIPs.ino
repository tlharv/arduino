// This sketch assumes that one end of each DIP switch goes to ground, and the other end goes to Vcc through a 10K pullup resistor.
// The input pin checks the signal at the pullup resistor.


// Arduino pins used for the switches

#define S1 2
#define S2 3

// State of each switch (0 or 1)
int s1state;
int s2state;
char DSS[2]; // 2-character string for the DIP switch state

void setup() {

  // pins for switches are inputs
  pinMode(S1, INPUT);
  pinMode(S2, INPUT);

  // setup serial port
  Serial.begin(9600);
  Serial.println("Serial port open");
}

void loop() {
  s1state = 1-digitalRead(S1);
  s2state = 1-digitalRead(S2);
  int reading = 10*s1state + s2state;

  // Make a character string that holds 00, 01, 10 or 11 showing DIP switch setting
  //DSS[0] = char(s1state);
  //DSS[1] = char(s2state);
  //Serial.println(DSS);
  
  Serial.print(reading);
  Serial.print(" translates to ");
  
  switch (reading)
  {
    case 0:
      Serial.print ("1 minute");
      break;
    case 1:
      Serial.print ("5 minutes");
      break;
    case 10:
      Serial.print ("10 minutes");
      break;
    case 11:
      Serial.print ("15 minutes");
      break;
  } 
  //Serial.print(s2state,BIN);
  Serial.println();
  delay(1000);
}

