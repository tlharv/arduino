#include <OneWire.h>
#include <DallasTemperature.h>

// This sketch is just to test that a DS18B20 temp sensor is working.
// It runs off of an Uno board.

// Data wire is plugged into D3 on the ATtiny85, physical pin 2
#define ONE_WIRE_BUS 3


// Conversion Table
// 16C = 60.8 F
// 17C = 62.6 F
// 18C = 64.4 F
// 19C = 66.2 F
// 20C = 68.0 F
// 21C = 69.8 F
// 22C = 71.6 F
// 23C = 73.4 F
// 24C = 75.2 F
// 25C = 77.0 F
// 26C = 78.8 F

OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors (&oneWire);

void setup()
{
  sensors.begin(); // Start up the library
  Serial.begin(9600);
}

void loop()
{
  int TempC;
  int TempF;
  
  pinMode(ONE_WIRE_BUS, OUTPUT);
  
  // call sensors.requestTemperatures() to issue a global temp request to all devices on the bus
  sensors.requestTemperatures();
  
  pinMode(ONE_WIRE_BUS, INPUT);
  TempC = sensors.getTempCByIndex(0);
  TempF = (9*TempC/5) + 32;
  
  Serial.print ("Temp read is ");
  Serial.println (TempF);
  
  delay(5000);
}
