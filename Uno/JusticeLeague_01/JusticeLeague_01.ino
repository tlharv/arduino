#include <LiquidCrystal.h>

// Constants for the number of rows and columns in the LCD
const int numRows = 2;
const int numCols = 16;

// initialize the library with the numbers of the interface pins
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

/* This is the pin arrangement needed, going from pin 1 to 16 on the LCD
Pin 1 (LED-) goes to Uno GND, also to GND pin of rotary pot to adjust LCD dimmer
Pin 2 (LED+) goes to Uno 5V, also to 5V pin of rotary pot to adjust LCD dimmer
Pin 3 (DB7) goes to output of rotary pot to adjust LCD dimmer
Pin 4 (DB6) goes to Uno pin D12
Pin 5 (DB5) goes to Uno GND
Pin 6 (DB4) goes to Uno pin D11
Pin 7 (DB3) is not used
Pin 8 (DB2) is not used
Pin 9 (DB1) is not used
Pin 10 (DB0) is not used
Pin 11 (E) goes to Uno pin D5
Pin 12 (RW) goes to Uno pin D4
Pin 13 (RS) goes to Uno pin D3
Pin 14 (V0) goes to Uno pin D2
Pin 15 (VDD) goes to Uno 5V
Pin 16 (VSS) goes to a 220 ohm resistor, then to Uno GND
*/


void setup()
{
  lcd.begin (numCols, numRows);
}

void loop()
{
  int i = random(12);
  lcd.clear();
  switch (i)
  {
    case 0:
      lcd.setCursor(0,0);
      lcd.print("WONDER WOMAN HAS");
      lcd.setCursor(0,1);
      lcd.print("PANTIES IN TWIST");
      break;
    case 1:
      lcd.setCursor(0,0);
      lcd.print("AQUAMAN NEEDS");
      lcd.setCursor(0,1);
      lcd.print("HELP RIGHT AWAY");
      break;
    case 2:
      lcd.setCursor(0,0);
      lcd.print("GIANT IGUANA IS");
      lcd.setCursor(0,1);
      lcd.print("WRECKING SEATTLE");
      break;
    case 3:
      lcd.setCursor(0,0);
      lcd.print("ASTEROID HEADING");
      lcd.setCursor(0,1);
      lcd.print("TOWARD THE EARTH");
      break;
    case 4:
      lcd.setCursor(0,0);
      lcd.print("HUGE TORNADO");
      lcd.setCursor(0,1);
      lcd.print("OVER MALTBY ELEM");
      break;
    case 5:
      lcd.setCursor(0,0);
      lcd.print("VOLCANO ERUPTING");
      lcd.setCursor(0,1);
      lcd.print("PEOPLE IN DANGER");
      break;
    case 6:
      lcd.setCursor(0,0);
      lcd.print("LEX LUTHOR HAS");
      lcd.setCursor(0,1);
      lcd.print("ESCAPED PRISON!");
      break;
    case 7:
      lcd.setCursor(0,0);
      lcd.print("RIDDLER ROBBING");
      lcd.setCursor(0,1);
      lcd.print("WOODINVILLE BANK");
      break;
    case 8:
      lcd.setCursor(0,0);
      lcd.print("TIDAL WAVE HEADS");
      lcd.setCursor(0,1);
      lcd.print("TOWARD HAWAII");
      break;
    case 9:
      lcd.setCursor(0,0);
      lcd.print("HUGE WHIRLPOOL");
      lcd.setCursor(0,1);
      lcd.print("IN PACIFIC OCEAN");
      break;
    case 10:
      lcd.setCursor(0,0);
      lcd.print("MONSTER CHICKEN");
      lcd.setCursor(0,1);
      lcd.print("ATTACKING KANSAS");
      break;
    case 11:
      lcd.setCursor(0,0);
      lcd.print("BIZARRO STEALING");
      lcd.setCursor(0,1);
      lcd.print("ALL TEDDY BEARS");
      break;
    default:
      lcd.setCursor(0,0);
      lcd.print("CAL POLY SLO");
      lcd.setCursor(0,1);
      lcd.print("MECH ENG 1991");
  }
  delay(60000);
}

