/**
 * Narcoleptic - A sleep library for Arduino
 * Copyright (C) 2010 Peter Knight (Cathedrow)
 *
*/

/*
** Welcome to Narcoleptic. This library allows you to dramatically reduce
** the power consumption of your Arduino project.
**
** Whilst in a Narcoleptic delay, the CPU current consumption drops from
** around 15mA down to about 7µA.
**
** Note that Narcoleptic only shuts down the CPU. It does not shut down
** anything else consuming current - LEDs, or the USB to serial chip.
** For low current applications, consider using boards without those,
** such as the Sparkfun Arduino Pro with the power LED removed.
**
** Narcoleptic is available from: http://narcoleptic.googlecode.com
*/

// This is a test program to see if the MCU can sleep until a PIR sensor
// detection makes it wake up.

#include <Narcoleptic.h>

void setup() {
  pinMode(4,INPUT);   // The PIR sensor
  pinMode(13,OUTPUT); // A simple LED
  digitalWrite(13,LOW);
}

void loop() {
  int a;

  Narcoleptic.delay(500); // During this time power consumption is minimised

  while (digitalRead(4) == HIGH) {
    digitalWrite(13,HIGH);
    delay(10000);
    digitalWrite(13,LOW);
    //delay(50);
  }
}
