#include <Nrf2401.h>

// Pin Connections
// DR1 -> 2 (digital pin 2)
// CD -> 3
// CS -> 4
// CLK -> 5
// DAT -> 6

Nrf2401 Radio;

// CODE FOR RECEIVER UNIT
// Listen for incoming messages targeting the device's local address
// of 3 byte length, whose contents equal 22, 33 and 44 respectively.
// If such a message is encountered, blink an LED on pin 13 for half a second.

void setup()
{
  pinMode(13,OUTPUT);
  Radio.localAddress = 1;
  Radio.rxMode(3);
}

void loop()
{
  while (!Radio.available());
  {
    Radio.read();
    if (Radio.data[0] == 22 && Radio.data[1] == 33 && Radio.data[2] == 44)
    {
      digitalWrite(13,HIGH);
      delay(500);
      digitalWrite(13,LOW);
    }
  }
}

