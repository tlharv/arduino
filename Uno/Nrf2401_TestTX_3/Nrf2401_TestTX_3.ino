#include <SPI.h>
#include "RF24.h"

/*
Pin   Label   Description               Uno Pin
---   -----   ------------------------  -------
 1    GND     Ground                      GND
 2    VCC     Voltage (must be 3.3VDC)    3V3
 3    CE      Chip Enable                 D09
 4    CSN     Chip Select                 D10
 5    SCK     ???                         D13
 6    MOSI    Master Out Slave In         D11
 7    MISO    Master In Slave Out         D12
 8    IRQ     ???                         --
*/

const int CE = 9;
const int CS = 10;

// Your chosen radio CE,CS pins
RF24 radio(CE,CS);

uint64_t addresses[] = {0xABABABABABLL, 0xC3C3C3C3C3LL};

/****** Configure this for sender or receiver *****/
boolean sender = 0; // Change this from 0 to 1 to switch between sending or receiving sketch


void setup() {
  
  Serial.begin(57600);
  radio.begin(); 
  
  if(sender == 0){
    radio.openWritingPipe(addresses[1]);
    radio.openReadingPipe(1,addresses[0]);
    radio.startListening();
  }else{
    radio.openWritingPipe(addresses[0]);    //*Edit to correct address assignments
    radio.openReadingPipe(1,addresses[1]);
    radio.stopListening(); 
  }
  
}

char myArray[] = "This is not a test";

void loop() {
    
  if(sender == 0){
      if(radio.available()){
        char tmpArray[19];                                               // This generally should be the same size as the sending array
        radio.read(&tmpArray,sizeof(tmpArray));  // Reading 19 bytes of payload (18 characters + NULL character)
        Serial.println(tmpArray);                                   // Prints only the received characters because the array is NULL terminated
        
      }
    
  }else{
  
      bool ok = radio.write(&myArray,sizeof(myArray));
  
      if(ok){ Serial.println("Transfer OK");
      }else { Serial.println("Transfer Fail"); 
      }
      delay(1000);
  }

}
