// Arduino pins used for the LEDs
#define LED1 13

// Arduino pins used for the switches
#define S1 5  // DIP switch 1
#define S2 6  // DIP switch 2
#define S3 7  // DIP switch 3


// State of each switch (0 or 1)
int s1state;
int s2state;
int s3state;

void setup() {
  // pins for LEDs are outputs
  pinMode(LED1, OUTPUT);

  // pins for switches are inputs
  pinMode(S1, INPUT);
  pinMode(S2, INPUT);
  pinMode(S3, INPUT);

  // setup serial port
  Serial.begin(9600);
  Serial.println("Serial port open");
}

void loop()
{
  // Check DIP switch setting and create code string
  s1state = digitalRead(S1);  // either a 1 or a zero
  s2state = digitalRead(S2);  // either a 1 or a zero
  s3state = digitalRead(S3);  // either a 1 or a zero
  String Code = "C";
  Code += s1state;
  Code += s2state;
  Code += s3state;
  Serial.println(Code);
  delay(1000);
  
  // Do something based on the DIP switch setting
  // Is there a switch statement based on string?
  
}


