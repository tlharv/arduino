int myDipPins[] = {2,3};
const int NumOfSwitches = 2;

void setup()
{
  Serial.begin(9600);
  for (int i = 0; i <= NumOfSwitches-1; i++)
  {
    pinMode(myDipPins[i], INPUT);      // Set DIP switch pins as inputs
    digitalWrite(myDipPins[i], HIGH);  // Set pullup resistor on DIP switch pins
    //Serial.print("myDipPins["); Serial.print(i); Serial.println("]");
  }
  delay(100);
}

void loop()
{
  byte DIPValue = readDIP(myDipPins, NumOfPins);
  Serial.println(DIPValue, BIN);
//  switch (DIPValue)
//  {
//    case B00000:
//      Serial.print("Match 000\n");
//      break;
//    case B00100:
//      Serial.print("Match 000\n");
//      break;
//    case B11011:
//      Serial.print("Match 000\n");
//      break;
//    case B11111:
//      Serial.print("Match 000\n");
//      break;
//    case B01001:
//      Serial.print("Match 000\n");
//      break;
//    case B10000:
//      Serial.print("Match 000\n");
//      break;
//  }
  delay(1000);
}

// Read binary from DIP switches
byte readDIP(int* dipPins, int numPins)
{
  byte j = 0;
  // Get the switches' state
  for (int i = 0; i < numPins; i++)
  {
    if (!digitalRead(dipPins[i]))
    {
      j += 1<<(numPins-1-i);
    }
  return j;
  }
}
