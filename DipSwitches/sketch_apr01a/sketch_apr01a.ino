// www.TinkerHobby.com
// Natalia Fargasch Norman
// LED control via DIP switches

// Arduino pins used for the switches
#define S1 7
#define S2 6
#define S3 5


// State of each switch (0 or 1)
int s1state;
int s2state;
int s3state;

void setup() {
  // pins for LEDs are outputs
  pinMode(LED1, OUTPUT);

  // pins for switches are inputs
  pinMode(S1, INPUT);
  pinMode(S2, INPUT);
  pinMode(S3, INPUT);

  // setup serial port
  Serial.begin(9600);
  Serial.println("Serial port open");
}

void loop() {
  s1state = digitalRead(S1);
  s2state = digitalRead(S2);
  s3state = digitalRead(S3);
  
  Serial.print(s1state);
  Serial.print(s2state);
  Serial.print(s3state);
  
  Serial.println();
  delay(1000);
}
