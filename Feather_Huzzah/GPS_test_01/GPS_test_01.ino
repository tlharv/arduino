/*
 *  Simple test sketch for the MCP9808 digital temp sensor
 *  It also includes connectivity to a wifi source
 *  Now adding GPS functionality
 */

// Libraries for using GPS
#include <Adafruit_GPS.h>  // So we can use GPS-specific functions

// Library for using software serial
// Note: we have to use software serial pins because the Ultimate GPS Featherwing
// and the Feather Huzzah ESP8266 have serial port conflicts.
#include<SoftwareSerial.h> // To use pins 12 & 13 for TX & RX, respectively

// Libraries for Feather Huzzah
#include <ESP8266WiFi.h>

// Libraries for MCP9808 temp sensor
#include <Wire.h>
#include "Adafruit_MCP9808.h"

// Wifi connection info
const char* ssid     = "Warp2";
const char* password = "z2xwSs6M";

// Create objects from library classes
SoftwareSerial mySerial(12,13); // object of software serial class
Adafruit_GPS GPS(&mySerial); // object of GPS class
Adafruit_MCP9808 TS = Adafruit_MCP9808(); // object of the temp sensor class

// Global variables
String NMEA1;  //We will use this variable to hold our first NMEA sentence
String NMEA2;  //We will use this variable to hold our second NMEA sentence
char c;       //Used to read the characters spewing from the GPS module
uint32_t timer = millis();
byte ASCII_degree = 248; // degree symbol


void ConnectToWiFi() {
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  
  WiFi.begin(ssid, password);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");  
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void readGPS()
{  
  //This function will read and remember two NMEA sentences from GPS
  clearGPS();    //Serial port probably has old or corrupt data, so begin by clearing it all out
  while(!GPS.newNMEAreceived()) { //Keep reading characters in this loop until a good NMEA sentence is received
    c=GPS.read(); //read a character from the GPS
  }

  GPS.parse(GPS.lastNMEA());  //Once you get a good NMEA, parse it
  NMEA1=GPS.lastNMEA();      //Once parsed, save NMEA sentence into NMEA1
  while(!GPS.newNMEAreceived()) {  //Go out and get the second NMEA sentence, should be different type than the first one read above.
    c=GPS.read();
  }
  GPS.parse(GPS.lastNMEA());
  NMEA2=GPS.lastNMEA();
  //Serial.println(NMEA1);
  //Serial.println(NMEA2);

  if (GPS.fix) {
    Serial.print("GPS antenna has fixed on "); Serial.print(GPS.satellites); Serial.println(" satellites.");
    
    // The GPS module provides latitude in degrees-minutes, but as DDMM.MMMM which won't work in Google Maps.
    // We need to modify this into a single decimal degrees latitude number (DD).
    // To do this, we need to parse out the DD and MM.MMMM, and convert MM.MMMM to decimal degrees and add to DD.
    
    float floatLatRaw = (GPS.latitude); // 4747.4517
    char LatDir = GPS.lat;
  
    float A = floatLatRaw / 100.0;  // should be 47.474517 now
    float B = A - int(A); // should be everything right of the decimal
    float C = (B*100.0) / 60.0;   // converting to decimal degrees
    float D = (A - B) + C; // rebuilding the number to have original degrees + decimal degrees
    // Add a +/- depending on North or South latitude -- South will be negative
    if (LatDir == 'S') {
      D = D * -1;
    }
    String DecLat = String(D,6);
  
    // Longitude is NOT decimal, but degrees-minutes (DDDMM.MMMM). Perform same operation as with Latitude
    float floatLonRaw = (GPS.longitude);
    char LonDir = GPS.lon;
  
    float E = floatLonRaw / 100.0;  // should be 47.474517 now
    float F = E - int(E); // should be everything right of the decimal
    float G = (F*100.0) / 60.0;   // converting to decimal degrees
    float H = (E - F) + G; // rebuilding the number to have original degrees + decimal degrees
    // Add a +/- depending on East or West longitude -- West will be negative
    if (LonDir == 'W') {
      H = H * -1;
    }
    String DecLon = String(H,6);
    //Serial.print("Decimal longitude = "); Serial.println(DecLon);
  
    //Serial.print("Your location is at "); Serial.print(D,6); Serial.print(","); Serial.println(H,6);
    Serial.print("Your location: ("); Serial.print(DecLat); Serial.print(","); Serial.print(DecLon); Serial.println(")");
    Serial.println("(you can cut and paste those coordinates into the search field of Google Maps)");
  
    // Other than GPS coords, we really only care about whether the speed of the car is zero.
    // Time and date will be captured in the email going out.

    float vehicleSpeed = (GPS.speed) * 5280.0 / 6080.0; // convert from knots to mph
    
    if (GPS.speed < 1) {
      Serial.println("Speed is essentially zero");
    }
    else {
      Serial.print("Speed (mph): "); Serial.println(vehicleSpeed);
    }
  
    Serial.println("");
  }
}

void clearGPS() {  //Since between GPS reads, we still have data streaming in, we need to clear the old data by reading a few sentences, and discarding these
  while(!GPS.newNMEAreceived()) {
    c=GPS.read();
  }
  GPS.parse(GPS.lastNMEA());
  while(!GPS.newNMEAreceived()) {
    c=GPS.read();
  }
  GPS.parse(GPS.lastNMEA());
}




void setup() {
  Serial.begin(115200);
  delay(100);

  // Find the temp sensor on I2C- it has a default I2C address of 0x18
  if (!TS.begin(0X18)) {
    Serial.println("Couldn't find MCP9808! Check your connections and verify the address is correct.");
    while (1);
  }
  Serial.println("Found MCP9808 at 0x18.");

  // Now set things up for the GPS
  GPS.begin(9600); // Turns the GPS on with a communication baud rate of 9600
  GPS.sendCommand("$PGCMD,33,0*6D"); // Turn Off GPS Antenna Update
  GPS.sendCommand(PMTK_SET_NMEA_OUTPUT_RMCGGA); //Tell GPS we want only $GPRMC and $GPGGA NMEA sentences
  GPS.sendCommand(PMTK_SET_NMEA_UPDATE_1HZ);   // 1 Hz update rate
  delay(1000); //pause for 1 sec
  Serial.println("GPS set up");
}


void loop() {
  //ConnectToWiFi(); // Self-explanatory
  
  // Wake up MCP9808
  Serial.println("Waking up temp sensor...");
  TS.wake();

  // Read and print the temperature
  float f = TS.readTempF();
  Serial.print("Temp: ");
  Serial.print(f, 2); Serial.println("*F.");
  delay(200);
  Serial.println("Put temp sensor to sleep to reduce power consumption...");
  TS.shutdown_wake(1); // shut down temp sensor; power consumption ~0.1 uA
  Serial.println("");

  // Read and print GPS
  readGPS();

  
  
  delay(10000); // Wait 10 seconds between loops
}
