/*
 * This is to set up a basic webserver on the Adafruit Feather Huzzah module
 * with attached CP9808 temp sensor.
 * December 18, 2021
 */


// Libraries for Feather Huzzah
#include <ESP8266WiFi.h>

// Libraries for MCP9808 temp sensor
#include <Wire.h>
#include "Adafruit_MCP9808.h"

// Libraries for running a webserver
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>

// Wifi connection info
const char* ssid     = "Warp2";
const char* password = "z2xwSs6M";

// Create the MCP9808 temp sensor object and call it TS
Adafruit_MCP9808 TS = Adafruit_MCP9808();


void setup() {
  Serial.begin(115200);
  delay(100);

  // We start by connecting to a WiFi network
  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  
  WiFi.begin(ssid, password);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");  
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  // Find the temp sensor on I2C- it has a default I2C address of 0x18
  if (!TS.begin(0X18)) {
    Serial.println("Couldn't find MCP9808! Check your connections and verify the address is correct.");
    while (1);
  }
  Serial.println("Found MCP9808 at 0x18.");
}


void loop() {
  // Wake up MCP9808
  Serial.println("Waking up temp sensor...");
  TS.wake();

  // Read and print the temperature
  float f = TS.readTempF();
  Serial.print("Temp: ");
  Serial.print(f, 2); Serial.println("*F.");
  delay(200);
  Serial.println("Put temp sensor to sleep to reduce power consumption...");
  TS.shutdown_wake(1); // shut down temp sensor; power consumption ~0.1 uA
  Serial.println("");
  delay(10000); // Wait 10 seconds between loops
}
