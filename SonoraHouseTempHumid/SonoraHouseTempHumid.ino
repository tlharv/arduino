/***************************************************
  Adafruit MQTT Library ESP8266 Example

  Must use ESP8266 Arduino from:
    https://github.com/esp8266/Arduino

  Works great with Adafruit's Huzzah ESP board & Feather
  ----> https://www.adafruit.com/product/2471
  ----> https://www.adafruit.com/products/2821

  Adafruit invests time and resources providing this open source code,
  please support Adafruit and open-source hardware by purchasing
  products from Adafruit!

  Written by Tony DiCola for Adafruit Industries.
  MIT license, all text above must be included in any redistribution
 ****************************************************/
#include <ESP8266WiFi.h>
#include "Adafruit_MQTT.h"
#include "Adafruit_MQTT_Client.h"
#include <DHT.h>
#define DHTTYPE DHT22
#define DHTPIN 2

// Initialize DHT sensor 
DHT dht(DHTPIN, DHTTYPE, 11);

// Define global variables
float humidity, temp_f;

/************************* WiFi Access Point *********************************/

//#define WLAN_SSID       "Harveyswifi"
//#define WLAN_PASS       "seascouts"
#define WLAN_SSID       "Home-7BE2"
#define WLAN_PASS       "DDD474809CF4B397"

/************************* Adafruit.io Setup *********************************/

#define AIO_SERVER      "io.adafruit.com"
#define AIO_SERVERPORT  1883                   // use 8883 for SSL
#define AIO_USERNAME    "tlharv"
#define AIO_KEY         "ee89a3a7fe1547a6890ed6e28a8562e2"

/************ Global State (you don't need to change this!) ******************/

// Create an ESP8266 WiFiClient class to connect to the MQTT server.
WiFiClient client;
// or... use WiFiFlientSecure for SSL
//WiFiClientSecure client;

// Store the MQTT server, username, and password in flash memory.
// This is required for using the Adafruit MQTT library.
const char MQTT_SERVER[] PROGMEM    = AIO_SERVER;
const char MQTT_USERNAME[] PROGMEM  = AIO_USERNAME;
const char MQTT_PASSWORD[] PROGMEM  = AIO_KEY;

// Setup the MQTT client class by passing in the WiFi client and MQTT server and login details.
Adafruit_MQTT_Client mqtt(&client, MQTT_SERVER, AIO_SERVERPORT, MQTT_USERNAME, MQTT_PASSWORD);

/****************************** Feeds ***************************************/

// Setup a feed called 'humidity' for publishing.
// Notice MQTT paths for AIO follow the form: <username>/feeds/<feedname>
const char RELHUMIDITY_FEED[] PROGMEM = AIO_USERNAME "/feeds/SonoraHumid";
Adafruit_MQTT_Publish RelHumidity = Adafruit_MQTT_Publish(&mqtt, RELHUMIDITY_FEED);

const char LOCALTEMPF_FEED[] PROGMEM = AIO_USERNAME "/feeds/SonoraTemp";
Adafruit_MQTT_Publish LocalTempF = Adafruit_MQTT_Publish(&mqtt, LOCALTEMPF_FEED);

// Setup a feed called 'onoff' for subscribing to changes.
const char ONOFF_FEED[] PROGMEM = AIO_USERNAME "/feeds/onoff";
Adafruit_MQTT_Subscribe onoffbutton = Adafruit_MQTT_Subscribe(&mqtt, ONOFF_FEED);

/*************************** Sketch Code ************************************/

// Bug workaround for Arduino 1.6.6, it seems to need a function declaration
// for some reason (only affects ESP8266, likely an arduino-builder bug).
void MQTT_connect();

void setup() {
  Serial.begin(115200);
  delay(10);

  //Serial.println(F("Adafruit MQTT demo"));

  // Connect to WiFi access point.
  //Serial.println(); Serial.println();
  //Serial.print("Connecting to ");
  //Serial.println(WLAN_SSID);

  WiFi.begin(WLAN_SSID, WLAN_PASS);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    //Serial.print(".");
  }
  //Serial.println();

  //Serial.println("WiFi connected");
  //Serial.println("IP address: "); 
  //Serial.println(WiFi.localIP());

  // Setup MQTT subscription for onoff feed.
  mqtt.subscribe(&onoffbutton);
}

uint32_t x=0;

void loop() {
  // Ensure the connection to the MQTT server is alive (this will make the first
  // connection and automatically reconnect when disconnected).  See the MQTT_connect
  // function definition further below.
  MQTT_connect();

  // this is our 'wait for incoming subscription packets' busy subloop
  // try to spend your time here

  Adafruit_MQTT_Subscribe *subscription;
  while ((subscription = mqtt.readSubscription(5000))) {
    if (subscription == &onoffbutton) {
      //Serial.print(F("Got: "));
      //Serial.println((char *)onoffbutton.lastread);
    }
  }

  // Grab the latest temperature and humidity values
  gettemperature(); // read sensor

  // Now we can publish stuff!
  //Serial.print(F("\nSending humidity val "));
  //Serial.print(humidity);
  //Serial.print("...");
  if (! RelHumidity.publish(humidity)) {
    //Serial.println(F("Failed"));
  } else {
    //Serial.println(F("OK!"));
  }
  //Serial.print(F("\nSending Temp val "));
  //Serial.print(temp_f);
  //Serial.print("...");
  if (! LocalTempF.publish(temp_f)) {
    //Serial.println(F("Failed"));
  } else {
    //Serial.println(F("OK!"));
  }

  delay(120000); // take a reading and upload every minute
  
  // ping the server to keep the mqtt connection alive
  // NOT required if you are publishing once every KEEPALIVE seconds
  
  if(! mqtt.ping()) {
    mqtt.disconnect();
  }
  
}

// Function to connect and reconnect as necessary to the MQTT server.
// Should be called in the loop function and it will take care if connecting.
void MQTT_connect() {
  int8_t ret;

  // Stop if already connected.
  if (mqtt.connected()) {
    return;
  }

  //Serial.print("Connecting to MQTT... ");

  uint8_t retries = 3;
  while ((ret = mqtt.connect()) != 0) { // connect will return 0 for connected
       //Serial.println(mqtt.connectErrorString(ret));
       //Serial.println("Retrying MQTT connection in 5 seconds...");
       mqtt.disconnect();
       delay(5000);  // wait 5 seconds
       retries--;
       if (retries == 0) {
         // basically die and wait for WDT to reset me
         while (1);
       }
  }
  //Serial.println("MQTT Connected!");
}

void gettemperature() {
  // Wait at least 2 seconds seconds between measurements.
  // if the difference between the current time and last time you read
  // the sensor is bigger than the interval you set, read the sensor
  // Works better than delay for things happening elsewhere also
  // unsigned long currentMillis = millis();
 
  //if(currentMillis - previousMillis >= interval) {
    // save the last time you read the sensor 
    //previousMillis = currentMillis;   
 
    // Reading temperature for humidity takes about 250 milliseconds!
    // Sensor readings may also be up to 2 seconds 'old' (it's a very slow sensor)
    humidity = dht.readHumidity();          // Read humidity (percent)
    temp_f = dht.readTemperature(true);     // Read temperature as Fahrenheit
    // Check if any reads failed and exit early (to try again).
    if (isnan(humidity) || isnan(temp_f)) {
      //Serial.println("Failed to read from DHT sensor!");
      return;
    }
  }
