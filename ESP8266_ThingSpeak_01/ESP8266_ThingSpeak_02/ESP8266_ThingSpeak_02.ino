#include "ThingSpeak.h"

/// ***********************************************************************************************************
// This example selects the correct library to use based on the board selected under the Tools menu in the IDE.
// Yun, Wired Ethernet shield, wi-fi shield, esp8266, and Spark are all supported.
// With Uno and Mega, the default is that you're using a wired ethernet shield (http://www.arduino.cc/en/Main/ArduinoEthernetShield)
// If you're using a wi-fi shield (http://www.arduino.cc/en/Main/ArduinoWiFiShield), uncomment the line below
// ***********************************************************************************************************
#include <ESP8266WiFi.h>
char ssid[] = "Warp2";          //  your network SSID (name) 
char pass[] = "Join9]Temperature>";   // your network password
int status = WL_IDLE_STATUS;
WiFiClient  client;



/*
  This is the ThingSpeak channel number for the MathwWorks weather station
  https://thingspeak.com/channels/12397.  It senses a number of things and puts them in the eight
  field of the channel:
  Field 1 - Wind Direction (degrees where 0 is North)
  Field 2 - Wind Speed (MPH)
  Field 3 - Humidity (%RH)
  Field 4 - Temperature (Degrees F)
  Field 5 - Rainfall (inches since last measurement)
  Field 6 - Atmospheric Pressure (inHg)
*/

unsigned long weatherStationChannelNumber = 104095;

void setup() {
  WiFi.begin(ssid, pass);
  ThingSpeak.begin(client);
}

void loop() {
  float windDirection = ThingSpeak.readFloatField(weatherStationChannelNumber,1);
  float windSpeed = ThingSpeak.readFloatField(weatherStationChannelNumber,2);
  float humidity = ThingSpeak.readFloatField(weatherStationChannelNumber,3);
  float temperature = ThingSpeak.readFloatField(weatherStationChannelNumber,4);
  float rainfall = ThingSpeak.readFloatField(weatherStationChannelNumber,5);
  float pressure = ThingSpeak.readFloatField(weatherStationChannelNumber,6);

  #if defined(ARDUINO_ARCH_AVR) || defined(ARDUINO_ARCH_ESP8266)
    Serial.println("======================================"); 
    Serial.println("Current weather conditions in Natick: "); 
    Serial.print(temperature);
    Serial.print(" degrees F, "); 
    Serial.print(humidity);
    Serial.println("% humidity"); 
    Serial.print("Wind at ");
    Serial.print(windSpeed);
    Serial.print(" MPH at "); 
    Serial.print(windDirection);
    Serial.println(" degrees"); 
    Serial.print("Pressure is ");
    Serial.print(pressure);
    Serial.print(" inHg");
    if(rainfall > 0)
    {
    	Serial.print(", and it's raining"); 
    }
    Serial.println(); 
  #endif
  #ifdef SPARK
    Particle.publish("thingspeak-weather", "Current weather conditions in Natick: ",60,PRIVATE);
    Particle.publish("thingspeak-weather", String(temperature) + " degrees F, " + String(humidity) + "% humidity",60,PRIVATE); 
    Particle.publish("thingspeak-weather", "Wind at " + String(windSpeed) + " MPH at " + String (windDirection) + " degrees",60,PRIVATE); 
    if(rainfall > 0)
    {
      Particle.publish("thingspeak-weather", "Pressure is " + String(pressure) + " inHg, and it's raining",60,PRIVATE);
    }
    else
    {
      Particle.publish("thingspeak-weather", "Pressure is " + String(pressure) + " inHg",60,PRIVATE);
    }
  #endif

  delay(60000); // Note that the weather station only updates once a minute

}
