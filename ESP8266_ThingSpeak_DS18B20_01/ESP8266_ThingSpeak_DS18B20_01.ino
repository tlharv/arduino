//#include <DHT.h>
#include <ESP8266WiFi.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#define ONE_WIRE_BUS 2

OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);

float temp_c, temp_f;

const int OneSec = 1000;
const int OneMin = 60 * OneSec;

String apiKey = "8LE8Z0FKUMKIJRXA";  // For channel 146476, InEchoHouse
const char* ssid = "Warp2";
const char* password = "z2xwSs6M";

const char* server = "api.thingspeak.com";
//#define DHTPIN 2

//DHT dht (DHTPIN, DHT22,11);
WiFiClient client;

const int sleepTimeS = 120; // in seconds

void setup() {
  //Serial.begin(115,200);
  //delay(10);
  //dht.begin();
  sensors.begin();
  WiFi.begin(ssid, password);
}

void loop() {
  
  //float h = dht.readHumidity();
  //float t = dht.readTemperature();
  //float tF = (9*t/5) + 32.0;

  sensors.requestTemperatures();
  temp_f = sensors.getTempFByIndex(0);

//  if (isnan(h) || isnan(t)) {
//    return;
//    }

  if (client.connect(server, 80)) {
    String postStr = apiKey;
    postStr += "&field1=";
    postStr += String(temp_f);
    //postStr += "&field2=";
    //postStr += String(h);
    postStr += "\r\n\r\n";

    client.print ("POST /update HTTP/1.1\n");
    client.print ("Host: api.thingspeak.com\n");
    client.print ("Connection: close\n");
    client.print ("X-THINGSPEAKAPIKEY: "+apiKey+"\n");
    client.print("Content-Type: application/x-www-form-urlencoded\n");
    client.print("Content-Length: ");
    client.print(postStr.length());
    client.print("\n\n");
    client.print(postStr);
  }
  
  client.stop();
  delay(2 * OneMin); // ThingSpeak needs minimum 15 sec delay between updates

  // Go to sleep
  //ESP.deepSleep(sleepTimeS * 1000000);
  
}
