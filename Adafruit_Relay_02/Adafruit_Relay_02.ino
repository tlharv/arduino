/* 
  This sketch uses an Adafruit Huzzah, three Adafruit.IO feeds and multiple Neopixels to paint
  different aspects of a plexiglass flower that you can color using the color dashboard tool
  on Adafruit.IO.  Each feed will set the color of one or more Neopixels that will illuminate 
  the flower center, petals, and leaves.

  The sketch used as a starting point the Smart Toilet Light with ESP8255 sketch by Toni DiCola.
  License: MIT (https://opensource.org/licenses/MIT)

  Where the Smart Toilet Light sketch uses one feed to set one Neopixel color, this sketch assumes
  a series of eight Neopixels and multiple feeds to target key Neopixels with certain colors.
  
*/

#include <ESP8266WiFi.h>
#include "Adafruit_MQTT.h"
#include "Adafruit_MQTT_Client.h"
//#include "Adafruit_NeoPixel.h"


// Configuration that you _must_ fill in:
#define WLAN_SSID       "Warp2"    // Your WiFi AP name.
#define WLAN_PASS       "Join9]Temperature>"     // Your WiFi AP password.
#define AIO_USERNAME    "tlharv" // Adafruit IO username (see http://accounts.adafruit.com).
#define AIO_KEY         "ee89a3a7fe1547a6890ed6e28a8562e2"      // Adafruit IO key


// Relay pin
#define RELAY_PIN 1
#define RELAY_FEED "Relay"

#define AIO_SERVER      "io.adafruit.com"      // Adafruit IO server address.
#define AIO_SERVERPORT  1883                   // AIO server port.
#define PING_SEC        60                     // How many seconds to wait between MQTT pings.
                                               // Used to help keep the connection alive during
                                               // long periods of inactivity.

// Global state (you don't need to change this):
// Put strings in flash memory (required for MQTT library).
const char MQTT_SERVER[] PROGMEM    = AIO_SERVER;
const char MQTT_USERNAME[] PROGMEM  = AIO_USERNAME;
const char MQTT_PASSWORD[] PROGMEM  = AIO_KEY;

const char MQTT_PATH_RELAY[] PROGMEM    = AIO_USERNAME "/feeds/" RELAY_FEED;

// Create ESP8266 wifi client, MQTT client, and feed subscriptions for Center, Petal and Leaf feeds
WiFiClient client;
Adafruit_MQTT_Client mqtt(&client, MQTT_SERVER, AIO_SERVERPORT, MQTT_USERNAME, MQTT_PASSWORD);
//Adafruit_MQTT_Subscribe lightFeed_C = Adafruit_MQTT_Subscribe(&mqtt, MQTT_PATH_C);
//Adafruit_MQTT_Subscribe lightFeed_P = Adafruit_MQTT_Subscribe(&mqtt, MQTT_PATH_P);
//Adafruit_MQTT_Subscribe lightFeed_L = Adafruit_MQTT_Subscribe(&mqtt, MQTT_PATH_L);
Adafruit_MQTT_Subscribe RelayFeed = Adafruit_MQTT_Subscribe(&mqtt, MQTT_PATH_RELAY);


// Create NeoPixel object
//Adafruit_NeoPixel strip = Adafruit_NeoPixel(PIXEL_COUNT, PIXEL_PIN, PIXEL_TYPE);

// Other global variables:
uint32_t nextPing = 0;       // Next time a MQTT ping should be sent.
int pulsePeriodMS = 0;       // Period of time (in MS) for the pulse animation.
//int C[3]; // array to store the three integer RGB values for the flower center color
//int P[3]; // array to store the three integer RGB values for the flower petal color
//int L[3]; // array to store the three integer RGB values for the flower leaf color
int R_State; // variable to store the relay state

// -------  MQTT_connect, parseHexByte are unchanged from Toilet Light sketch ---------
// Explicit declaration of MQTT_connect function defined further below.
// Necessary because of bug/issue with recent Arduino builder & ESP8266.
void MQTT_connect();

// Function to parse a hex byte value from a string.  No different from the toilet light sketch.
// The passed in string MUST be at least 2 characters long!
// If the value can't be parsed then -1 is returned, otherwise the
// byte value is returned.
int parseHexByte(char* data) {
  char high = tolower(data[0]);
  char low = tolower(data[1]);
  uint8_t result = 0;
  // Parse the high nibble.
  if ((high >= '0') && (high <= '9')) {
    result += 16*(high-'0');
  }
  else if ((high >= 'a') && (high <= 'f')) {
    result += 16*(10+(high-'a'));
  }
  else {
    // Couldn't parse the high nibble.
    return -1;
  }
  // Parse the low nibble.
  if ((low >= '0') && (low <= '9')) {
    result += low-'0';
  }
  else if ((low >= 'a') && (low <= 'f')) {
    result += 10+(low-'a');
  }
  else {
    // Couldn't parse the low nibble.
    return -1;
  }
  return result;
}



void setup() {
  // Initialize serial output.
  Serial.begin(115200);
  delay(10);

  // Initialize NeoPixels and turn them off.
  //strip.begin();
  //lightPixels_ALL((0,0,0));
  //delay(500);

  // Connect to WiFi access point.
  Serial.println(); Serial.println();
  Serial.print("Connecting to ");
  Serial.println(WLAN_SSID);
  WiFi.begin(WLAN_SSID, WLAN_PASS);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println();
  Serial.println("WiFi connected");
  Serial.println("IP address: "); Serial.println(WiFi.localIP());

  // Setup MQTT subscription.
  //mqtt.subscribe(&lightFeed);
  //mqtt.subscribe(&lightFeed_C);
  //mqtt.subscribe(&lightFeed_P);
  //mqtt.subscribe(&lightFeed_L);
  mqtt.subscribe(&RelayFeed);
}







void loop() {
  
  // Ensure the connection to the MQTT server is alive (this will make the first
  // connection and automatically reconnect when disconnected).  See the MQTT_connect
  // function definition further below.
  MQTT_connect();

  // Check if any new data has been received from the light feed.
  Adafruit_MQTT_Subscribe *subscription;
  while ((subscription = mqtt.readSubscription(1000))) {
    if (subscription == &RelayFeed) {
      // Received data from the relay feed!
      // Parse the data to see how to change the relay.
      char* data = (char*)RelayFeed.lastread;
      int dataLen = strlen(data);
      Serial.print("Got lightFeed_C = ");  Serial.println(data);
      if (dataLen < 1) {
        // Stop processing if not enough data was received.
        continue;
      }
    } // end subscription RelayFeed if


    // Now trigger the relay!
    //PaintFlower(C, P, L);  // IS THIS THE BEST LOCATION TO PLACE THIS FUNCTION?
    // ActivateRelay(data);   // data should be a 0 or 1
    
    break;  // kicks us out of the While loop
  } // end while

  // Ping the MQTT server periodically to prevent the connection from being closed.
  if (millis() >= nextPing) {
    // Attempt to send a ping.
    if(! mqtt.ping()) {
      // Disconnect if the ping failed.  Next loop iteration a reconnect will be attempted.
      mqtt.disconnect();
    }
    // Set the time for the next ping.
    nextPing = millis() + PING_SEC*1000L;
  } // end ping if
} // end loop()

// Function to connect and reconnect as necessary to the MQTT server.
// Should be called in the loop function and it will take care of connecting.
void MQTT_connect() {
  int8_t ret;

  // Stop if already connected.
  if (mqtt.connected()) {
    return;
  }

  Serial.print("Connecting to MQTT... ");

  uint8_t retries = 3;
  while ((ret = mqtt.connect()) != 0) { // connect will return 0 for connected
       Serial.println(mqtt.connectErrorString(ret));
       Serial.println("Retrying MQTT connection in 5 seconds...");
       mqtt.disconnect();
       delay(5000);  // wait 5 seconds
       retries--;
       if (retries == 0) {
         // basically die and wait for WDT to reset me
         while (1);
       }
  }
  Serial.println("MQTT Connected!");
}

