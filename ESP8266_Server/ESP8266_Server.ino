/*********
  Rui Santos
  Complete project details at http://randomnerdtutorials.com  
  http://randomnerdtutorials.com/esp8266-web-server-with-arduino-ide/

        +-------------------------+
     TX |  O  O  GND              |
  CH_PD |  O  O  GPIO-2           |
  RESET |  O  O  GPIO-0           |
    VCC |  O  O  RX               |
        +-------------------------+

*********/

#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>

MDNSResponder mdns;

// Replace with your network credentials
const char* ssid = "Warp2";
const char* password = "z2xwSs6M";

String WebServerHeader = "Ranch Security";
String Socket1Name = "Shop light: ";
String Socket2Name = "Coop light: ";
String Socket1Value = "  currently OFF";
String Socket2Value = "  currently OFF";
ESP8266WebServer server(80);

String webPage = "";

int gpio0_pin = 0;
int gpio2_pin = 2;

void setup(void){
  webPage += "<h1>" + WebServerHeader + "</h1>";
  webPage += "<p>" + Socket1Name + "<a href=\"socket1On\"><button>ON</button></a>&nbsp;<a href=\"socket1Off\"><button>OFF</button></a><label for='lblSoc1'>" + Socket1Value + "</label></p>";
  webPage += "<p>" + Socket2Name + "<a href=\"socket2On\"><button>ON</button></a>&nbsp;<a href=\"socket2Off\"><button>OFF</button></a><label for='lblSoc2'>" + Socket2Value + "</label></p>";
  
  // preparing GPIOs
  pinMode(gpio0_pin, OUTPUT);
  digitalWrite(gpio0_pin, LOW);
  pinMode(gpio2_pin, OUTPUT);
  digitalWrite(gpio2_pin, LOW);
  
  delay(1000);
  Serial.begin(115200);
  WiFi.begin(ssid, password);

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  
  if (mdns.begin("esp8266", WiFi.localIP())) {
    // Serial.println("MDNS responder started");
  }
  
  server.on("/", [](){
    server.send(200, "text/html", webPage);
  });
  server.on("/socket1On", [](){
    Socket1Value = "  currently ON";
    webPage = "<h1>" + WebServerHeader + "</h1>";
    webPage += "<p>" + Socket1Name + "<a href=\"socket1On\"><button>ON</button></a>&nbsp;<a href=\"socket1Off\"><button>OFF</button></a><label for='lblSoc1'>" + Socket1Value + "</label></p>";
    webPage += "<p>" + Socket2Name + "<a href=\"socket2On\"><button>ON</button></a>&nbsp;<a href=\"socket2Off\"><button>OFF</button></a><label for='lblSoc2'>" + Socket2Value + "</label></p>";
    server.send(200, "text/html", webPage);
    digitalWrite(gpio0_pin, HIGH);
    delay(1000);
  });
  server.on("/socket1Off", [](){
    Socket1Value = "  currently OFF";
    webPage = "<h1>" + WebServerHeader + "</h1>";
    webPage += "<p>" + Socket1Name + "<a href=\"socket1On\"><button>ON</button></a>&nbsp;<a href=\"socket1Off\"><button>OFF</button></a><label for='lblSoc1'>" + Socket1Value + "</label></p>";
    webPage += "<p>" + Socket2Name + "<a href=\"socket2On\"><button>ON</button></a>&nbsp;<a href=\"socket2Off\"><button>OFF</button></a><label for='lblSoc2'>" + Socket2Value + "</label></p>";
    server.send(200, "text/html", webPage);
    digitalWrite(gpio0_pin, LOW);
    delay(1000); 
  });
  server.on("/socket2On", [](){
    Socket2Value = "  currently ON";
    webPage = "<h1>" + WebServerHeader + "</h1>";
    webPage += "<p>" + Socket1Name + "<a href=\"socket1On\"><button>ON</button></a>&nbsp;<a href=\"socket1Off\"><button>OFF</button></a><label for='lblSoc1'>" + Socket1Value + "</label></p>";
    webPage += "<p>" + Socket2Name + "<a href=\"socket2On\"><button>ON</button></a>&nbsp;<a href=\"socket2Off\"><button>OFF</button></a><label for='lblSoc2'>" + Socket2Value + "</label></p>";
    server.send(200, "text/html", webPage);
    digitalWrite(gpio2_pin, HIGH);
    delay(1000);
  });
  server.on("/socket2Off", [](){
    Socket2Value = "  currently OFF";
    webPage = "<h1>" + WebServerHeader + "</h1>";
    webPage += "<p>" + Socket1Name + "<a href=\"socket1On\"><button>ON</button></a>&nbsp;<a href=\"socket1Off\"><button>OFF</button></a><label for='lblSoc1'>" + Socket1Value + "</label></p>";
    webPage += "<p>" + Socket2Name + "<a href=\"socket2On\"><button>ON</button></a>&nbsp;<a href=\"socket2Off\"><button>OFF</button></a><label for='lblSoc2'>" + Socket2Value + "</label></p>";
    server.send(200, "text/html", webPage);
    digitalWrite(gpio2_pin, LOW);
    delay(1000); 
  });
  server.begin();
  // Serial.println("HTTP server started");
}
 
void loop(void){
  server.handleClient();
} 
