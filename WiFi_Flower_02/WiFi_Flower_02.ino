// Smart Toilet Light with ESP8266
//
// Use a feed in Adafruit IO to control the color and animation of a neopixel.

#include <ESP8266WiFi.h>
#include "Adafruit_MQTT.h"
#include "Adafruit_MQTT_Client.h"
#include "Adafruit_NeoPixel.h"


// Configuration that you _must_ fill in:
#define WLAN_SSID       "Warp2"    // Your WiFi AP name.
#define WLAN_PASS       "2e8e6tcvhh"     // Your WiFi AP password.
#define AIO_USERNAME    "tlharv" // Adafruit IO username (see http://accounts.adafruit.com).
#define AIO_KEY         "691fd8103744453c991bd315d23c89e2"      // Adafruit IO key

// Configuration you can optionally change (but probably want to keep the same):
#define PIXEL_PIN       4                      // Pin connected to the NeoPixel data input.
#define PIXEL_COUNT     7                      // Number of NeoPixels.
#define PIXEL_TYPE      NEO_GRB + NEO_KHZ800   // Type of the NeoPixels (see strandtest example).
#define LIGHT_FEED      "Flower-lights"        // Name of the feed in Adafruit IO to listen for colors.
#define AIO_SERVER      "io.adafruit.com"      // Adafruit IO server address.
#define AIO_SERVERPORT  1883                   // AIO server port.
#define PING_SEC        60                     // How many seconds to wait between MQTT pings.
                                               // Used to help keep the connection alive during
                                               // long periods of inactivity.

// Global state (you don't need to change this):
// Put strings in flash memory (required for MQTT library).
const char MQTT_SERVER[] PROGMEM    = AIO_SERVER;
const char MQTT_USERNAME[] PROGMEM  = AIO_USERNAME;
const char MQTT_PASSWORD[] PROGMEM  = AIO_KEY;
const char MQTT_PATH[] PROGMEM      = AIO_USERNAME "/feeds/" LIGHT_FEED;

// Create ESP8266 wifi client, MQTT client, and feed subscription.
WiFiClient client;
Adafruit_MQTT_Client mqtt(&client, MQTT_SERVER, AIO_SERVERPORT, MQTT_USERNAME, MQTT_PASSWORD);
Adafruit_MQTT_Subscribe lightFeed = Adafruit_MQTT_Subscribe(&mqtt, MQTT_PATH);

// Create NeoPixel object
Adafruit_NeoPixel strip = Adafruit_NeoPixel(PIXEL_COUNT, PIXEL_PIN, PIXEL_TYPE);

// Other global state:
uint32_t nextPing = 0;       // Next time a MQTT ping should be sent.
int red = 0;                 // RGB color for the current animation.
int green = 0;
int blue = 0;
int animation = 0;           // Current animation (0 = none, 1 = pulse, 2 = rainbow cycle).
int pulsePeriodMS = 0;       // Period of time (in MS) for the pulse animation.


// Explicit declaration of MQTT_connect function defined further below.
// Necessary because of bug/issue with recent Arduino builder & ESP8266.
void MQTT_connect();

// Function to parse a hex byte value from a string.
// The passed in string MUST be at least 2 characters long!
// If the value can't be parsed then -1 is returned, otherwise the
// byte value is returned.
int parseHexByte(char* data) {
  char high = tolower(data[0]);
  char low = tolower(data[1]);
  uint8_t result = 0;
  // Parse the high nibble.
  if ((high >= '0') && (high <= '9')) {
    result += 16*(high-'0');
  }
  else if ((high >= 'a') && (high <= 'f')) {
    result += 16*(10+(high-'a'));
  }
  else {
    // Couldn't parse the high nibble.
    return -1;
  }
  // Parse the low nibble.
  if ((low >= '0') && (low <= '9')) {
    result += low-'0';
  }
  else if ((low >= 'a') && (low <= 'f')) {
    result += 10+(low-'a');
  }
  else {
    // Couldn't parse the low nibble.
    return -1;
  }
  return result;
}


void setup() {
  // Initialize serial output.
  Serial.begin(115200);
  delay(10);
  Serial.println("Smart flower Light with ESP8266");

  // Initialize NeoPixels and turn them off.
  strip.begin();
  lightPixels(strip.Color(0, 0, 0));

  // Connect to WiFi access point.
  Serial.println(); Serial.println();
  Serial.print("Connecting to ");
  Serial.println(WLAN_SSID);
  WiFi.begin(WLAN_SSID, WLAN_PASS);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println();
  Serial.println("WiFi connected");
  Serial.println("IP address: "); Serial.println(WiFi.localIP());

  // Setup MQTT subscription.
  mqtt.subscribe(&lightFeed);
}

// Function to set all the NeoPixels to the specified color.
void lightPixels(uint32_t color) {
  for (int i=0; i<PIXEL_COUNT; ++i) {
    strip.setPixelColor(i, color);
  }
  strip.show();
}

void loop() {
  
  // Ensure the connection to the MQTT server is alive (this will make the first
  // connection and automatically reconnect when disconnected).  See the MQTT_connect
  // function definition further below.
  MQTT_connect();

  // Check if any new data has been received from the light feed.
  Adafruit_MQTT_Subscribe *subscription;
  while ((subscription = mqtt.readSubscription(10))) {
    if (subscription == &lightFeed) {
      // Received data from the light feed!
      // Parse the data to see how to change the light.
      char* data = (char*)lightFeed.lastread;
      int dataLen = strlen(data);
      Serial.print("Got: ");
      Serial.println(data);
      if (dataLen < 1) {
        // Stop processing if not enough data was received.
        continue;
      }

      // Solid color.
      // Expect 6 more characters with the hex red, green, blue color.
      if (dataLen >= 7) {
        // Parse out the RGB color bytes.
        int r = parseHexByte(&data[1]);
        int g = parseHexByte(&data[3]);
        int b = parseHexByte(&data[5]);
        //Serial.print("Red = "); Serial.println(r);
        //Serial.print("Green = "); Serial.println(g);
        //Serial.print("Blue = "); Serial.println(b);
        if ((r < 0) || (g < 0) || (b < 0)) {
          // Couldn't parse the color, stop processing.
          Serial.println("Could not parse the color, so stop processing.");
          break; 
        }
      
        // Light the pixels!
        Serial.print("Lighting the pixels to ("); 
        Serial.print(r); Serial.print(","); 
        Serial.print(g); Serial.print(","); 
        Serial.print(b); Serial.println(")");
        lightPixels(strip.Color(0, 0, 0));
                
        //for (int i=0; i<PIXEL_COUNT; i++) {
        //  strip.setPixelColor(i,(0,0,0));
        //}
        //strip.show();
        //Serial.println("Colors cleared.");
        delay(500);
        lightPixels(strip.Color(r,g,b));
        Serial.println("New color applied.");

        
        // Change the animation to none/stop animating.
        //animation = 0;
      }
      break;

    }
  }

  // Ping the MQTT server periodically to prevent the connection from being closed.
  if (millis() >= nextPing) {
    // Attempt to send a ping.
    if(! mqtt.ping()) {
      // Disconnect if the ping failed.  Next loop iteration a reconnect will be attempted.
      mqtt.disconnect();
    }
    // Set the time for the next ping.
    nextPing = millis() + PING_SEC*1000L;
  }
}

// Function to connect and reconnect as necessary to the MQTT server.
// Should be called in the loop function and it will take care of connecting.
void MQTT_connect() {
  int8_t ret;

  // Stop if already connected.
  if (mqtt.connected()) {
    return;
  }

  Serial.print("Connecting to MQTT... ");

  uint8_t retries = 3;
  while ((ret = mqtt.connect()) != 0) { // connect will return 0 for connected
       Serial.println(mqtt.connectErrorString(ret));
       Serial.println("Retrying MQTT connection in 5 seconds...");
       mqtt.disconnect();
       delay(5000);  // wait 5 seconds
       retries--;
       if (retries == 0) {
         // basically die and wait for WDT to reset me
         while (1);
       }
  }
  Serial.println("MQTT Connected!");
}

