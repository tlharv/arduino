const int powerPin = 2;
const int pingPin = 3;
const int echoPin = 4;
const int dangerledPin = 5;


void setup()
{
  pinMode (powerPin, OUTPUT);
  pinMode (dangerledPin, OUTPUT);
  pinMode (pingPin, OUTPUT);
  pinMode (echoPin, INPUT);
  Serial.begin(9600);
}
 
void loop()
{
  //raw duration in milliseconds, cm is the
  //converted amount into a distance
  long duration, cm;
  digitalWrite(powerPin, HIGH);

  //sending the signal, starting with LOW for a clean signal
  digitalWrite(pingPin, LOW);
  delayMicroseconds(2);
  digitalWrite(pingPin, HIGH);
  delayMicroseconds(5);
  digitalWrite(pingPin, LOW);
 
  //setting up the input pin, and receiving the duration in
  //microseconds for the sound to bounce off the object infront
  duration = pulseIn(echoPin, HIGH);
 
  // convert the time into a distance
  cm = microsecondsToCentimeters(duration);
 
  int safeZone = 100;
  if (cm > safeZone)
  {
    digitalWrite(dangerledPin, LOW);
  }
  else
  {
    digitalWrite(dangerledPin, HIGH);
  }
 
  delay(100);
}
 
long microsecondsToCentimeters(long microseconds)
{
  // The speed of sound is 340 m/s or 29 microseconds per centimeter.
  // The ping travels out and back, so to find the distance of the
  // object we take half of the distance travelled.
  return microseconds / 29 / 2;
}
