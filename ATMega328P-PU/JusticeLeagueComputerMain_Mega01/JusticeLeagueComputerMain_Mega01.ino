/* This is the main processor program for the justice league computer.
It is written to be put onto an ATMega328P-PU

                              +-\/-+
                      RESET  1|    |28  (A 5)
                       RX 0  2|    |27  (A 4)
                       TX 1  3|    |26  (A 3)
                      (D 2)  4|    |25  (A 2)
                      (D 3)  5|    |24  (A 1)
                      (D 4)  6|    |23  (A 0)
                        VCC  7|    |22  GND
                        GND  8|    |21  AREF
                             9|    |20  VCC
                            10|    |19  (D13)
                      (D 5) 11|    |18  (D12)
                      (D 6) 12|    |17  (D11)
                      (D 7) 13|    |16  (D10)
                      (D 8) 14|    |15  (D 9)
                           +----+
                           
PWM = Pulse Width Modulation (good for analog out signals like glowing LEDs

D0: used for big green LED
D1: used for big red LED
D2: used for LCD
D3: used for LCD
D4: used for LCD
D5: used for LCD
D6: open
D7: open
D8: open
D9: used for LCD
D10: used for LCD
*/

#include <LiquidCrystal.h>

// Constants for the number of rows and columns in the LCD
const int numRows = 2;
const int numCols = 16;
const int greenLED = 7;  // D7, or physical pin 13
const int redLED = 8; // D8, or physical pin 14

// initialize the library with the numbers of the interface pins
LiquidCrystal lcd(10, 9, 5, 4, 3, 2);

/* This is the pin arrangement needed, going from pin 1 to 16 on the LCD
Pin 1 (LED-) goes to Uno GND, also to GND pin of rotary pot to adjust LCD dimmer
Pin 2 (LED+) goes to Uno 5V, also to 5V pin of rotary pot to adjust LCD dimmer
Pin 3 (DB7) goes to output of rotary pot to adjust LCD dimmer
Pin 4 (DB6) goes to ATMega328 D10, physical pin 16
Pin 5 (DB5) goes to GND
Pin 6 (DB4) goes to ATMega328 D9, physical pin 15
Pin 7 (DB3) is not used
Pin 8 (DB2) is not used
Pin 9 (DB1) is not used
Pin 10 (DB0) is not used
Pin 11 (E) goes to ATMega328 D5, physical pin 11
Pin 12 (RW) goes to ATMega328 D4, physical pin 6
Pin 13 (RS) goes to ATMega328 D3, physical pin 5
Pin 14 (V0) goes to ATMega328 D2, physical pin 4
Pin 15 (VDD) goes to 5V
Pin 16 (VSS) goes to a 220 ohm resistor, then to Uno GND
*/


void setup()
{
  lcd.begin (numCols, numRows);
  pinMode(greenLED, OUTPUT);
  pinMode(redLED, OUTPUT);
}

void loop()
{
  int i = random(24);
  int j = random(9);
  int DangerDelay = 20000 + j*1000;
  
  lcd.clear();
  
  lcd.setCursor(0,0);
  lcd.print("HALL OF JUSTICE");
  lcd.setCursor(0,1);
  lcd.print("EVERYTHING GOOD");
  digitalWrite(greenLED,HIGH);
  digitalWrite(redLED, LOW);
  delay(DangerDelay);  // delay time between Danger signal alarms

  // Sound the alarm and show the message!
  lcd.clear();
  digitalWrite(greenLED,LOW);
  digitalWrite(redLED, HIGH);

  switch (i)
  {
    case 0:
      lcd.setCursor(0,0);
      lcd.print("WONDER WOMAN HAS");
      lcd.setCursor(0,1);
      lcd.print("PANTIES IN TWIST");
      break;
    case 1:
      lcd.setCursor(0,0);
      lcd.print("AQUAMAN NEEDS");
      lcd.setCursor(0,1);
      lcd.print("HELP RIGHT AWAY!");
      break;
    case 2:
      lcd.setCursor(0,0);
      lcd.print("GIANT IGUANA IS");
      lcd.setCursor(0,1);
      lcd.print("WRECKING SEATTLE");
      break;
    case 3:
      lcd.setCursor(0,0);
      lcd.print("ASTEROID HEADING");
      lcd.setCursor(0,1);
      lcd.print("TOWARD THE EARTH");
      break;
    case 4:
      lcd.setCursor(0,0);
      lcd.print("HUGE TORNADO");
      lcd.setCursor(0,1);
      lcd.print("OVER MALTBY ELEM");
      break;
    case 5:
      lcd.setCursor(0,0);
      lcd.print("VOLCANO ERUPTING");
      lcd.setCursor(0,1);
      lcd.print("PEOPLE IN DANGER");
      break;
    case 6:
      lcd.setCursor(0,0);
      lcd.print("LEX LUTHOR HAS");
      lcd.setCursor(0,1);
      lcd.print("ESCAPED PRISON!");
      break;
    case 7:
      lcd.setCursor(0,0);
      lcd.print("RIDDLER ROBBING");
      lcd.setCursor(0,1);
      lcd.print("WOODINVILLE BANK");
      break;
    case 8:
      lcd.setCursor(0,0);
      lcd.print("TIDAL WAVE HEADS");
      lcd.setCursor(0,1);
      lcd.print("TOWARD HAWAII");
      break;
    case 9:
      lcd.setCursor(0,0);
      lcd.print("HUGE WHIRLPOOL");
      lcd.setCursor(0,1);
      lcd.print("IN PACIFIC OCEAN");
      break;
    case 10:
      lcd.setCursor(0,0);
      lcd.print("MONSTER CHICKEN");
      lcd.setCursor(0,1);
      lcd.print("ATTACKING KANSAS");
      break;
    case 11:
      lcd.setCursor(0,0);
      lcd.print("BIZARRO STEALING");
      lcd.setCursor(0,1);
      lcd.print("ALL TEDDY BEARS");
      break;
    case 12:
      lcd.setCursor(0,0);
      lcd.print("BATMAN TRAPPED");
      lcd.setCursor(0,1);
      lcd.print("IN GENIE BOTTLE");
      break;
    case 13:
      lcd.setCursor(0,0);
      lcd.print("LEGION OF DOOM");
      lcd.setCursor(0,1);
      lcd.print("SEEN OVER MALTBY");
      break;
    case 14:
      lcd.setCursor(0,0);
      lcd.print("GLEEK NEEDS MORE");
      lcd.setCursor(0,1);
      lcd.print("BANANAS AND MILK");
      break;
    case 15:
      lcd.setCursor(0,0);
      lcd.print("WORLD CHOCOLATE");
      lcd.setCursor(0,1);
      lcd.print("POISONED BY DOOM");
      break;
    case 16:
      lcd.setCursor(0,0);
      lcd.print("HULK FARTED -");
      lcd.setCursor(0,1);
      lcd.print("SMELLS TERRIBLE");
      break;
    case 17:
      lcd.setCursor(0,0);
      lcd.print("SPACE MAN NEEDS");
      lcd.setCursor(0,1);
      lcd.print("HELP RIGHT AWAY");
      break;
    case 18:
      lcd.setCursor(0,0);
      lcd.print("SUN IS GETTING");
      lcd.setCursor(0,1);
      lcd.print("COLDER - HELP!");
      break;
    case 19:
      lcd.setCursor(0,0);
      lcd.print("LUTHOR TRYING TO");
      lcd.setCursor(0,1);
      lcd.print("SPOIL HALLOWEEN");
      break;
    case 20:
      lcd.setCursor(0,0);
      lcd.print("NEW YORK CITY");
      lcd.setCursor(0,1);
      lcd.print("LOST ELECTRICITY");
      break;
    case 21:
      lcd.setCursor(0,0);
      lcd.print("MANNY BROWN LOST");
      lcd.setCursor(0,1);
      lcd.print("PLEASE FIND HIM");
      break;
    case 22:
      lcd.setCursor(0,0);
      lcd.print("BIG SQUID EATING");
      lcd.setCursor(0,1);
      lcd.print("THE BAY BRIDGE");
      break;
    case 23:
      lcd.setCursor(0,0);
      lcd.print("SOLOMON GRUNDY");
      lcd.setCursor(0,1);
      lcd.print("SEEN IN SEATTLE");
      break;
    default:
      lcd.setCursor(0,0);
      lcd.print("CAL POLY SLO");
      lcd.setCursor(0,1);
      lcd.print("MECH ENG 1991");
  }
  delay(25000);
}

