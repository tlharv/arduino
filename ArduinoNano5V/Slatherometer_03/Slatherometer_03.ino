/*********************************************************************
This is an example for our Monochrome OLEDs based on SSD1306 drivers

  Pick one up today in the adafruit shop!
  ------> http://www.adafruit.com/category/63_98

This example is for a 128x64 size display using SPI to communicate
4 or 5 pins are required to interface

Adafruit invests time and resources providing this open source code, 
please support Adafruit and open-source hardware by purchasing 
products from Adafruit!

Written by Limor Fried/Ladyada  for Adafruit Industries.  
BSD license, check license.txt for more information
All text above, and the splash screen must be included in any redistribution
*********************************************************************
                              +-\/-+
                      RESET  1|    |28  (A 5)
                       RX 0  2|    |27  (A 4)
                       TX 1  3|    |26  (A 3)
                      (D 2)  4|    |25  (A 2)  <-- UV SIGNAL
                      (D 3)  5|    |24  (A 1)
                      (D 4)  6|    |23  (A 0)
                        VCC  7|    |22  GND
                        GND  8|    |21  AREF
                             9|    |20  VCC
                            10|    |19  (D13)  <-- OLED RESET
         ds18b20 -->  (D 5) 11|    |18  (D12)  <-- OLED CS
                      (D 6) 12|    |17  (D11)  <-- OLED DC
                      (D 7) 13|    |16  (D10)  <-- OLED CLK
                      (D 8) 14|    |15  (D 9)  <-- OLED MOSI
                              +----+
                           
PWM = Pulse Width Modulation (good for analog out signals like glowing LEDs */

#include <DHT.h>
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <OneWire.h>
#include <DallasTemperature.h>

//#define ONE_WIRE_BUS 5
//OneWire oneWire(ONE_WIRE_BUS);
//DallasTemperature sensors (&oneWire);

#define DHTPIN 5
#define DHTTYPE DHT22 // also AM2302
DHT dht(DHTPIN, DHTTYPE);

// If using software SPI (the default case):
#define OLED_MOSI   9
#define OLED_CLK   10
#define OLED_DC    11
#define OLED_CS    12
#define OLED_RESET 13
Adafruit_SSD1306 display(OLED_MOSI, OLED_CLK, OLED_DC, OLED_RESET, OLED_CS);

const int UVpin = 2; // to A2 on Uno

//#define NUMFLAKES 10
//#define XPOS 0
//#define YPOS 1
//#define DELTAY 2

//#define LOGO16_GLCD_HEIGHT 16 
//#define LOGO16_GLCD_WIDTH  16 
static const unsigned char PROGMEM logo16_glcd_bmp[] =
{ B00000000, B11000000,
  B00000001, B11000000,
  B00000001, B11000000,
  B00000011, B11100000,
  B11110011, B11100000,
  B11111110, B11111000,
  B01111110, B11111111,
  B00110011, B10011111,
  B00011111, B11111100,
  B00001101, B01110000,
  B00011011, B10100000,
  B00111111, B11100000,
  B00111111, B11110000,
  B01111100, B11110000,
  B01110000, B01110000,
  B00000000, B00110000 };

#if (SSD1306_LCDHEIGHT != 64)
#error("Height incorrect, please fix Adafruit_SSD1306.h!");
#endif

void setup()   {                
  //Serial.begin(9600);
  
  pinMode(UVpin, INPUT);
  
  // by default, we'll generate the high voltage from the 3.3v line internally! (neat!)
  display.begin(SSD1306_SWITCHCAPVCC);
  // init done
  
  //pinMode(ONE_WIRE_BUS, INPUT);
  //sensors.begin(); // start up the library
  dht.begin();
}

void loop() {
  // Clear the buffer.
  display.clearDisplay();
  
  // Get the temperature
  float TempC;
  float TempF;
  float h; //humidity reading
  
  //pinMode(ONE_WIRE_BUS, OUTPUT);
  // call sensors.requestTemperatures() to issue a global temp request to all devices on the bus
  //sensors.requestTemperatures();
  //pinMode(ONE_WIRE_BUS, INPUT);
  //TempC = sensors.getTempCByIndex(0);
  h = dht.readHumidity();
  TempC = dht.readTemperature();
  TempF = (9.0 * TempC / 5.0) + 32.0;

  // Get the UV index reading
  int UVindex = analogRead(UVpin) * 3.0 / 102.4;
  String msg;
  
  // text display tests
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(0,0);
  display.println("Hello, Susanne!");
  //display.println("");
  display.print("The UV index = ");
  display.println(UVindex);
  display.println("Sunburn risk is");
  if (UVindex < 3) msg="LOW";
  if ((UVindex >= 3) && (UVindex < 6)) msg = "MODERATE";
  if ((UVindex >= 6) && (UVindex < 8)) msg = "HIGH";
  if ((UVindex >= 8) && (UVindex < 10)) msg = "VERY HIGH";
  if (UVindex > 10) msg = "EXTREME";
  display.println(msg);
  display.println("");
  display.print("Temp = ");
  display.print(TempF);
  display.println("F");
  display.print("Humidity = ");
  display.print(h);
  display.print("%");
  display.display();
  delay(2000);
}

