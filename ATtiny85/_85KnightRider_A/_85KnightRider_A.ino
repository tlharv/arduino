// ATMEL ATTINY85 / ARDUINO
//
//                  +-\/-+
// Ain0 (D 5) PB5  1|    |8  Vcc
// Ain3 (D 3) PB3  2|    |7  PB2 (D 2)  Ain1
// Ain2 (D 4) PB4  3|    |6  PB1 (D 1) pwm1
//            GND  4|    |5  PB0 (D 0) pwm0
//                  +----+

const int NumOfLEDs = 5;
int pinArray[NumOfLEDs];
int count = 0;
int timer = 30; // 30 has a nice fluid effect but is fairly fast

void setup(){
  for (count=0;count<NumOfLEDs;count++) {
    pinArray[count] = count;
    pinMode(pinArray[count], OUTPUT);
  }
}

void loop() {
  for (count=0;count<(NumOfLEDs-1);count++) {
   digitalWrite(pinArray[count], HIGH);
   delay(timer);
   digitalWrite(pinArray[count + 1], HIGH);
   delay(timer);
   digitalWrite(pinArray[count], LOW);
   delay(timer*2);
  }
  for (count=(NumOfLEDs-1);count>0;count--) {
   digitalWrite(pinArray[count], HIGH);
   delay(timer);
   digitalWrite(pinArray[count - 1], HIGH);
   delay(timer);
   digitalWrite(pinArray[count], LOW);
   delay(timer*2);
  }
}
