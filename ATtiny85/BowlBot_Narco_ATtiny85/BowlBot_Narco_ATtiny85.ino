/**
 * Narcoleptic - A sleep library for Arduino
 * Copyright (C) 2010 Peter Knight (Cathedrow)
 *
*/

/*
** Welcome to Narcoleptic. This library allows you to dramatically reduce
** the power consumption of your Arduino project.
**
** Whilst in a Narcoleptic delay, the CPU current consumption drops from
** around 15mA down to about 7µA.
**
** Note that Narcoleptic only shuts down the CPU. It does not shut down
** anything else consuming current - LEDs, or the USB to serial chip.
** For low current applications, consider using boards without those,
** such as the Sparkfun Arduino Pro with the power LED removed.
**
** Narcoleptic is available from: http://narcoleptic.googlecode.com
*/

// This is a test program to see if the MCU can sleep until a PIR sensor
// detection makes it wake up.

#include <Narcoleptic.h>

const int LightSensorPin = 3;    // analog in, to ADC3 for light sensor, pin 2 on ATtiny85
const int ledPin = 0;
const int LightLevelThreshold = 200;

const int PIRsensorPin = 4;  // digital in, to ADC1 for light sensor, pin 7 on ATtiny85

void setup() {
  pinMode(ledPin, OUTPUT);
  pinMode(LightSensorPin, INPUT);
  pinMode(PIRsensorPin, INPUT);   // The PIR sensor
  digitalWrite(ledPin,LOW);
}

void loop() {
  Narcoleptic.delay(500); // During this time power consumption is minimised

  while (digitalRead(PIRsensorPin) == HIGH) {
    int rate = analogRead(LightSensorPin);
    if (rate < LightLevelThreshold)
    {
      digitalWrite(ledPin,HIGH);
      delay(10000);
      digitalWrite(ledPin,LOW);
      //delay(50);
    }
    else
    {
      digitalWrite(ledPin, LOW);
    }
  }
}
