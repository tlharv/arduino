

// Refer to 6.8 in Arduino Cookbook for temperature equations
//
// This sketch simply lights a blue LED if the temperature falls below 65F.
// Otherwise, if above 65F the red LED is lit.

const int inPin = 3; // analog pin for temp sensor
const int led = 1;

void setup()
{
  pinMode(inPin, INPUT);
  pinMode(led, OUTPUT);
}

void loop()
{
  int value = analogRead(inPin);
  float millivolts = (value / 1024.0) * 5000;
  float celsius = (millivolts / 10.0) - 50; // this equation from a guy on the SparkFun site
  float fahrenheit = (celsius *9)/5 + 32;
  
  if (value == 0)
  {
    analogWrite(led,255);
  }
  else
  {
    analogWrite(led,0);
  }
  
  delay(1000);
}

