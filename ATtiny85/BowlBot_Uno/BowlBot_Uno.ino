/*
  PIR sketch
  a passive infrared motion sensor connected to pin 2
  lights the LED on pin 13.
*/

const int ledPin = 13; // choose the pin for the LED
const int inputPin = 2; // choose the input pin for the PIR sensor
const int lightSensorPin = 0; // analog pin 0 for light sensor
const int LightLevelThreshold = 100;

void setup()
{
  Serial.begin(9600);
  pinMode(ledPin, OUTPUT);   // declare LED as output
  pinMode(inputPin, INPUT);  // declare PIR as input
  pinMode(lightSensorPin, INPUT); // declare photoresistor as input
}

void loop()
{
  int rate = analogRead(lightSensorPin);
  Serial.print("Light sensor reading is ");
  Serial.println(rate);
  delay(1000);
  if (rate < LightLevelThreshold) // then too dark to see bowl
  {
    // digitalWrite(ledPin, HIGH);
    int val = digitalRead(inputPin);  // read input value
    if (val == HIGH)                  // check if the input is HIGH
    {
      digitalWrite(ledPin, HIGH);     // turn LED on if motion detected
      delay(25000);
      digitalWrite(ledPin, LOW);      // turn LED off
    }
  }
  else
  {
    digitalWrite(ledPin, LOW);
  }
}
