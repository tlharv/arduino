const int redPin = 0;   // analog out, to PB0 for red LED, pin 5 on ATtiny85
const int bluePin = 1;  // analog out, to PB1 for blue LED, pin 6 on ATtiny85
const int greenPin = 4; // analog out, to PB4 for green LED, pin 3 on ATtiny85

void setup()
{
  pinMode(bluePin, OUTPUT);
  pinMode(redPin, OUTPUT);
  pinMode(greenPin, OUTPUT);

}

void loop()
{
  GlowLightOnce(bluePin);
  GlowLightOnce(greenPin);
  GlowLightOnce(redPin);
}

void GlowLightOnce(int bulb)  // subroutine applies one fade in - fade out blink of an LED
{
  int brightness = 0;
  int fadeAmount = 5;
  int totalcount = 0;

  do
  {
    analogWrite(bulb, brightness);
    brightness = brightness + fadeAmount;
    totalcount++;
    if (brightness == 255)
    {
      fadeAmount = -fadeAmount;
    }
    delay(35);
  } while (totalcount < 103);
  delay(1000);
}
