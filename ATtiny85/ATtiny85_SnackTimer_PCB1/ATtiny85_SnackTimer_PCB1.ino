// ATMEL ATTINY85 / ARDUINO
//
//                  +-\/-+
// Ain0 (D 5) PB5  1|    |8  Vcc
// Ain3 (D 3) PB3  2|    |7  PB2 (D 2) Ain1
// Ain2 (D 4) PB4  3|    |6  PB1 (D 1) pwm1
//            GND  4|    |5  PB0 (D 0) pwm0
//                  +----+
//
// This is a 15 minute timer for Mrs. Johnson's Montessori classroom, so kids know when
// to end their snack time based on visual cues instead of a disruptive audible alarm.

#include <avr/sleep.h>

const long oneSecond = 1000;
const long oneMinute = oneSecond * 60;

const int GreenLED = 4;
const int RedLED = 0;
const int YellowLED = 1;
const int S1 = 2; // DIP switch 1
const int S2 = 3; // DIP switch 2

//const long GreenDuration = (12 * oneMinute) + (50 * oneSecond);
//const long GreenDuration = (8 * oneSecond);
const int GreenFlashes = 7; // this takes about 10 seconds
const int RedFlashes = 200;
//const long YellowDuration = oneMinute + (50 * oneSecond);
//const long YellowDuration = (5 * oneSecond);
const int YellowFlashes = 7; // this takes about 10 seconds
const int AlarmDelay = 100;
const int AlarmFlashCount = 10;

int x,heartbeats,hbcycle,hbval,hbdelta;
int s1state, s2state, TS; // state of DIP switches 1 & 2, and Timer Setting value

void setup()
{
  pinMode(GreenLED, OUTPUT);   // declare LED as output
  pinMode(YellowLED, OUTPUT);   // declare LED as output
  pinMode(RedLED, OUTPUT);   // declare LED as output
  pinMode(S1, INPUT);
  pinMode(S2, INPUT);
  
  // Read DIP switches and determine timer setting
  s1state = 1-digitalRead(S1);
  s2state = 1-digitalRead(S2);
  int reading = 10*s1state + s2state;
  TS = TimerSetting(reading);  
}

void loop()
{
  analogWrite(RedLED,255/TS);
}


void IdleChip(unsigned long duration)
{
  unsigned long start = millis();
  set_sleep_mode(SLEEP_MODE_IDLE);  // choose idle mode for the chip
  sleep_enable();                              // this puts the chip into sleep mode, but doesn't command it to sleep yet
  while (millis() - start <= duration) {
    sleep_mode();  // put chip in idle mode
  }
  sleep_disable();
}

int TimerSetting(int r) {
  int TimerSetting;
  switch (r)
  {
    case 0:  // Switches at 00
      TimerSetting = 1;
      break;
    case 1:  // Switches at 01
      TimerSetting = 5;
      break;
    case 10: // Switches at 10
      TimerSetting = 10;
      break;
    case 11: // Switches at 11
      TimerSetting = 15;
      break;
  }
  return TimerSetting;
}
