#include <Adafruit_NeoPixel.h>
#include <avr/sleep.h>

#define PIN 0
#define NumOfPixels 2

// ATMEL ATTINY85 / ARDUINO
//
//                  +-\/-+
// Ain0 (D 5) PB5  1|    |8  Vcc
// Ain3 (D 3) PB3  2|    |7  PB2 (D 2) Ain1
// Ain2 (D 4) PB4  3|    |6  PB1 (D 1) pwm1
//            GND  4|    |5  PB0 (D 0) pwm0
//                  +----+
//

// Parameter 1 = number of pixels in strip
// Parameter 2 = Arduino pin number (most are valid)
// Parameter 3 = pixel type flags, add together as needed:
//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
//   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
//   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
//   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)
Adafruit_NeoPixel strip = Adafruit_NeoPixel(NumOfPixels, PIN, NEO_GRB + NEO_KHZ800);

// IMPORTANT: To reduce NeoPixel burnout risk, add 1000 uF capacitor across
// pixel power leads, add 300 - 500 Ohm resistor on first pixel's data input
// and minimize distance between Arduino and first pixel.  Avoid connecting
// on a live circuit...if you must, connect GND first.

uint32_t red = strip.Color(255,0,0);
uint32_t green = strip.Color(0,255,0);
uint32_t blue = strip.Color(0,0,255);
uint32_t magenta = strip.Color(255,0,255);
uint32_t cyan = strip.Color(0,255,255);
uint32_t white = strip.Color(255,255,255);
uint32_t paleyellow = strip.Color(255,255,0);
uint32_t off = strip.Color(0,0,0);

uint32_t orange = strip.Color(255,102,0);
uint32_t pumpkin = strip.Color(255,60,0);
uint32_t gold = strip.Color(255,180,0);
uint32_t pink = strip.Color(255,114,86);

float RandBrightRange = 255;
float RandTimeRange = 750;

int onesecond = 1000;
int oneminute = 60 * onesecond;

int RandomTimeBetweenLightning = 30 * onesecond;

void setup() {
  strip.begin();
  strip.show(); // Initialize all pixels to 'off'
}

void loop() {
  //GlowSingleLightOnce(0,red);
  //GlowSingleLightOnce(1,red);
  // GlowAllLightsOnce(white);
  IdleChip(random(RandomTimeBetweenLightning));
  Lightning(0);
  IdleChip(250);
  Lightning(1);
  IdleChip(1000);
}


void GlowSingleLightOnce(int bulb, uint32_t color)  // subroutine applies one fade in - fade out blink of an LED
{
  int brightness = 0;
  int fadeAmount = 10;
  int totalcount = 0;

  strip.show();
  do
  {
    brightness = brightness + fadeAmount;
    strip.setBrightness(brightness);
    strip.setPixelColor(bulb,color);
    strip.show();
    totalcount++;
    if (brightness == 250)
    {
      fadeAmount = -fadeAmount;
    }
    IdleChip(35);
  } while (totalcount < 50);
}

void GlowAllLightsOnce(uint32_t color)  // subroutine applies one fade in - fade out blink of an LED
{
  int brightness = 0;
  int fadeAmount = 10;
  int totalcount = 0;

  strip.show();
  do
  {
    brightness = brightness + fadeAmount;
    strip.setBrightness(brightness);
    for (int j = 0;j<NumOfPixels; j++)
    {
      strip.setPixelColor(j,color);
    }
    strip.show();
    totalcount++;
    if (brightness == 250)
    {
      fadeAmount = -fadeAmount;
    }
    IdleChip(35);
  } while (totalcount < 50);
}


void Lightning(int bulb)
{
  int BETWEEN = 2579;
  int DURATION = 75;
  int TIMES = 20;
   
  unsigned long lastTime = 0;
  int waitTime = 0;
  
  if (millis() - waitTime > lastTime)  // time for a new flash
  {
    // adjust timing params
    lastTime += waitTime;
    waitTime = random(BETWEEN);

    for (int i=0; i< random(TIMES); i++)
    {
      strip.setBrightness(255);
      strip.setPixelColor(bulb,white);
      strip.show();
      IdleChip(20 + random(DURATION));
      strip.setBrightness(0);
      strip.setPixelColor(bulb,white);
      strip.show();
      IdleChip(10);
    }
  }
} 

void IdleChip(unsigned long duration)
{
  unsigned long start = millis();
  set_sleep_mode(SLEEP_MODE_IDLE);  // choose idle mode for the chip
  sleep_enable();                              // this puts the chip into sleep mode, but doesn't command it to sleep yet
  while (millis() - start <= duration) {
    sleep_mode();  // put chip in idle mode
  }
  sleep_disable();
}
