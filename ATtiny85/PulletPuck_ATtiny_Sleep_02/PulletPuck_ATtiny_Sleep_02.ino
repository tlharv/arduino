#include <avr/sleep.h>
#include <avr/wdt.h>

#define WDTO_2S   7  // secret code for defining a watchdog timer for 2 seconds, to be used with wdt_enable()
#define WDTO_4S   8  // secret code for defining a watchdog timer for 4 seconds, to be used with wdt_enable()

const int inPin = 3;    // analog in, to ADC3 for temp sensor, pin 2 on ATtiny85
const int redPin = 0;   // analog out, to PB0 for red LED, pin 5 on ATtiny85
const int bluePin = 1;  // analog out, to PB1 for blue LED, pin 6 on ATtiny85
const int greenPin = 4; // analog out, to PB4 for green LED, pin 3 on ATtiny85

const int LL = 65; // Lower Limit of acceptable temperature range; below this the light is blue
const int UL = 75; // Upper Limit of acceptable temperature range; above this the light is red

void setup()
{
  analogReference(INTERNAL1V1);
  wdtSetup();  // this sets up the watchdog timer
}

void loop()
{
  pinMode(bluePin, OUTPUT);
  pinMode(redPin, OUTPUT);
  pinMode(greenPin, OUTPUT);
  
  // Read temperature sensor
  analogRead(inPin);
  delay(10);
  int value = analogRead(inPin);
  
  float millivolts = (value / 1024.0) * 1100.0;
  //float celsius = (millivolts / 10.0) - 50.0; // this equation from a guy on the SparkFun site
  float celsius = (millivolts - 500.0) / 10.0;
  float fahrenheit = (celsius *9.0)/5.0 + 32.0;
  
 // NOW LIGHT THE LED BASED ON THE TEMPERATURE READ
  if (fahrenheit < LL) // then light the blue LED - it's cold!
  {
    analogWrite(greenPin, 0);
    analogWrite(redPin, 0);
    GlowLightOnce(bluePin);
  }
  else if (fahrenheit >= LL && fahrenheit <= UL) // then light the green LED
  {
    analogWrite(bluePin, 0);
    analogWrite(redPin, 0);
    GlowLightOnce(greenPin);
  }
  else // light the red LED
  {
    analogWrite(bluePin, 0);
    analogWrite(greenPin, 0);
    GlowLightOnce(redPin);
  }
  
  // DEFINE SLEEP MODE
  wdt_enable(WDTO_4S);  // Set and start the WDT for the argument's duration
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);  // choose your preferred sleep mode for the chip
  sleep_enable(); // this puts the chip into, sleep mode, but doesn't command it to sleep yet.
  // energy saving move: change pin modes to input
  pinMode(bluePin, INPUT);  
  pinMode(redPin, INPUT);
  pinMode(greenPin, INPUT);
  sleep_mode();  // Last statement of loop() - this command actually puts the chip to sleep.
}

// Watchdog Timer Interrupt
ISR (WDT_vect) {
}

void GlowLightOnce(int bulb)  // subroutine applies one fade in - fade out blink of an LED
{
  int brightness = 0;
  int fadeAmount = 5;
  int totalcount = 0;

  do
  {
    analogWrite(bulb, brightness);
    brightness = brightness + fadeAmount;
    totalcount++;
    if (brightness == 255)
    {
      fadeAmount = -fadeAmount;
    }
    IdleChip(35);
  } while (totalcount < 103);
  //IdleChip(1000);
}

void IdleChip(unsigned long duration)
{
  unsigned long start = millis();
  set_sleep_mode(SLEEP_MODE_IDLE);  // choose idle mode for the chip
  sleep_enable();                              // this puts the chip into sleep mode, but doesn't command it to sleep yet
  while (millis() - start <= duration) {
    sleep_mode();  // put chip in idle mode
  }
  sleep_disable();
}

void wdtSetup() // Set up watchdog timer
{
  cli(); // disable all interrupts
  MCUSR = 0;  // Reset Micro Controller Unit Status Register
  // WDTCR register gives you access to the ATtiny85 WDT.
  // Bit 4 = Watchdog Change Enable.  Must be cleared to change or clear the WDE bit.
  // Bit 3 = WDT bit.  Must be cleared to set WDP3 through WDP0.
  WDTCR |= B00011000; // clear WDTCSR register bits 3 & 4
  WDTCR = B01000111;  // Set the WDP3 through WDP0 bits, to set the WDT prescale to two seconds.
  sei(); // enables all interrupts
}

