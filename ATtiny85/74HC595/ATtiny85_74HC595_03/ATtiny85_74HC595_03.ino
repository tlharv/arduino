#include <avr/sleep.h>
#include <avr/wdt.h>

#define WDTO_2S   7  // secret code for defining a watchdog timer for 2 seconds, to be used with wdt_enable()
#define WDTO_4S   8  // secret code for defining a watchdog timer for 4 seconds, to be used with wdt_enable()


// 8-Bit latching shift register 74HC595
// Q1 through Q8 are the shift register outputs
//
//       +-\/-+
//  Q2  1|    |16  VCC (3.3 to 6VDC)
//  Q3  2|    |15  Q1
//  Q4  3|    |14  SER    (serial input)
//  Q5  4|    |13  OE     (output enable)
//  Q6  5|    |12  RCLK   (register clock)
//  Q7  6|    |11  SRCLK  (serial clock)
//  Q8  7|    |10  SRCLR  (serial clear) - connect to VCC to enable
//  GND 8|    | 9  
//       +----+

int SER_Pin = 0;   // D0, or physical pin 5 on ATtiny85
int RCLK_Pin = 1;  // D1, or physical pin 6 on ATtiny85
int SRCLK_Pin = 2; // D2, or physical pin 7 on ATtiny85

//Define how many shift registers we're using
#define number_of_74HC595s 1

//Define total number of shift pins (always will be 8 per chip)
#define numOfRegisterPins number_of_74HC595s * 8

boolean registers[numOfRegisterPins];

void setup()
{
  pinMode(SER_Pin, OUTPUT);
  pinMode(RCLK_Pin, OUTPUT);
  pinMode(SRCLK_Pin, OUTPUT);
  
  // reset all register pins
  clearRegisters();
  writeRegisters();
  wdtSetup();  // this sets up the watchdog timer
}

//set all register pins to LOW
void clearRegisters()
{
  for (int i = numOfRegisterPins - 1; i>=0; i--)
  {
    registers[i] = LOW;
  }
}

//set and display registers
//Only call AFTER all values are set how you would like (slow otherwise)
void writeRegisters()
{
  digitalWrite(RCLK_Pin, LOW);
  
  for (int i = numOfRegisterPins-1; i>=0; i--)
  {
    digitalWrite(SRCLK_Pin, LOW);
    int val = registers[i];
    digitalWrite(SER_Pin, val);
    digitalWrite(SRCLK_Pin, HIGH);
  }
  digitalWrite(RCLK_Pin, HIGH);
}

//set an individual pin HIGH or LOW
void setRegisterPin(int index, int value)
{
  registers[index] = value;
}

void loop()
{
  pinMode(SER_Pin, OUTPUT);  
  pinMode(RCLK_Pin, OUTPUT);
  pinMode(SRCLK_Pin, OUTPUT);

  // employ an unused analog pin to generate random seed
  randomSeed(analogRead(2));
  
  for (int x=0; x<50; x++)
  {
    int i = random(numOfRegisterPins);
  
    for (int count=0; count<numOfRegisterPins; count++)
    {
      if (count== i)
      {
        setRegisterPin(count,HIGH);
      }
      else
      {
        setRegisterPin(count,LOW);
      }
    }
    writeRegisters(); // must be called to display changes
    IdleChip(120);     
  }
   
  // DEFINE SLEEP MODE
  wdt_enable(WDTO_4S);  // Set and start the WDT for the argument's duration
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);  // choose your preferred sleep mode for the chip
  sleep_enable(); // this puts the chip into, sleep mode, but doesn't command it to sleep yet.
  // energy saving move: change pin modes to input
  clearRegisters();
  writeRegisters();
  pinMode(SER_Pin, INPUT);  
  pinMode(RCLK_Pin, INPUT);
  pinMode(SRCLK_Pin, INPUT);
  sleep_mode();  // Last statement of loop() - this command actually puts the chip to sleep.
}

// Watchdog Timer Interrupt
ISR (WDT_vect)
{
}

void IdleChip(unsigned long duration)
{
  unsigned long start = millis();
  set_sleep_mode(SLEEP_MODE_IDLE);  // choose idle mode for the chip
  sleep_enable();                              // this puts the chip into sleep mode, but doesn't command it to sleep yet
  while (millis() - start <= duration) {
    sleep_mode();  // put chip in idle mode
  }
  sleep_disable();
}

void wdtSetup() // Set up watchdog timer
{
  cli(); // disable all interrupts
  MCUSR = 0;  // Reset Micro Controller Unit Status Register
  // WDTCR register gives you access to the ATtiny85 WDT.
  // Bit 4 = Watchdog Change Enable.  Must be cleared to change or clear the WDE bit.
  // Bit 3 = WDT bit.  Must be cleared to set WDP3 through WDP0.
  WDTCR |= B00011000; // clear WDTCSR register bits 3 & 4
  WDTCR = B01000111;  // Set the WDP3 through WDP0 bits, to set the WDT prescale to two seconds.
  sei(); // enables all interrupts
}
