// ATMEL ATTINY85 / ARDUINO
//
//                  +-\/-+
// Ain0 (D 5) PB5  1|    |8  Vcc
// Ain3 (D 3) PB3  2|    |7  PB2 (D 2)  Ain1
// Ain2 (D 4) PB4  3|    |6  PB1 (D 1) pwm1
//            GND  4|    |5  PB0 (D 0) pwm0
//                  +----+

const int DelayOn = 600;
const int DelayOff = 500;

const int dataPin = 0;  // To D0 or pin 5 on ATtiny85
const int latchPin = 1; // To D1 or pin 6 on ATtiny85
const int clockPin = 2; // To D2 or pin 7 on ATtiny85
const int piezoPin = 3; // To D3 or pin 2 on ATtiny85

byte leds = 0;
int var = 0;

void setup() 
{
  pinMode(latchPin, OUTPUT);
  pinMode(dataPin, OUTPUT);  
  pinMode(clockPin, OUTPUT);
}

void loop() 
{
  while(var < 5) {
    leds = 0;
    updateShiftRegister();
    delay(DelayOff);
    for (int i = 0; i < 6; i++)
    {
      bitSet(leds, i);
      updateShiftRegister();
      //delay(200);
    }
    tone(piezoPin, 294);
    delay(DelayOn);
    noTone(piezoPin);
    var++;
  }
  leds = 0;
  updateShiftRegister();
}

void updateShiftRegister()
{
   digitalWrite(latchPin, LOW);
   shiftOut(dataPin, clockPin, LSBFIRST, leds);
   digitalWrite(latchPin, HIGH);
}


