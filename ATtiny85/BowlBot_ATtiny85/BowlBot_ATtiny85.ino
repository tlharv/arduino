// ATMEL ATTINY85 / ARDUINO
//
//                  +-\/-+
// Ain0 (D 5) PB5  1|    |8  Vcc
// Ain3 (D 3) PB3  2|    |7  PB2 (D 2) Ain1
// Ain2 (D 4) PB4  3|    |6  PB1 (D 1) pwm1
//            GND  4|    |5  PB0 (D 0) pwm0
//                  +----+


const int LightSensorPin = 3;    // analog in, to ADC3 for light sensor, pin 2 on ATtiny85
const int ledPin = 0;
const int LightLevelThreshold = 200;

const int PIRsensorPin = 4;  // digital in, to D4 for light sensor, pin 3 on ATtiny85

void setup()
{
  pinMode(ledPin, OUTPUT);   // declare LED as output
  pinMode(LightSensorPin, INPUT); // declare photoresistor as input
  pinMode(PIRsensorPin, INPUT);  // declare PIR sensor as input
}

void loop()
{
  int rate = analogRead(LightSensorPin);
  if (rate < LightLevelThreshold) // then too dark to see bowl
  {
    int val = digitalRead(PIRsensorPin);  // read input value
    if (val == HIGH)                  // check if the input is HIGH
    {
      analogWrite(ledPin, 255);     // turn LED on if motion detected
      delay(20000);
      analogWrite(ledPin, 0);      // turn LED off
    }
  }
  else
 {
   digitalWrite(ledPin, LOW);
  }
}
