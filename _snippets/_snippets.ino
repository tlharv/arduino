#include <stdio.h>
#include <stdlib.h>

int dice(int num, int faces)
{
  int roll;
  int sum = 0;
  int x;
    
  for (x=0; x<num; x++)
  {
    roll = rand() % faces;
    sum += roll;
  }
  return sum;
}

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  int sum, faces, numdice;

  faces = 6;
  numdice = 10;

    sum = dice(faces,numdice);
    Serial.print("The sum of ");
    Serial.print(numdice);
    Serial.print("d");
    Serial.print(faces);
    Serial.print(" = ");
    Serial.println(sum);
}

void loop() {
  // put your main code here, to run repeatedly:

}
