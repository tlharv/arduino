#include <Time.h>

void setup()
{
  Serial.begin(9600);
  setTime(21,13,20,10,7,12);
}

void loop()
{
  digitalClockDisplay();
  delay(1000);
}

void digitalClockDisplay()
{
  Serial.print(hour());
  printDigits(minute());
  printDigits(second());
  Serial.print(" ");
  Serial.print(day());
  Serial.print(" ");
  Serial.print(month());
  Serial.print(" ");
  Serial.print(year());
  Serial.println();
}

void printDigits(int digits)
{
  // utility function for clock display: prints preceding colon and leading zero
  Serial.print(":");
  if(digits < 10)
    Serial.print('0');
  Serial.print(digits);
}
