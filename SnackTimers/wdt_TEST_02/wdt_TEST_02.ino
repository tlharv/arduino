// ATMEL ATTINY85 / ARDUINO
//
//                  +-\/-+
// Ain0 (D 5) PB5  1|    |8  Vcc
// Ain3 (D 3) PB3  2|    |7  PB2 (D 2) Ain1
// Ain2 (D 4) PB4  3|    |6  PB1 (D 1) pwm1
//            GND  4|    |5  PB0 (D 0) pwm0
//                  +----+
//
// This is a 15 minute timer for Mrs. Johnson's Montessori classroom, so kids know when
// to end their snack time based on visual cues instead of a disruptive audible alarm.


#include <avr/sleep.h>
#include <avr/wdt.h>

//Watchdog timer wake duration options
#define WDTO_15MS   0  // secret code for defining a watchdog timer for 15 milliseconds, to be used with wdt_enable()
#define WDTO_30MS   1  // secret code for defining a watchdog timer for 30 milliseconds, to be used with wdt_enable()
#define WDTO_60MS   2  // secret code for defining a watchdog timer for 60 milliseconds, to be used with wdt_enable()
#define WDTO_120MS  3  // secret code for defining a watchdog timer for 120 milliseconds, to be used with wdt_enable()
#define WDTO_250MS  4  // secret code for defining a watchdog timer for 250 milliseconds, to be used with wdt_enable()
#define WDTO_500MS  5  // secret code for defining a watchdog timer for 500 milliseconds, to be used with wdt_enable()
#define WDTO_1S     6  // secret code for defining a watchdog timer for 1 second, to be used with wdt_enable()
#define WDTO_2S     7  // secret code for defining a watchdog timer for 2 seconds, to be used with wdt_enable()
#define WDTO_4S     8  // secret code for defining a watchdog timer for 4 seconds, to be used with wdt_enable()
#define WDTO_8S     9  // secret code for defining a watchdog timer for 8 seconds, to be used with wdt_enable()

const long oneSecond = 1000;
const long oneMinute = oneSecond * 60;

const int GreenLED = 4; // physical pin 3 on ATtiny85
const int RedLED = 0; // physical pin 5 on ATtiny85
const int YellowLED = 1; // physical pin 6 on ATtiny85
const int S1 = 2; // DIP switch 1 -- physical pin 7 on ATtiny85
const int S2 = 3; // DIP switch 2 -- physical pin 2 on ATtiny85

long GreenDuration, YellowDuration; // global variables, set based on DIP switch position

const int GreenFlashes = 7;
const int RedFlashes = 10;
const int YellowFlashes = 7;
const int AlarmDelay = 100;
const int AlarmFlashCount = 10;

int x,heartbeats,hbcycle,hbval,hbdelta;
int s1state, s2state, TS; // state of DIP switches 1 & 2, and Timer Setting value

int TimerComplete = 0;

void setup()
{
  //pinMode(LEDPin, OUTPUT);
  //wdtSetup();  //this subroutine configures the watchdog timer
  SnackTimer();
}

void loop()
{
  
  // Flash yellow LED to signal that you're entering power-down mode
  pinMode(YellowLED, OUTPUT);
  for (x=0; x<5; x++) {
    analogWrite(YellowLED, 255);
    IdleChip(125);
    analogWrite(YellowLED, 0);
    IdleChip(250);
  }
  pinMode(YellowLED, INPUT);

  
  // DEFINE AND ENGAGE SLEEP MODE
  //wdt_enable(WDTO_8S);  // Set and start the WDT for the argument's duration
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);  // choose your preferred sleep mode for the chip
  sleep_enable(); // this puts the chip into, sleep mode, but doesn't command it to sleep yet.
  sleep_mode();  // Last statement of loop() - this command actually puts the chip to sleep.
}


// Watchdog Timer Interrupt
//ISR (WDT_vect) {
//}



void IdleChip(unsigned long duration)
{
  unsigned long start = millis();
  set_sleep_mode(SLEEP_MODE_IDLE);  // choose idle mode for the chip
  sleep_enable();                              // this puts the chip into sleep mode, but doesn't command it to sleep yet
  while (millis() - start <= duration) {
    sleep_mode();  // put chip in idle mode
  }
  sleep_disable();
}

void wdtSetup() // Set up watchdog timer
{
  cli(); // disable all interrupts
  MCUSR = 0;  // Reset Micro Controller Unit Status Register
  // WDTCR register gives you access to the ATtiny85 WDT.
  // Bit 4 = Watchdog Change Enable.  Must be cleared to change or clear the WDE bit.
  // Bit 3 = WDT bit.  Must be cleared to set WDP3 through WDP0.
  WDTCR |= B00011000; // clear WDTCSR register bits 3 & 4
  WDTCR = B01000111;  // Set the WDP3 through WDP0 bits, to set the WDT prescale to two seconds.
  sei(); // enables all interrupts
}

int TimerSetting(int r) {
  int TimerSetting;
  switch (r)
  {
    case 0:  // Switches at 00
      TimerSetting = 1;
      break;
    case 1:  // Switches at 01
      TimerSetting = 10;
      break;
    case 10: // Switches at 10
      TimerSetting = 5;
      break;
    case 11: // Switches at 11
      TimerSetting = 15;
      break;
  }
  return TimerSetting;
}

void SnackTimer()
{
    // Execute standard snacktimer routine
  pinMode(GreenLED, OUTPUT);   // declare LED as output
  pinMode(YellowLED, OUTPUT);   // declare LED as output
  pinMode(RedLED, OUTPUT);   // declare LED as output
  pinMode(S1, INPUT);
  pinMode(S2, INPUT);
  
  // Read DIP switches and determine timer setting
  s1state = 1-digitalRead(S1);
  s2state = 1-digitalRead(S2);
  int reading = 10*s1state + s2state;
  TS = TimerSetting(reading);  
  GreenDuration = TS * .80 * oneMinute;  // remember this is in milliseconds
  YellowDuration = TS * .20 * oneMinute; // remember this is in milliseconds
  
  analogWrite(GreenLED, 255);
  delay(GreenDuration);
  heartbeats = 0;
  hbval = 255;
  hbdelta = 8;
  do
  {
    hbcycle=0;
    do // this pulses the heartbeat for one cycle
    {
      if (hbval > 254) hbdelta = -hbdelta;
      if (hbval < 32) hbdelta = -hbdelta;
      hbval += hbdelta;
      analogWrite(GreenLED, hbval);
      IdleChip(20);
      hbcycle+= 1;
    } while (hbcycle<41);
    heartbeats += 1;
  } while (heartbeats<GreenFlashes);
  analogWrite(GreenLED,0);
  pinMode(GreenLED, INPUT);
  
  analogWrite(YellowLED, 75);
  delay(YellowDuration);
  hbval = 255;
  hbdelta = 8;
  heartbeats = 0;
  do
  {
    hbcycle=0;
    do // this pulses the heartbeat for one cycle
    {
      if (hbval > 254) hbdelta = -hbdelta;
      if (hbval < 32) hbdelta = -hbdelta;
      hbval += hbdelta;
      analogWrite(YellowLED, hbval);
      IdleChip(20);
      hbcycle+= 1;
    } while (hbcycle<41);
    heartbeats += 1;
  } while (heartbeats<YellowFlashes);
  analogWrite(YellowLED,0);
  pinMode(YellowLED, INPUT);
  
  // Use Red Heartbeat
  hbval = 255;
  hbdelta = 8;
  heartbeats = 0;
  analogWrite(RedLED,255);
  IdleChip(1000);
  do
  {
    hbcycle=0;
    do // this pulses the heartbeat for one cycle
    {
      if (hbval > 254) hbdelta = -hbdelta;
      if (hbval < 1)
      {
        hbdelta = -hbdelta;
        IdleChip(100);
      }
      hbval += hbdelta;
      analogWrite(RedLED, hbval);
      IdleChip(20);
      hbcycle+= 1;
    } while (hbcycle<41);
    heartbeats += 1;
  } while (heartbeats<RedFlashes);
    pinMode(RedLED, INPUT);
    
    TimerComplete = 1;
}
