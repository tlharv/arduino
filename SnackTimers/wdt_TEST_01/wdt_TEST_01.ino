/*
This example simply blinks an LED on pin 0 periodically.
While the LED is lit, the chip enters Idle sleep mode.
Between blinks the MCU is in power-down sleep mode.
The sleep mode is broken using the watchdog timer.

Board is set to ATtiny85 @ 1 MHz (internal oscillator; BOD disabled)

ATMEL ATTINY85 / ARDUINO

                  +-\/-+
 Ain0 (D 5) PB5  1|    |8  Vcc
 Ain3 (D 3) PB3  2|    |7  PB2 (D 2) Ain1
 Ain2 (D 4) PB4  3|    |6  PB1 (D 1) pwm1
            GND  4|    |5  PB0 (D 0) pwm0 -> to LED
                  +----+
*/

#include <avr/sleep.h>
#include <avr/wdt.h>

//Watchdog timer wake duration options
#define WDTO_15MS   0  // secret code for defining a watchdog timer for 15 milliseconds, to be used with wdt_enable()
#define WDTO_30MS   1  // secret code for defining a watchdog timer for 30 milliseconds, to be used with wdt_enable()
#define WDTO_60MS   2  // secret code for defining a watchdog timer for 60 milliseconds, to be used with wdt_enable()
#define WDTO_120MS  3  // secret code for defining a watchdog timer for 120 milliseconds, to be used with wdt_enable()
#define WDTO_250MS  4  // secret code for defining a watchdog timer for 250 milliseconds, to be used with wdt_enable()
#define WDTO_500MS  5  // secret code for defining a watchdog timer for 500 milliseconds, to be used with wdt_enable()
#define WDTO_1S     6  // secret code for defining a watchdog timer for 1 second, to be used with wdt_enable()
#define WDTO_2S     7  // secret code for defining a watchdog timer for 2 seconds, to be used with wdt_enable()
#define WDTO_4S     8  // secret code for defining a watchdog timer for 4 seconds, to be used with wdt_enable()
#define WDTO_8S     9  // secret code for defining a watchdog timer for 8 seconds, to be used with wdt_enable()


// Define output LED pins
const int LEDPin = 0;   // analog out, to PB0 for red LED, pin 5 on ATtiny85


void setup()
{
  pinMode(LEDPin, OUTPUT);
  wdtSetup();  //this subroutine configures the watchdog timer
}

void loop()
{
  analogWrite(LEDPin, 100);
  IdleChip(125);
  analogWrite(LEDPin, 0);
  
  // DEFINE SLEEP MODE
  wdt_enable(WDTO_8S);  // Set and start the WDT for the argument's duration
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);  // choose your preferred sleep mode for the chip
  sleep_enable(); // this puts the chip into, sleep mode, but doesn't command it to sleep yet.
  // energy saving move: change pin modes to input
  pinMode(LEDPin, INPUT);  

  sleep_mode();  // Last statement of loop() - this command actually puts the chip to sleep.
}

// Watchdog Timer Interrupt
ISR (WDT_vect) {
}



void IdleChip(unsigned long duration)
{
  unsigned long start = millis();
  set_sleep_mode(SLEEP_MODE_IDLE);  // choose idle mode for the chip
  sleep_enable();                              // this puts the chip into sleep mode, but doesn't command it to sleep yet
  while (millis() - start <= duration) {
    sleep_mode();  // put chip in idle mode
  }
  sleep_disable();
}

void wdtSetup() // Set up watchdog timer
{
  cli(); // disable all interrupts
  MCUSR = 0;  // Reset Micro Controller Unit Status Register
  // WDTCR register gives you access to the ATtiny85 WDT.
  // Bit 4 = Watchdog Change Enable.  Must be cleared to change or clear the WDE bit.
  // Bit 3 = WDT bit.  Must be cleared to set WDP3 through WDP0.
  WDTCR |= B00011000; // clear WDTCSR register bits 3 & 4
  WDTCR = B01000111;  // Set the WDP3 through WDP0 bits, to set the WDT prescale to two seconds.
  sei(); // enables all interrupts
}

