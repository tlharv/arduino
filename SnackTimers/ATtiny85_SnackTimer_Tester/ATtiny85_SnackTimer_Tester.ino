// ATMEL ATTINY85 / ARDUINO
//
//                  +-\/-+
// Ain0 (D 5) PB5  1|    |8  Vcc
// Ain3 (D 3) PB3  2|    |7  PB2 (D 2) Ain1
// Ain2 (D 4) PB4  3|    |6  PB1 (D 1) pwm1
//            GND  4|    |5  PB0 (D 0) pwm0
//                  +----+
//
// This is a 15 minute timer for Mrs. Johnson's Montessori classroom, so kids know when
// to end their snack time based on visual cues instead of a disruptive audible alarm.

#include <avr/sleep.h>

const long oneSecond = 1000;
const long oneMinute = oneSecond * 60;

const int GreenLED = 4; // physical pin 3 on ATtiny85
const int RedLED = 0; // physical pin 5 on ATtiny85
const int YellowLED = 1; // physical pin 6 on ATtiny85
const int S1 = 2; // DIP switch 1 -- physical pin 7 on ATtiny85
const int S2 = 3; // DIP switch 2 -- physical pin 2 on ATtiny85

long GreenDuration, YellowDuration; // global variables, set based on DIP switch position

const int GreenFlashes = 7;
const int RedFlashes = 200;
const int YellowFlashes = 7;
const int AlarmDelay = 100;
const int AlarmFlashCount = 10;

int x,heartbeats,hbcycle,hbval,hbdelta;
int s1state, s2state, TS; // state of DIP switches 1 & 2, and Timer Setting value

void setup()
{
  pinMode(GreenLED, OUTPUT);   // declare LED as output
  pinMode(YellowLED, OUTPUT);   // declare LED as output
  pinMode(RedLED, OUTPUT);   // declare LED as output
  pinMode(S1, INPUT);
  pinMode(S2, INPUT);
  
  // Read DIP switches and determine timer setting
  s1state = 1-digitalRead(S1);
  s2state = 1-digitalRead(S2);
  int reading = 10*s1state + s2state;
  TS = TimerSetting(reading);  
  GreenDuration = TS * .80 * oneMinute;  // remember this is in milliseconds
  YellowDuration = TS * .20 * oneMinute; // remember this is in milliseconds
}

void loop()
{
  analogWrite(GreenLED, 255);
  analogWrite(YellowLED, 255);
  analogWrite(RedLED, 255);
  IdleChip(250);
  analogWrite(GreenLED, 0);
  analogWrite(YellowLED, 0);
  analogWrite(RedLED, 0);
  IdleChip(250);
 }


void IdleChip(unsigned long duration)
{
  unsigned long start = millis();
  set_sleep_mode(SLEEP_MODE_IDLE);  // choose idle mode for the chip
  sleep_enable(); // this puts the chip into sleep mode, but doesn't command it to sleep yet
  while (millis() - start <= duration) {
    sleep_mode();  // put chip in idle mode
  }
  sleep_disable();
}

int TimerSetting(int r) {
  int TimerSetting;
  switch (r)
  {
    case 0:  // Switches at 00
      TimerSetting = 1;
      break;
    case 1:  // Switches at 01
      TimerSetting = 10;
      break;
    case 10: // Switches at 10
      TimerSetting = 5;
      break;
    case 11: // Switches at 11
      TimerSetting = 15;
      break;
  }
  return TimerSetting;
}
