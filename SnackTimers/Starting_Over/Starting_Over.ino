#include <avr/sleep.h>

const long oneSecond = 1000;
const long oneMinute = oneSecond * 60;

const int GreenLED = 3; // physical pin 3 on ATtiny85
const int RedLED = 6; // physical pin 5 on ATtiny85
const int YellowLED = 5; // physical pin 6 on ATtiny85

const int GreenFlashes = 5;
const int RedFlashes = 10;
const int YellowFlashes = 5;

int s1state, s2state;  // position for DIP switches

unsigned long SolidGreenDuration, SolidYellowDuration;
unsigned long SG,PG,SY,PY,PR;
int heartbeats, hbval, hbdelta, hbcycle;

const int TS = 15; // Timer Setting, in minutes

void setup()
{
  Serial.begin(9600);
  pinMode(GreenLED, OUTPUT);   // declare LED as output
  pinMode(YellowLED, OUTPUT);   // declare LED as output
  pinMode(RedLED, OUTPUT);   // declare LED as output
  
  Serial.print("Timer set for "); Serial.print(TS); Serial.print(" min or "); Serial.print(TS*oneMinute); Serial.println(" ms");
  SolidGreenDuration = (TS * .8 * oneMinute) - 5980;   // 5 pulse cycles lasts 5974 milliseconds
  SolidYellowDuration = (TS * .2 * oneMinute) - 5980;  // 5 pulse cycles lasts 5974 milliseconds
  
  SG = millis();  // start of solid green
  solidLED(GreenLED, SolidGreenDuration);  // 2nd argument is milliseconds
  PG = millis();  // start of pulsing green
  pulseLED(GreenLED,GreenFlashes); // 2nd argument is number of pulse cycles
  SY = millis();  // start of solid yellow
  solidLED(YellowLED,SolidYellowDuration);
  PY = millis();  // start of pulsing yellow
  pulseLED(YellowLED,YellowFlashes);
  PR = millis();  // start of flashing red
  pulseLED(RedLED, RedFlashes);
  
  Serial.println("Variable Summary");
  Serial.print("SG = "); Serial.println(SG);
  Serial.print("PG = "); Serial.println(PG);
  Serial.print("SY = "); Serial.println(SY);
  Serial.print("PY = "); Serial.println(PY);
  Serial.print("PR = "); Serial.println(PR);
  Serial.print("SolidGreenDuration = "); Serial.print(SolidGreenDuration); Serial.println(" ms.");
  Serial.print("SolidYellowDuration = "); Serial.print(SolidYellowDuration); Serial.println(" ms.");
  Serial.print("Green light pulsed for "); Serial.print(SY - PG); Serial.println(" ms");
  Serial.print("Yellow light pulsed for "); Serial.print(PR - PY); Serial.println(" ms");
  Serial.println();
  Serial.println("Timing Summary");
  Serial.print("Green Light was on for a total of ");
  Serial.print(SY - SG);
  Serial.print(" ms. 80% of TS = ");
  Serial.print(.80*TS*oneMinute);
  Serial.println(" ms.");
  Serial.print("Yellow Light was on for a total of ");
  Serial.print(PR - SY);
  Serial.print(" ms. 20% of TS = ");
  Serial.print(.2*TS*oneMinute);
  Serial.println(" ms.");
  Serial.println();
  Serial.print("Grand total of "); Serial.print((PR-SY)+(SY-SG)); Serial.print(" ms out of "); Serial.print(TS * oneMinute); Serial.print(" ms");
}

void loop()
{
  // Flash yellow LED to signal that you're entering power-down mode
  pinMode(YellowLED, OUTPUT);
  for (int x=0; x<10; x++)
  {
    analogWrite(YellowLED, 125);
    IdleChip(125);
    analogWrite(YellowLED, 0);
    IdleChip(250);
  }
  pinMode(YellowLED, INPUT);
  
  // DEFINE AND ENGAGE SLEEP MODE
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);  // choose your preferred sleep mode for the chip
  sleep_enable(); // this puts the chip into, sleep mode, but doesn't command it to sleep yet.
  sleep_mode();  // Last statement of loop() - this command actually puts the chip to sleep.

}

void solidLED(int LEDpin, unsigned long duration)
{
  analogWrite(LEDpin,255);
  delay(duration);
  analogWrite(LEDpin, 31);
}  


void pulseLED(int LEDpin, int flashes)
{
  for (int x=0; x<flashes; x++)
  {
    heartbeats = 0;
    hbval = 31;
    hbdelta = -8;
    hbcycle=0;
    analogWrite(LEDpin, 31);
    do // this pulses the heartbeat for one cycle
    {
      if (hbval > 254) hbdelta = -hbdelta;
      if (hbval < 32) hbdelta = -hbdelta;
      hbval += hbdelta;
      analogWrite(LEDpin, hbval);
      IdleChip(20);
      hbcycle+= 1;
      //if (hbval <32) Serial.println(hbcycle);
    } while (hbcycle<56);
  }
  pinMode(LEDpin,INPUT); 
}


void IdleChip(unsigned long duration)
{
  unsigned long start = millis();
  set_sleep_mode(SLEEP_MODE_IDLE);  // choose idle mode for the chip
  sleep_enable();                              // this puts the chip into sleep mode, but doesn't command it to sleep yet
  while (millis() - start <= duration) {
    sleep_mode();  // put chip in idle mode
  }
  sleep_disable();
}


int TimerSetting(int r) {
  int TimerSetting;
  switch (r)
  {
    case 0:  // Switches at 00
      TimerSetting = 1;
      break;
    case 1:  // Switches at 01
      TimerSetting = 10;  // use 10 for rotated PCB, 5 for unrotated PCB
      break;
    case 10: // Switches at 10
      TimerSetting = 5;  // use 5 for rotated PCB, 10 for unrotated PCB
      break;
    case 11: // Switches at 11
      TimerSetting = 15;
      break;
  }
  return TimerSetting;
}
