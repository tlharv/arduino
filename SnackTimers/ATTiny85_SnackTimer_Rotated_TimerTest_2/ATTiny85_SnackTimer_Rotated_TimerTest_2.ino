// This test is done using the Uno board and the three LEDs on a breadboard.

#include <avr/sleep.h>

const long oneSecond = 1000;
const long oneMinute = oneSecond * 60;

const int GreenLED = 3; // physical pin 3 on ATtiny85
const int RedLED = 6; // physical pin 5 on ATtiny85
const int YellowLED = 5; // physical pin 6 on ATtiny85
//const int S1 = 2; // DIP switch 1 -- physical pin 7 on ATtiny85
//const int S2 = 3; // DIP switch 2 -- physical pin 2 on ATtiny85

long SolidGreenDuration, SolidYellowDuration; // global variables, set based on DIP switch position

const int GreenFlashes = 7;
const int RedFlashes = 10;
const int YellowFlashes = 7;
const int AlarmDelay = 100;
const int AlarmFlashCount = 10;

int x,heartbeats,hbcycle,hbval,hbdelta;
int s1state, s2state, TS; // state of DIP switches 1 & 2, and Timer Setting value
int TimerComplete;


void setup()
{
  TS = 1; // duration of timer
  Serial.begin(9600);
  Serial.print("Timer set for "); Serial.print(TS); Serial.println(" min");
  SnackTimer();  // This runs the standard snack timer routine, one pass
  Serial.print("Entering deep sleep mode at ");
  Serial.print (millis());
  Serial.println (" ms");
}

void loop()
{
  
  // Flash yellow LED to signal that you're entering power-down mode
  pinMode(YellowLED, OUTPUT);
  for (x=0; x<10; x++) {
    analogWrite(YellowLED, 125);
    IdleChip(125);
    analogWrite(YellowLED, 0);
    IdleChip(250);
  }
  pinMode(YellowLED, INPUT);


  
  // DEFINE AND ENGAGE SLEEP MODE
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);  // choose your preferred sleep mode for the chip
  sleep_enable(); // this puts the chip into, sleep mode, but doesn't command it to sleep yet.
  sleep_mode();  // Last statement of loop() - this command actually puts the chip to sleep.
}


void IdleChip(unsigned long duration)
{
  unsigned long start = millis();
  set_sleep_mode(SLEEP_MODE_IDLE);  // choose idle mode for the chip
  sleep_enable();                              // this puts the chip into sleep mode, but doesn't command it to sleep yet
  while (millis() - start <= duration) {
    sleep_mode();  // put chip in idle mode
  }
  sleep_disable();
}

/*
void wdtSetup() // Set up watchdog timer
{
  cli(); // disable all interrupts
  MCUSR = 0;  // Reset Micro Controller Unit Status Register
  // WDTCR register gives you access to the ATtiny85 WDT.
  // Bit 4 = Watchdog Change Enable.  Must be cleared to change or clear the WDE bit.
  // Bit 3 = WDT bit.  Must be cleared to set WDP3 through WDP0.
  WDTCR |= B00011000; // clear WDTCSR register bits 3 & 4
  WDTCR = B01000111;  // Set the WDP3 through WDP0 bits, to set the WDT prescale to two seconds.
  sei(); // enables all interrupts
}


int TimerSetting(int r) {
  int TimerSetting;
  switch (r)
  {
    case 0:  // Switches at 00
      TimerSetting = 1;
      break;
    case 1:  // Switches at 01
      TimerSetting = 10;
      break;
    case 10: // Switches at 10
      TimerSetting = 5;
      break;
    case 11: // Switches at 11
      TimerSetting = 15;
      break;
  }
  return TimerSetting;
}
*/

void SnackTimer()
{ 
  unsigned long SGstart,PGstart,SYstart,PYstart,PRstart;
  
  // Execute standard snacktimer routine
  pinMode(GreenLED, OUTPUT);   // declare LED as output
  pinMode(YellowLED, OUTPUT);   // declare LED as output
  pinMode(RedLED, OUTPUT);   // declare LED as output
  //pinMode(S1, INPUT);
  //pinMode(S2, INPUT);
  
  // Read DIP switches and determine timer setting
  //s1state = 1-digitalRead(S1);
  //s2state = 1-digitalRead(S2);
  //int reading = 10*s1state + s2state;
  //TS = TimerSetting(reading);  

  
  // Modify the fraction here so that solid green + pulse green = 80% of TS
  SolidGreenDuration = TS * .78 * oneMinute - 6112;  // remember this is in milliseconds
  //Serial.print("SolidGreenDuration = ");
  //Serial.println(SolidGreenDuration);
  //PulseDuration = TS * .02 * oneMinute;  // same for green and yellow
  SolidYellowDuration = TS * .18 * oneMinute - 6112; // remember this is in milliseconds
  //Serial.print("SolidYellowDuration = ");
  //Serial.println(SolidYellowDuration);

  Serial.print ("Starting green solid light at ");
  SGstart = millis();
  Serial.print (SGstart);
  Serial.println (" ms");
  analogWrite(GreenLED, 255);
  delay(SolidGreenDuration);
  heartbeats = 0;
  hbval = 255;
  hbdelta = 8;
  Serial.print ("Starting green pulsing light at ");
  PGstart = millis();
  Serial.print (PGstart);
  Serial.println (" ms");
  do
  {
    hbcycle=0;
    do // this pulses the heartbeat for one cycle
    {
      if (hbval > 254) hbdelta = -hbdelta;
      if (hbval < 32) hbdelta = -hbdelta;
      hbval += hbdelta;
      analogWrite(GreenLED, hbval);
      IdleChip(20);
      hbcycle+= 1;
    } while (hbcycle<41);
    heartbeats += 1;
  } while (heartbeats<GreenFlashes);
  analogWrite(GreenLED,0);
  pinMode(GreenLED, INPUT);
  
  
  Serial.print ("Starting yellow solid light at ");
  SYstart = millis();
  Serial.print (SYstart);
  Serial.println (" ms");
  analogWrite(YellowLED, 0);
  delay(SolidYellowDuration);
  hbval = 255;
  hbdelta = 8;
  heartbeats = 0;
  
  Serial.print ("Starting yellow pulsing light at ");
  PYstart = millis();
  Serial.print (PYstart);
  Serial.println (" ms");
  
  do
  {
    hbcycle=0;
    do // this pulses the heartbeat for one cycle
    {
      if (hbval > 254) hbdelta = -hbdelta;
      if (hbval < 32) hbdelta = -hbdelta;
      hbval += hbdelta;
      analogWrite(YellowLED, hbval);
      IdleChip(20);
      hbcycle+= 1;
    } while (hbcycle<41);
    heartbeats += 1;
  } while (heartbeats<YellowFlashes);
  analogWrite(YellowLED,0);
  pinMode(YellowLED, INPUT);
  
  // Use Red Heartbeat
  hbval = 255;
  hbdelta = 8;
  heartbeats = 0;
  
  Serial.print ("Starting red pulsing light at ");
  PRstart = millis();
  Serial.print (PRstart);
  Serial.println (" ms");
  
  analogWrite(RedLED,255);
  IdleChip(1000);
  do
  {
    hbcycle=0;
    do // this pulses the heartbeat for one cycle
    {
      if (hbval > 254) hbdelta = -hbdelta;
      if (hbval < 1)
      {
        hbdelta = -hbdelta;
        IdleChip(100);
      }
      hbval += hbdelta;
      analogWrite(RedLED, hbval);
      IdleChip(20);
      hbcycle+= 1;
    } while (hbcycle<41);
    heartbeats += 1;
  } while (heartbeats<RedFlashes);
    pinMode(RedLED, INPUT);
    
    TimerComplete = 1;
    Serial.println();
    
    Serial.println("Variable Summary");
    Serial.print("SGstart = "); Serial.println(SGstart);
    Serial.print("PGstart = "); Serial.println(PGstart);
    Serial.print("SYstart = "); Serial.println(SYstart);
    Serial.print("PYstart = "); Serial.println(PYstart);
    Serial.print("PRstart = "); Serial.println(PRstart);
    Serial.print("SolidGreenDuration = "); Serial.print(SolidGreenDuration); Serial.println(" ms.");
    Serial.print("SolidYellowDuration = "); Serial.print(SolidYellowDuration); Serial.println(" ms.");
    Serial.print("Green light pulsed for "); Serial.print(SYstart - PGstart); Serial.println(" ms");
    Serial.print("Yellow light pulsed for "); Serial.print(PRstart - PYstart); Serial.println(" ms");
    Serial.println();
    Serial.println("Timing Summary");
    Serial.print("Green Light was on for a total of ");
    Serial.print(SYstart - SGstart);
    Serial.print(" ms. 80% of TS = ");
    Serial.print(.80*TS*oneMinute);
    Serial.println(" ms.");
    Serial.print("Yellow Light was on for a total of ");
    Serial.print(PRstart - SYstart);
    Serial.print(" ms. 20% of TS = ");
    Serial.print(.2*TS*oneMinute);
    Serial.println(" ms.");
    
}
