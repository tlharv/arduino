// ATMEL ATTINY85 / ARDUINO
//
//                  +-\/-+
// Ain0 (D 5) PB5  1|    |8  Vcc
// Ain3 (D 3) PB3  2|    |7  PB2 (D 2) Ain1
// Ain2 (D 4) PB4  3|    |6  PB1 (D 1) pwm1
//            GND  4|    |5  PB0 (D 0) pwm0
//                  +----+
//
// This is a 15 minute timer for Mrs. Johnson's Montessori classroom, so kids know when
// to end their snack time based on visual cues instead of a disruptive audible alarm.

#include <avr/sleep.h>

const long oneSecond = 1000;
const long oneMinute = oneSecond * 60;

const int GreenLED = 4; // physical pin 3 on ATtiny85
const int RedLED = 0; // physical pin 5 on ATtiny85
const int YellowLED = 1; // physical pin 6 on ATtiny85
const int S1 = 2; // DIP switch 1 -- physical pin 7 on ATtiny85
const int S2 = 3; // DIP switch 2 -- physical pin 2 on ATtiny85

const int GreenFlashes = 5;  // 5980 ms
const int RedFlashes = 100;  // about two minutes
const int YellowFlashes = 5; // 5980 ms

int s1state, s2state;  // position for DIP switches

unsigned long SolidGreenDuration, SolidYellowDuration;
int heartbeats, hbval, hbdelta, hbcycle;
int TS;

void setup()
{
  pinMode(GreenLED, OUTPUT);   // declare LED as output
  pinMode(YellowLED, OUTPUT);   // declare LED as output
  pinMode(RedLED, OUTPUT);   // declare LED as output
  
  // Read DIP switches and determine timer setting
  s1state = 1-digitalRead(S1);
  s2state = 1-digitalRead(S2);
  int reading = 10*s1state + s2state;
  TS = TimerSetting(reading);  

  SolidGreenDuration = (TS * .8 * oneMinute) - 5980;   // Subtract time for 5 pulse cycles
  SolidYellowDuration = (TS * .2 * oneMinute) - 5980;  // Subtract time for 5 pulse cycles
  
  solidLED(GreenLED, SolidGreenDuration);  // 2nd argument is milliseconds
  pulseLED(GreenLED,GreenFlashes); // 2nd argument is number of pulse cycles
  solidLED(YellowLED,SolidYellowDuration);
  pulseLED(YellowLED,YellowFlashes);
  pulseLED(RedLED, RedFlashes);
}

void loop()
{
  // Flash yellow LED to signal that you're entering power-down mode
  pinMode(YellowLED, OUTPUT);
  for (int x=0; x<5; x++)
  {
    analogWrite(YellowLED, 125);
    IdleChip(125);
    analogWrite(YellowLED, 0);
    IdleChip(250);
  }
  pinMode(YellowLED, INPUT);
  
  // DEFINE AND ENGAGE SLEEP MODE
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);  // choose your preferred sleep mode for the chip
  sleep_enable(); // this puts the chip into, sleep mode, but doesn't command it to sleep yet.
  sleep_mode();  // Last statement of loop() - this command actually puts the chip to sleep.

}

void solidLED(int LEDpin, unsigned long duration)
{
  analogWrite(LEDpin,255);
  delay(duration);
  analogWrite(LEDpin, 31);
}  


void pulseLED(int LEDpin, int flashes)
{
  for (int x=0; x<flashes; x++)
  {
    heartbeats = 0;
    hbval = 31;
    hbdelta = -8;
    hbcycle=0;
    analogWrite(LEDpin, 31);
    do // this pulses the heartbeat for one cycle
    {
      if (hbval > 254) hbdelta = -hbdelta;
      if (hbval < 32) hbdelta = -hbdelta;
      hbval += hbdelta;
      analogWrite(LEDpin, hbval);
      IdleChip(20);
      hbcycle+= 1;
      //if (hbval <32) Serial.println(hbcycle);
    } while (hbcycle<56);
  }
  pinMode(LEDpin,INPUT); 
}


void IdleChip(unsigned long duration)
{
  unsigned long start = millis();
  set_sleep_mode(SLEEP_MODE_IDLE);  // choose idle mode for the chip
  sleep_enable();                              // this puts the chip into sleep mode, but doesn't command it to sleep yet
  while (millis() - start <= duration) {
    sleep_mode();  // put chip in idle mode
  }
  sleep_disable();
}


int TimerSetting(int r) {
  int TimerSetting;
  switch (r)
  {
    case 0:  // Switches at 00
      TimerSetting = 1;
      break;
    case 1:  // Switches at 01
      TimerSetting = 10;  // use 10 for rotated PCB, 5 for unrotated PCB
      break;
    case 10: // Switches at 10
      TimerSetting = 5;  // use 5 for rotated PCB, 10 for unrotated PCB
      break;
    case 11: // Switches at 11
      TimerSetting = 15;
      break;
  }
  return TimerSetting;
}
