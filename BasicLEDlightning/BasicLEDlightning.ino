/*
  Blink
  Turns on an LED on for one second, then off for one second, repeatedly.

  Most Arduinos have an on-board LED you can control. On the Uno and
  Leonardo, it is attached to digital pin 13. If you're unsure what
  pin the on-board LED is connected to on your Arduino model, check
  the documentation at http://www.arduino.cc

  This example code is in the public domain.

  modified 8 May 2014
  by Scott Fitzgerald

 */
// ATMEL ATTINY85 / ARDUINO
//
//                  +-\/-+
// Ain0 (D 5) PB5  1|    |8  Vcc
// Ain3 (D 3) PB3  2|    |7  PB2 (D 2) Ain1
// Ain2 (D 4) PB4  3|    |6  PB1 (D 1) pwm1
//            GND  4|    |5  PB0 (D 0) pwm0
//                  +----+
//

#include <avr/sleep.h>
#define NumOfLEDs 5

float RandBrightRange = 255;
float RandTimeRange = 750;
int onesecond = 1000;
int oneminute = 60 * onesecond;
int RandomTimeBetweenLightning = 30 * onesecond;
int LEDpin[] = {0,1,2,3,4};

// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin as an output.  
  for (int i=0; i<NumOfLEDs; i++)
  {
    pinMode(LEDpin[i], OUTPUT);
  }
}


// the loop function runs over and over again forever
void loop() {
 IdleChip(random(RandomTimeBetweenLightning));

 Lightning(0);
 //IdleChip(1000);
 Lightning(1);
 //IdleChip(1000);
 
 //for (int i=0; i<NumOfLEDs; i++)
 //{
  //digitalWrite(LEDpin[i],HIGH);
 //}
 
 //IdleChip(1000);

 //for (int i=0; i<NumOfLEDs; i++)
 //{
  //digitalWrite(LEDpin[i],LOW);
 //} 
 
 //IdleChip(1000);
}

void Lightning(int bulb)
{
  int BETWEEN = 2579;
  int DURATION = 75;
  int TIMES = 20;
  unsigned long lastTime = 0;
  int waitTime = 0;

  if (millis() - waitTime > lastTime) // time for a new flash
  {
    // adjust timing params
    lastTime += waitTime;
    waitTime = random(BETWEEN);

    for (int i=0; i < random(TIMES); i++)
    {
      digitalWrite(LEDpin[bulb], HIGH);
      IdleChip(20 + random(DURATION));
      digitalWrite(LEDpin[bulb], LOW);
      IdleChip(10);
    }
  }
  //digitalWrite(LEDpin[bulb], HIGH);
  //IdleChip(1000);
  //digitalWrite(LEDpin[bulb], LOW);
  //IdleChip(1000);
}



void IdleChip(unsigned long duration)
{
 unsigned long start = millis();
 set_sleep_mode(SLEEP_MODE_IDLE);  // choose idle mode for the chip
 sleep_enable();                              // this puts the chip into sleep mode, but doesn't command it to sleep yet
 while (millis() - start <= duration) {
   sleep_mode();  // put chip in idle mode
 }
 sleep_disable();
}
